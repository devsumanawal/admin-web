export class SessionStorageConstants {

  public static IsFirstLogin = 'IsFirstLogin';
  public static Authorization = 'Authorization';
  public static CardViewEnabled = 'cardViewEnabled';
}
