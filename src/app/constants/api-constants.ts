import { environment } from 'environments/environment';

export class ApiConstants {
  public static ENDPOINT = environment.ENDPOINT;

  public static OFFICE = 'office';
  public static DEPARTMENT = 'department';
  public static MINISTRY = 'ministry';
  public static CURRENCY = 'currency';
  public static SOURCE = 'source';
  public static DONOR = 'donor';
  public static LINE_ITEM = 'lineItem';
  public static FISCAL_YEAR = 'fiscalYear';
  // public static BUDGET = 'budget';
  public static PROJECT_DATA = 'projectData';

  public static ADMIN = 'user';
  public static CLIENT = 'client';
  public static MERCHANT = 'merchant';
  public static LOG = 'log';
  public static MASTER = 'master';
  public static NOTIFICATION = 'notification';
  public static MERCHANT_OPERATIONAL_FEATURE = 'merchantOperationalFeature';
  public static MERCHANT_DETAIL = 'merchantDetail';
  public static RECONCILATION = 'reconcilation';
  public static STATUS = 'status';
  public static CLIENT_LOGIN = 'clientLogin';
  public static DASHBOARD = 'dashBoard';
  public static CHART_DASHBOARD = 'chartDashBoard';

  public static OFFICE_TYPE = 'officeType';
  public static USER_GROUP = 'userGroup';
  public static PROVINCE = 'province';

  public static LOGGER = 'logger';
  public static RELOAD = 'reload';

  public static NAVIGATION = '/navigation';

  static PROFILE = '/profile';

  static DETAIL = '/detail';

  public static MERCHANT_REQUEST_DETAILS = '/merchantRequestDetails';
  public static TIMELINE = '/timeLine';
  public static MERCHANT_REQUEST_MANAGE = '/merchantRequestManage';
  public static PERFORM_ESEWA_SETTLEMENT = '/performEsewaSettlement';

  public static ADMIN_PROFILE = 'users/profile';
  public static CLIENT_PROFILE = 'clientProfile';
  public static FOR_ADMIN_CREATE = 'forAdminCreate';
  public static FOR_CLIENT_CREATE = 'forClientCreate';

  public static AGENT_TYPE = 'agentType';

  public static ADMIN_ROLES = '/userRoles';
  public static CLIENT_ROLES = '/clientRoles';

  public static MANAGE = '/manage';
  public static BLOCK = '/block';
  public static MODIFY = '/modify';
  public static UNBLOCK = '/unBlock';
  public static DELETE = '/Delete';
  public static PHOTO = '/photo';
  public static CLIENT_QR_IMAGE = '/clientQrImage';
  public static RESET_PASSWORD = '/resetPassword';
  public static RESET_TOTP = '/resetTotp';
  public static GENERATE_QR_CODE = '/generateQrCode';
  public static DOWNLOAD = '/download';
  public static SEARCH_PARAM_BY_MERCHANT = '/searchParamByMerchant';
  public static DOWNLOAD_NEW_QR = '/downloadNewQr';

  public static ACTIVE = '/active';
  public static DEACTIVE = '/deactive';

  public static ADMIN_AUTHENTICATION = 'login';
  public static TWO_FACTOR_AUTHENTICATION = '/2FAuthentication';
  public static ENABLE_OTP_BEFORE_LOGIN = '/enableOtpBeforeLogin';
  public static CHANGE_PASSWORD = '/changePassword';

  public static ADMIN_DETAIL = '/adminDetails';
  public static ADMIN_PHOTO = '/adminPhoto';
  public static GENERATE_OTP = '/generateOtp';
  public static ENABLE_OTP = '/enableOtp';
  public static DISABLE_OTP = '/disableOtp';
  public static EDIT_ADMIN = '/editAdmin';
  public static ADMIN_ACTIVITY = '/adminActivity';

  public static PASSWORD_POLICY = '/passwordPolicy';
  public static META_TABLE = 'metaTable';

  public static MERCHANT_PROFILE = 'merchantProfile';
  public static MERCHANT_TYPE = 'merchantType';
  public static MERCHANT_TYPE_MANAGE = 'merchantTypeManage';
  public static FOR_MERCHANT_CREATE = 'forMerchantCreate';
  public static MERCHANT_ROLES = '/merchantRoles';
  public static MANAGE_MERCHANT_PROFILE = '/manageMerchantProfile';

  public static SEARCH = '/search';
  public static REVALIDATION_SETUP = '/revalidationSetup';
  public static SEARCH_RECONCILE_CONDITION = '/searchReconcileCondition';
  public static SEARCH_BY_MRCHANT = '/searchByMerchant';
  public static SUSPICIOUS_REQUEST = '/suspiciousRequest';
  public static ESEWA_SETTLEMENT_LOG = '/esewaSettlementLog';
  public static ESEWA_BANK_WITHDRAW_LOG = '/esewaBankWithDrawLog';
  public static MERCHANT_QR_REQUEST = '/merchantQrRequest';
  public static MERCHANT_REQUEST_VERIFICATION = '/merchantRequestVerification';
  public static CLIENT_RESPONSE = '/clientResponse';

  public static TOTAL_TXN_AMOUNT_BY_CLIENT = '/totalTxnAmountByClient';
  public static TOTAL_TXN_AMOUNT_BY_MERCHANT = '/totalTxnAmountByMerchant';
  public static TOTAL_REGISTERED_MERCHANT = '/totalRegisteredMerchant';
  public static TOTAL_QR_TRANSACTIONS = '/totalQrTransactions';
  public static TOTAL_REGISTERED_CLIENT = '/totalRegisteredClient';
  public static TOTAL_SUSPICIOUS_REQUEST = '/totalSuspiciousRequest';
  public static ALL_QR_TYPE_TXN = '/allQrTypeTxn';
  public static MERCHANT_BY_OWNERSHIP = '/merchantByOwnership';
  public static MONTHLY_TOTAL_SUCCESSFUL_TXN = '/monthlyTotalSuccessfulTxn';
  public static MONTHLY_QR_TRANSACTION = '/monthlyQrTransaction';
  public static CLIENT_TXN_DETAILS = '/clientTxnDetails';
  public static TOP_TWENTY_MERCHANT_TXN_DETAILS = '/topTwentyMerchantTxnDetails';
  public static TOP_FIVE_FREQUENNT_CUSTOMERS = '/topFiveFrequentCustomers';
  public static TOP_FIVE_TXN_BANKS = '/topFiveTxnBanks';
  public static CUSTOMER_REGISTERED_THROUGH_PARTNER_IN_RPS = '/customerRegisteredThroughPartner';
  public static MONTHLY_REGISTERED_CUSTOMER_IN_RPS = '/montlyRegisteredCustomerInRPS';
  public static DAILY_REGISTERED_CUSTOMER_IN_RPS = '/dailyRegisteredCustomerInRPS';
  public static UNIQUE_CUSTOMER = '/uniqueCustomer';


  public static PDF = '/pdf';
  public static XLS = '/xls';

  public static BUSINESS_TYPE_LIST = '/businessTypeList';
  public static BUSINESS_REGISTRATION_ORG = '/businessRegistrationOrgList';
  public static ALREADY_CREATED_MERCHANT = '/alreadyCreatedMerchant';

  public static EMAIL_INTEGRATION_DETAIL = '/emailIntegrationDetail';

  public static SUB_MERCHANT = '/subMerchant';
  public static SERVICE_PROVIDER = '/serviceProvider';
  public static PUSH_NOTIFICATION_TO_MERCHANT = '/pushNotificationToMerchant';
  public static AUTO_WITHDRAWL_DISABLE = '/autoWithdrawlDisable';
  public static AUTO_WITHDRAWL_ENABLE = '/autoWithdrawlEnable';
  public static MERCHANT_APPROVAL_API_LOG = '/merchantApprovalApiLog';
  public static TAX_REFUND_LOG = '/taxRefundLog';
  public static MERCHANT_OFFER = '/merchantOffer';

  public static REVERSE_TXN_PAYMENT_VIA_APP = '/reverseTxnPaymentViaApp';

  public static BLACK_LIST_QUICK_MERCHANT = '/quickMerchant';
  public static BLACK_LIST = '/blackList';
  public static ENABLE_DISABLE_BLACKLIST_QUICK_MERCHANT = '/enableDisableBLackListQuickMerchant';


  public static UNION_PAY = 'unionpay';
  public static UNION_PAY_SETTLEMENT = '/unionpaySettlement';
  public static UNION_PAY_SETTLEMENT_AUDIT_FILE = '/unionpaySettlementAuditFile';


  public static SETTLEMENT_SHARING = 'settlementSharing';
  public static CREATE_SETTLEMENT_SHARING = '/createSettlementSharing';
  public static MODIFY_SETTLEMENT_SHARING = '/modifySettlementSharing';
  public static FETCH_ALL_SETTLEMENT_PARNTERS = '/fetchAllSettlementPartners';


  public static DISCOUNT = 'discount';
  public static DISCOUNT_SETUP = '/discountSetup';

  public static COMMISSION_SETUP = 'commissionSetup';

  public static ENABLE_DISABLE_MERCHANT_NETWORK = '/merchantNetworkEnableDisable';
  public static MERCHANT_NETWORK = 'merchantNetwork';

  //default commission setup
  public static DEFAULT_COMMISSION_SETUP = 'defaultCommission';
  public static COMMISSIONABLE_SERVICE_PROVIDERS = 'commissionableServiceProviders';
  public static CREATE_DEFAULT_NETWORK_COMMISSION = 'createDefaultCommission';
  public static CATEGORY_WISE_COMMISSIONABLE_SERVICE_PROVIDERS = 'categoryWiseCommissionableServiceProviders';
  public static CREATE_CATEGORY_WISE_NETWORK_COMMISION = 'createCategoryWiseCommission';


  public static TODAY = '/today';
  public static THIS_MONTH = '/thisMonth';
  public static THIS_YEAR = '/thisYear';
  public static COUNT = '/count';

  public static MERCHANT_OPERATIONAL_ACTIVITY = 'merchantOperationalActivities';
  public static OPERATIONAL_ACTIVITY = 'operationalActivities';
  public static CREATE_MERCHANT_OPERATIONAL_ACTIVITY = '/createOperationalActivities';
  public static AIRLINE_TICKET_COUNT = '/airlineTicketCount';

  public static ACQUIRER_MERCHANT_DETAILS = '/acquirerMerchantDetails';
  public static TOTAL_CLIENT_TXN_DETAILS = '/totalClientTxnDetails';
  public static MERCHANT_DOCUMENT = '/merchantDocument';

  // For municipalities

  public static PROVINCE_LIST = '/provinceList';
  public static DISTRICT_LIST = '/districtList';
  public static MANAGE_MUNICIPALITY = '/manageMunicipality';
  public static CREATE_MUNICIPALITIES = '/createMuniciplities';
  public static MUNICIPALITIES_SETUP = 'municipalitiesSetup';

  public static REALLOCATION = '/reallocation';
  public static REALLOCATION_APPROVE = '/reallocationApprove';
  public static REALLOCATION_REJECT = '/reallocationReject';

  public static QR_ISSUER_WISE_CHART = '/qrIssuerWiserChart';
  public static DISPUTED_MERCHANT_APPROVE = '/disputedMerchantApprove';
  public static DISPUTED_MERCHANT_REJECT = '/disputedMerchantReject';
  public static MINICIPALITIES_LIST = '/municipalitiesList';

  // For delete unapproved merchant
  public static DELETE_UNAPPROVED_MERCHANT_REQUEST = '/deleteUnapprovedMerchantRequest';
  public static DELETE_UNAPPROVED_MERCHANT_APPROVE = '/deleteUnapprovedMerchantApprove';
  public static DELETE_UNAPPROVED_MERCHANT_REJECT = '/deleteUnapprovedMerchantReject';
  public static DELETE_UNAPPROVED_MERCHANT_REALLOCATION_LIST = '/deleteUnapprovedReallocationList';

  public static MERCHANT_PROFILE_DETAILS = 'merchantProfileDetails';

  public static MERCHANT_ACCOUNT_EDITED_LOG = "/merchantAccountEditedLog";
  public static EDITED_ACCOUNT_ESEWA_UPDATE = "/editedAccountEsewaUpdate";

  public static CONFIG = 'config';
  public static QR_MANAGE = 'qrManage';
  public static SMALL_QR = 'smallQr';
  public static LARGE_QR = 'largeQr';
  public static MINIO_PRESIGNED_URL = 'minioPresignedUrl';

  public static GUIDELINE_DOCUMENT_MANAGE = 'guidelineDocumentManage';
  public static GUIDELINE_DOCUMENT = 'guidelineDocument';
  public static GUIDELINE_DOCUMENT_TYPE = 'guidelineDocumentType';
  public static FORWARDED_TO_ISSUER = '/forwaredToIssuer';
  public static FONEPAY_ADMIN_EMAIL_FOR_INTEGRATION_DOCUMENT = 'adminEmailForIntegrationDocument';

}
