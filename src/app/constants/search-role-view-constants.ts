export class SearchRoleViewConstants {

  public static VIEW = 'VIEW';
  public static VIEW_MANAGE = 'VIEW_MANAGE';
  public static VIEW_MANAGE_SUB_MERCHANT = 'VIEW_MANAGE_SUB_MERCHANT';
  public static NOT_IN_SETTLEMENT = 'NOT_IN_SETTLEMENT';

}
