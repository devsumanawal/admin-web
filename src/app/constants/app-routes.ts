export class AppRoutes {
  public static LOGIN = 'login';
  public static DASHBOARD = 'dashboard';
  public static AUTHENTICATE = 'authenticate';
  public static ENABLE_AUTHENTICATION = 'enableAuthentication';
  public static CHANGE_PASSWORD = 'changePassword';

  public static ADMIN = 'admin';
  public static MERCHANT = 'merchant';
  public static CLIENT = 'client';
  public static MERCHANT_TYPE = 'merchantType';

  public static OFFICE_TYPE = 'officeType';
  public static USER_GROUP = 'userGroup';
  public static OFFICE = 'office';
  public static DEPARTMENT = 'department';
  public static MINISTRY = 'ministry';
  public static CURRENCY = 'currency';
  public static SOURCE = 'source';
  public static LINE_ITEM = 'line-item';
  public static DONOR = 'donor';
  public static FISCAL_YEAR = 'fiscalYear';
  // public static BUDGET = 'budget';
  public static PROJECT_DATA = 'projectData';

  public static ADMIN_PROFILE = 'adminProfile';
  public static MERCHANT_PROFILE = 'merchantProfile'
  public static CLIENT_PROFILE = 'clientProfile'

  public static REQUEST = 'request';
  public static SEARCH_REQUEST = 'searchRequest';
  public static SEARCH_REQUEST_ID = 'searchRequest/:id';
  public static SEARCH_SUSPICIOUS_REQUEST = 'searchSuspiciousRequest';
  public static SEARCH_SUSPICIOUS_REQUEST_ID = 'searchSuspiciousRequest/:id';
  public static SEARCH_ESEWA_SETTLEMENT_LOG = 'searchEsewaSettlementLog';
  public static SEARCH_ESEWA_BANK_WITHDRAW_LOG = 'searchEsewaBankWithDrawLog';
  public static SEARCH_ESEWA_SETTLEMENT_LOG_REQUEST_ID = 'searchEsewaSettlementLog/:requestId';
  public static SEARCH_MERCHANT_QR_REQUEST = 'searchMerchantQrRequest';
  public static SEARCH_MERCHANT_QR_REQUEST_REQUEST_ID = 'searchMerchantQrRequest/:requestId';
  public static SEARCH_MERCHANT_REQUEST_VERIFICATION = 'searchMerchantRequestVerification';
  public static SEARCH_MERCHANT_REQUEST_VERIFICATION_REQUEST_ID = 'searchMerchantRequestVerification/:requestId';
  public static SEARCH_CLIENT_RESPONSE = 'searchClientResponse';
  public static SEARCH_CLIENT_RESPONSE_REQUEST_ID = 'searchClientResponse/:requestId';
  public static MERCHANT_REQUEST_DETAILS = 'merchantRequestDetails';
  public static MERCHANT_REQUEST_DETAILS_ID = 'merchantRequestDetails/:id';
  public static ESEWA_UNSETTLED_REQUEST = 'esewaUnSettledRequest';
  public static NOTIFY_TO_MERCHANT = 'notifyToMerchant';
  public static RECONCILE_AGAIN = 'reconcileAgain';

  public static MANAGE_PASSWORD_POLICY = 'managePasswordPolicy';
  public static MANAGE_LOGGER = 'manageLogger';
  public static META_TABLE = 'manageMetaTable';
  public static CONFIG = 'config';
  public static NOTIFICATION = 'notification';
  public static STATUS = 'status';

  public static MERCHANT_DETAILS = 'merchantDetails';
  public static MERCHANT_DETAILS_ID = 'merchantDetails/:id';
  public static ADMIN_DETAIL = 'adminDetail';

  public static CREATE = 'create';
  public static MANAGE = 'manage';
  public static VIEW = 'view';
  public static UNAPPROVED_MERCHANT = 'unApprovedMerchant';
  public static DELETE_UNAPPROVED_MERCHANT = 'deleteUnApprovedMerchant';
  public static DETAILS_MERCHANT_BY_ID_Only = 'merchantDetail';
  public static DETAILS_MERCHANT_BY_ID = 'merchantDetail/:id';
  public static EDIT = 'edit';
  public static EDIT_OPERATIONAL_FEATURE = 'editOperational/:id';
  public static EDIT_ID = 'edit/:id';
  public static EDIT_BY_EDIT_ID = 'edit/:editID';
  public static DETAILS = 'details';
  public static DETAILS_ID = 'details/:id';

  public static notFound = '404';
  public static forbidden = '403';
  public static serverError = '500';

  public static CLIENT_LOGIN = 'clientLogin/:id';
  public static CLIENT_LOGIN_CREATE = 'clientLogin/:id/clientLoginCreate';
  public static CLIENT_LOGIN_MANAGE = 'clientLogin/:id/clientLoginManage/:clientLoginId';
  public static CLIENT_LOGIN_DETAIL = 'clientLogin/:id/clientLoginDetails';

  public static SUB_MERCHANT = 'subMerchant/:id';
  public static SUB_MERCHANT_CREATE = 'subMerchantCreate';
  public static SUB_MERCHANT_MANAGE = 'subMerchantManage';
  public static SUB_MERCHANT_DETAIL = 'subMerchantDetails';

  public static MERCHANT_APPROVAL_API_LOG = 'merchantApprovalSequenceLog';


  public static MERCHANT_OFFER = 'merchantOffer/:id';
  public static MANAGE_BLACKLIST_QUICK_MERCHANT = 'manageBlackListQuickMerchant/:id';
  public static ADD_BLACKLIST_QUICK_MERCHANT = 'addBlackListQuickMerchant';

  public static CREATE_MERCHANT_OFFER = 'merchantOfferCreate';
  public static SEND_REVERSAL_REQUEST = 'sendReversalRequest';

  public static UNIONPAY = 'unionpay';
  public static UNIONPAY_SETTLEMENT = 'unionpaySettlement';
  public static UNIONPAY_SETTLEMENT_AUDIT_FILE = 'unionpaySettlementAuditFile';
  public static REJECTED_MERCHANT = 'rejectedMerchant';
  public static DISPUTED_MERCHANT = 'disputedMerchant';
  public static SETTLEMENT_SHARING = 'settlementSharingSetup';
  public static CREATE_SETTLEMENT_SHARING = 'createSettlementSharing';
  public static MANAGE_SETTLEMENT_SHARING = 'manageSettlementSharing';
  public static VIEW_SETTLEMENT_SHARING = 'viewSettlementSharing';

  public static COMMISSION_SETUP = 'commissionSetup/:id';
  public static COMMISSION_SETUP_DETAILS = 'commissionSetupDetails';
  public static CREATE_COMMISSION_SETUP = 'commissionSetupCreate';


  public static DISCOUNT_SETUP = 'discountSetup/:id';
  public static DISCOUNT_CREATE = 'discountSetupCreate';
  public static DISCOUNT_VIEW = 'discountSchemeView';

  public static DEFAULT_COMMISSION = 'defaultCommission';

  public static DASHBOARD_REWARDS_CUSTOMER_REPORT = 'dashboardRewardsCustomerReport';
  public static DASHBOARD_REWARDS_CUSTOMER_COMPARISION_REPORT = 'dashboardRewardsCustomerComparisionReport';


  public static MERCHANT_OPERATIONAL_ACTIVITY = 'operationalMaterial/:id';
  public static ISSUED_AIRLINE_TICKET = 'issuedAirlineTicket';

  public static DASHBOARD_ISSUER_TXNS_REPORT = 'totalIssuerTxn';

  public static CREATE_MUNICIPALITY = 'createMunicipalities';
  public static MANAGE_MUNICIPALITY = 'municipalitiesSetup';

  public static REALLOCATION_MERCHANT = 'reallocationMerchant';
  public static REALLOCATION_APPROVED = 'reallocationApproved';

  public static UNIQUE_CUSTOMER_REPORT = 'uniqueCustomerReport';
  public static UNIQUE_CUSTOMRER_COMPARISION_REPORT = 'uniqueCustomerComparisionReport';

  public static QR_MANAGE = 'qrManage';

  public static GUIDELINE_DOCUMENT_MANAGE = 'guidelineDocument';
  public static GUIDELINE_DOCUMENT = 'guidelineDocument';
  public static GUIDELINE_DOCUMENT_TYPE = 'guidelineDocumentType';

  public static MERCHANT_ACCOUNT_EDITED_LOG = 'merchantAccountEditedLog';
  public static TAX_REFUND_LOG = 'taxRefundLog';
}
