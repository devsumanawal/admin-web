export class QrRequestTypeConstants {
  public static WEB_ONLINE = 'WEB_ONLINE';
  public static APP_ONLINE = 'APP_ONLINE';
  public static APP_OFFLINE = 'APP_OFFLINE';

  public static QrRequestTypeConstantsList =
    [
      QrRequestTypeConstants.WEB_ONLINE, QrRequestTypeConstants.APP_ONLINE,
      QrRequestTypeConstants.APP_OFFLINE
    ];
}
