import { StatusSelectItem } from '../module/core/models/status-selectitem';

export class FonepayAppConstant {

  public static ALL_CLIENT = 'ALL_CLIENT';
  public static ALL_MERCHANT = 'ALL_MERCHANT';
  public static BOTH = 'BOTH';

  public static getStatusForFilter(): StatusSelectItem[] {
    const selectItemList: StatusSelectItem[] = [];

    const statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = FonepayAppConstant.ALL_CLIENT;
    statusSelectItem1.value = FonepayAppConstant.ALL_CLIENT;
    selectItemList.push(statusSelectItem1);

    const statusSelectItem2 = new StatusSelectItem();
    statusSelectItem2.label = FonepayAppConstant.ALL_MERCHANT;
    statusSelectItem2.value = FonepayAppConstant.ALL_MERCHANT;
    selectItemList.push(statusSelectItem2);

    const statusSelectItem3 = new StatusSelectItem();
    statusSelectItem3.label = FonepayAppConstant.BOTH;
    statusSelectItem3.value = FonepayAppConstant.BOTH;
    selectItemList.push(statusSelectItem3);

    return selectItemList;
  }
}
