import {BsDatepickerConfig} from 'ngx-bootstrap/datepicker';

export class AppConstant {
  public static RECAPTCHA_SITE_KEY = '6LfiAmIUAAAAAP5EmSHZartD1qgu6av2z6hBPsss';
  public static MAX_FILE_SIZE_LIMIT_IN_MEGABYTE = 5;
  public static MAX_FILE_SIZE_LIMIT_IN_BYTE = AppConstant.MAX_FILE_SIZE_LIMIT_IN_MEGABYTE * 1000000;

  public static readonly bsConfig: Partial<BsDatepickerConfig> = Object.assign({}, {showWeekNumbers: false});
}
