import {StatusSelectItem} from 'app/module/core/models/status-selectitem';
import {HasSubMerchantSelectItem} from 'app/module/core/models/HasSubMerchantSelectItem';
import {Status} from '../module/core/models/status';

export class StatusDescriptionConstants {

  public static ACTIVE = 'ACTIVE';
  public static INACTIVE = 'IN ACTIVE';

  public static CREATED = 'CREATE_APPROVE';
  public static EDITED = 'EDIT_APPROVE';
  public static BLOCKED = 'BLOCKED_APPROVE';
  public static UNBLOCKED = 'UNBLOCK_APPROVE';
  public static APPROVED = 'APPROVED';
  public static DELETED = 'DELETED_APPROVE';
  public static REJECTED = 'REJECTED';

  public static CREATE_PENDING = 'CREATE_PENDING';
  // public static EDITED_PENDING = 'EDITED PENDING';
  // public static BLOCKED_PENDING = 'BLOCKED PENDING';
  // public static UNBLOCKED_PENDING = 'UNBLOCKED PENDING';

  public static PENDING = 'PENDING';
  public static AMBIGIOUS = 'AMBIGIOUS';
  public static CANCEL = 'CANCEL';
  public static SUCCESS = 'SUCCESS';
  public static FAILED = 'FAILED';
  public static COMPLETED = 'COMPLETED';
  public static VERIFIED = 'VERIFIED';

  public static ESEWA_TRANSACTION_PENDING = 'ESEWA_TRANSACTION_PENDING';
  public static ESEWA_SETTLED = 'ESEWA_SETTLED';
  public static ESEWA_SETTLEMENT_FAILED = 'ESEWA_SETTLEMENT_FAILED';

  public static MERCHANT_REQUEST = 'MERCHANT_REQUEST';
  public static ESEWA_VALIDATE = 'ESEWA_VALIDATE';
  public static CLIENT_REQUEST = 'CLIENT_REQUEST';
  public static CLIENT_RESPONSE = 'CLIENT_RESPONSE';
  public static CLIENT_VERIFY_REQUEST = 'CLIENT_VERIFY_REQUEST';
  public static CLIENT_VERIFY_RESPONSE = 'CLIENT_VERIFY_RESPONSE';
  public static MERCHANT_VERIFY_REQUEST = 'MERCHANT_VERIFY_REQUEST';

  public static TRANSACTION_COMPLETED = 'TRANSACTION_COMPLETED';
  public static TRANSACTION_FAILED = 'TRANSACTION_FAILED';
  public static TRANSACTION_PENDING = 'TRANSACTION_PENDING';

  public static DO_REVERSAL = 'DO_REVERSAL';
  public static REFUNDED = 'REFUNDED';
  public static REVERSED = 'REVERSED';
  public static WAITING_SETTLEMENT_NOTIFICATION = 'WAITING_SETTLEMENT_NOTIFICATION';
  public static SETTLEMENT_NOT_REQUIRED = 'SETTLEMENT_NOT_REQUIRED';
  public static REVERSAL_DUE_TO_CLIENT_ISSUE = 'REVERSAL_DUE_TO_CLIENT_ISSUE';
  public static REVERSAL_DUE_TO_MERCHANT_ISSUE = 'REVERSAL_DUE_TO_MERCHANT_ISSUE';

  public static MERCHANT_REALLOCATION_PENDING = 'MERCHANT_REALLOCATION_PENDING';

  public static REFUND = 'REFUND';
  public static REVERSAL = 'REVERSAL';

  //by Name
  // public static MERCHANT_VERIFY_REQUEST_STAGE = 'MERCHANT_VERIFY_REQUEST';
  // public static CREATED_STAGE = 'CREATE_APPROVE';
  // public static BLOCKED_STAGE = 'BLOCKED_APPROVE';

  public static listToShowForManage =
    [
      StatusDescriptionConstants.CREATED, StatusDescriptionConstants.EDITED,
      StatusDescriptionConstants.BLOCKED, StatusDescriptionConstants.UNBLOCKED,
      StatusDescriptionConstants.APPROVED
    ];

  public static ApprvedAndBlockedStatus =
    [
      StatusDescriptionConstants.CREATED,
      StatusDescriptionConstants.BLOCKED
    ];

  public static listToShowForUnApprovedMerchant =
    [
      StatusDescriptionConstants.CREATED, StatusDescriptionConstants.EDITED,
      StatusDescriptionConstants.BLOCKED, StatusDescriptionConstants.UNBLOCKED,
      StatusDescriptionConstants.APPROVED, StatusDescriptionConstants.CREATE_PENDING
    ];

  // public static listToShowForApproveDisApprove =
  //   [
  //     StatusDescriptionConstants.CREATE_PENDING, StatusDescriptionConstants.EDITED_PENDING,
  //     StatusDescriptionConstants.BLOCKED_PENDING, StatusDescriptionConstants.UNBLOCKED_PENDING
  //   ];

  public static listToShowForMerchantRequestStatus =
    [
      StatusDescriptionConstants.PENDING, StatusDescriptionConstants.CANCEL,
      StatusDescriptionConstants.AMBIGIOUS,
      StatusDescriptionConstants.REVERSED,
      StatusDescriptionConstants.SUCCESS, StatusDescriptionConstants.FAILED,
      StatusDescriptionConstants.ESEWA_TRANSACTION_PENDING, StatusDescriptionConstants.ESEWA_SETTLED,
      StatusDescriptionConstants.ESEWA_SETTLEMENT_FAILED,
      StatusDescriptionConstants.SETTLEMENT_NOT_REQUIRED,
      StatusDescriptionConstants.REFUNDED
    ];

  public static listToShowForSuspiciousRequestStatus =
    [
      StatusDescriptionConstants.PENDING,
      StatusDescriptionConstants.AMBIGIOUS,
      StatusDescriptionConstants.SUCCESS
    ];

  public static listToShowForMerchantQrRequestStatus =
    [
      StatusDescriptionConstants.TRANSACTION_PENDING,
      StatusDescriptionConstants.VERIFIED,
      StatusDescriptionConstants.SUCCESS,
      StatusDescriptionConstants.FAILED,
      StatusDescriptionConstants.AMBIGIOUS,
      StatusDescriptionConstants.CANCEL,
      StatusDescriptionConstants.DO_REVERSAL,
      StatusDescriptionConstants.COMPLETED
    ];

  public static listToShowForEsewaSettlementLogStatus =
    [
      StatusDescriptionConstants.ESEWA_SETTLEMENT_FAILED,
      StatusDescriptionConstants.ESEWA_TRANSACTION_PENDING
    ];

  public static listToShowForMerchantRequestStages =
    [
      StatusDescriptionConstants.MERCHANT_REQUEST, StatusDescriptionConstants.ESEWA_VALIDATE,
      StatusDescriptionConstants.CLIENT_REQUEST,
      // StatusDescriptionConstants.CLIENT_RESPONSE,
      StatusDescriptionConstants.CLIENT_VERIFY_REQUEST, StatusDescriptionConstants.CLIENT_VERIFY_RESPONSE,
      StatusDescriptionConstants.MERCHANT_VERIFY_REQUEST, StatusDescriptionConstants.TRANSACTION_COMPLETED,
      StatusDescriptionConstants.TRANSACTION_FAILED, StatusDescriptionConstants.ESEWA_SETTLED,
      StatusDescriptionConstants.WAITING_SETTLEMENT_NOTIFICATION,
      StatusDescriptionConstants.DO_REVERSAL,
      StatusDescriptionConstants.REVERSED,
      StatusDescriptionConstants.REVERSAL_DUE_TO_CLIENT_ISSUE,
      StatusDescriptionConstants.REVERSAL_DUE_TO_MERCHANT_ISSUE,
      StatusDescriptionConstants.SETTLEMENT_NOT_REQUIRED,
      StatusDescriptionConstants.REFUNDED
    ];

  public static listToShowForSuspiciousRequestStages =
    [
      StatusDescriptionConstants.REVERSAL_DUE_TO_CLIENT_ISSUE,
      StatusDescriptionConstants.REVERSAL_DUE_TO_MERCHANT_ISSUE
    ];

  public static inActiveStatusList =
    [
      StatusDescriptionConstants.BLOCKED,
      StatusDescriptionConstants.DELETED,
      StatusDescriptionConstants.REJECTED,
      StatusDescriptionConstants.MERCHANT_REALLOCATION_PENDING
    ];

    public static isVisibilityHidden = [

    ];

  public static isActive(statusName: string): boolean {
    if (statusName) {
      statusName = statusName.toLocaleUpperCase();
      if (this.inActiveStatusList.includes(statusName)) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  public static isActiveStatus(status: Status): boolean {
    if (status) {
      return StatusDescriptionConstants.isActive(status.name);
    } else {
      return false;
    }
  }

  public static getShortNameForStatus(statusDescription: string) {
    let shortForm = '';
    statusDescription.split(/_| /).forEach(function (value) {
      shortForm = shortForm + value.charAt(0);
    });
    return shortForm;
  }

  public static getStatusForFilter(): StatusSelectItem[] {
    let selectitemList: StatusSelectItem[] = new Array();
    let statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = 'ACTIVE';
    statusSelectItem1.value = StatusDescriptionConstants.CREATED;
    selectitemList.push(statusSelectItem1);

    let statusSelectItem3 = new StatusSelectItem();
    statusSelectItem3.label = 'IN ACTIVE';
    statusSelectItem3.value = StatusDescriptionConstants.BLOCKED;
    selectitemList.push(statusSelectItem3);

    return selectitemList;
  }


  public static getStatusForSearchRequest(): StatusSelectItem[] {
    let selectitemList: StatusSelectItem[] = new Array();
    let statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = 'SUCCESS';
    statusSelectItem1.value = StatusDescriptionConstants.SUCCESS;
    selectitemList.push(statusSelectItem1);

    let statusSelectItem2 = new StatusSelectItem();
    statusSelectItem2.label = 'FAILED';
    statusSelectItem2.value = StatusDescriptionConstants.FAILED;
    selectitemList.push(statusSelectItem2);

    let statusSelectItem3 = new StatusSelectItem();
    statusSelectItem3.label = 'PENDING';
    statusSelectItem3.value = StatusDescriptionConstants.PENDING;
    selectitemList.push(statusSelectItem3);

    let statusSelectItem4 = new StatusSelectItem();
    statusSelectItem4.label = 'CANCEL';
    statusSelectItem4.value = StatusDescriptionConstants.CANCEL;
    selectitemList.push(statusSelectItem4);

    let statusSelectItem5 = new StatusSelectItem();
    statusSelectItem5.label = 'AMBIGIOUS';
    statusSelectItem5.value = StatusDescriptionConstants.AMBIGIOUS;
    selectitemList.push(statusSelectItem5);

    let statusSelectItem6 = new StatusSelectItem();
    statusSelectItem6.label = 'REVERSED';
    statusSelectItem6.value = StatusDescriptionConstants.REVERSED;
    selectitemList.push(statusSelectItem6);

    let statusSelectItem7 = new StatusSelectItem();
    statusSelectItem7.label = 'ESEWA TRANSACTION PENDING';
    statusSelectItem7.value = StatusDescriptionConstants.ESEWA_TRANSACTION_PENDING;
    selectitemList.push(statusSelectItem7);

    let statusSelectItem8 = new StatusSelectItem();
    statusSelectItem8.label = 'ESEWA SETTLED';
    statusSelectItem8.value = StatusDescriptionConstants.ESEWA_SETTLED;
    selectitemList.push(statusSelectItem8);

    let statusSelectItem9 = new StatusSelectItem();
    statusSelectItem9.label = 'ESEWA SETTLEMENT FAILED';
    statusSelectItem9.value = StatusDescriptionConstants.ESEWA_SETTLEMENT_FAILED;
    selectitemList.push(statusSelectItem9);

    let statusSelectItem10 = new StatusSelectItem();
    statusSelectItem10.label = 'SETTLEMENT NOT REQUIRED';
    statusSelectItem10.value = StatusDescriptionConstants.SETTLEMENT_NOT_REQUIRED;
    selectitemList.push(statusSelectItem10);

    let statusSelectItem11 = new StatusSelectItem();
    statusSelectItem11.label = 'REFUNDED';
    statusSelectItem11.value = StatusDescriptionConstants.REFUNDED;
    selectitemList.push(statusSelectItem11);

    let statusSelectItem12 = new StatusSelectItem();
    statusSelectItem12.label = 'DO REVERSAL';
    statusSelectItem12.value = StatusDescriptionConstants.DO_REVERSAL;
    selectitemList.push(statusSelectItem12);

    return selectitemList;
  }

  public static getStagessForSearchRequest(): StatusSelectItem[] {
    let selectitemList: StatusSelectItem[] = new Array();
    let statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = 'MERCHANT REQUEST';
    statusSelectItem1.value = StatusDescriptionConstants.MERCHANT_REQUEST;
    selectitemList.push(statusSelectItem1);

    let statusSelectItem2 = new StatusSelectItem();
    statusSelectItem2.label = 'ESEWA VALIDATE';
    statusSelectItem2.value = StatusDescriptionConstants.ESEWA_VALIDATE;
    selectitemList.push(statusSelectItem2);

    let statusSelectItem3 = new StatusSelectItem();
    statusSelectItem3.label = 'CLIENT REQUEST';
    statusSelectItem3.value = StatusDescriptionConstants.CLIENT_REQUEST;
    selectitemList.push(statusSelectItem3);


    let statusSelectItem5 = new StatusSelectItem();
    statusSelectItem5.label = 'CLIENT VERIFY REQUEST';
    statusSelectItem5.value = StatusDescriptionConstants.CLIENT_VERIFY_REQUEST;
    selectitemList.push(statusSelectItem5);

    let statusSelectItem6 = new StatusSelectItem();
    statusSelectItem6.label = 'CLIENT VERIFY RESPONSE';
    statusSelectItem6.value = StatusDescriptionConstants.CLIENT_VERIFY_RESPONSE;
    selectitemList.push(statusSelectItem6);

    let statusSelectItem7 = new StatusSelectItem();
    statusSelectItem7.label = 'MERCHANT VERIFY REQUEST';
    statusSelectItem7.value = StatusDescriptionConstants.MERCHANT_VERIFY_REQUEST;
    selectitemList.push(statusSelectItem7);

    let statusSelectItem8 = new StatusSelectItem();
    statusSelectItem8.label = 'TRANSACTION COMPLETED';
    statusSelectItem8.value = StatusDescriptionConstants.TRANSACTION_COMPLETED;
    selectitemList.push(statusSelectItem8);

    let statusSelectItem9 = new StatusSelectItem();
    statusSelectItem9.label = 'TRANSACTION FAILED';
    statusSelectItem9.value = StatusDescriptionConstants.TRANSACTION_FAILED;
    selectitemList.push(statusSelectItem9);

    let statusSelectItem10 = new StatusSelectItem();
    statusSelectItem10.label = 'ESEWA SETTLED';
    statusSelectItem10.value = StatusDescriptionConstants.ESEWA_SETTLED;
    selectitemList.push(statusSelectItem10);

    let statusSelectItem11 = new StatusSelectItem();
    statusSelectItem11.label = 'WAITING SETTLEMENT NOTIFICATION';
    statusSelectItem11.value = StatusDescriptionConstants.WAITING_SETTLEMENT_NOTIFICATION;
    selectitemList.push(statusSelectItem11);

    let statusSelectItem12 = new StatusSelectItem();
    statusSelectItem12.label = 'DO REVERSAL';
    statusSelectItem12.value = StatusDescriptionConstants.DO_REVERSAL;
    selectitemList.push(statusSelectItem12);

    let statusSelectItem13 = new StatusSelectItem();
    statusSelectItem13.label = 'REVERSED';
    statusSelectItem13.value = StatusDescriptionConstants.REVERSED;
    selectitemList.push(statusSelectItem13);

    let statusSelectItem14 = new StatusSelectItem();
    statusSelectItem14.label = 'REVERSAL DUE TO CLIENT ISSUE';
    statusSelectItem14.value = StatusDescriptionConstants.REVERSAL_DUE_TO_CLIENT_ISSUE;
    selectitemList.push(statusSelectItem14);

    let statusSelectItem4 = new StatusSelectItem();
    statusSelectItem4.label = 'REVERSAL DUE TO MERCHANT ISSUE';
    statusSelectItem4.value = StatusDescriptionConstants.REVERSAL_DUE_TO_MERCHANT_ISSUE;
    selectitemList.push(statusSelectItem4);

    let statusSelectItem15 = new StatusSelectItem();
    statusSelectItem15.label = 'SETTLEMENT NOT REQUIRED';
    statusSelectItem15.value = StatusDescriptionConstants.SETTLEMENT_NOT_REQUIRED;
    selectitemList.push(statusSelectItem15);

    let statusSelectItem16 = new StatusSelectItem();
    statusSelectItem16.label = 'REFUNDED';
    statusSelectItem16.value = StatusDescriptionConstants.REFUNDED;
    selectitemList.push(statusSelectItem16);

    return selectitemList;
  }

  // public static getStagessForSearchSendReversalRequest(): StatusSelectItem[] {
  //   let selectitemList: StatusSelectItem[] = [];
  //
  //
  //   let statusSelectItem3 = new StatusSelectItem();
  //   statusSelectItem3.label = 'CLIENT REQUEST';
  //   statusSelectItem3.value = StatusDescriptionConstants.CLIENT_REQUEST;
  //   selectitemList.push(statusSelectItem3);
  //
  //
  //   let statusSelectItem9 = new StatusSelectItem();
  //   statusSelectItem9.label = 'TRANSACTION FAILED';
  //   statusSelectItem9.value = StatusDescriptionConstants.TRANSACTION_FAILED;
  //   selectitemList.push(statusSelectItem9);
  //
  //
  //   return selectitemList;
  // }

  public static getStagesForSuspiciousFilter(): StatusSelectItem[] {
    let selectitemList: StatusSelectItem[] = new Array();
    let statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = 'REVERSAL DUE TO CLIENT ISSUE';
    statusSelectItem1.value = StatusDescriptionConstants.REVERSAL_DUE_TO_CLIENT_ISSUE;
    selectitemList.push(statusSelectItem1);

    let statusSelectItem2 = new StatusSelectItem();
    statusSelectItem2.label = 'REVERSAL DUE TO MERCHANT ISSUE';
    statusSelectItem2.value = StatusDescriptionConstants.REVERSAL_DUE_TO_MERCHANT_ISSUE;
    selectitemList.push(statusSelectItem2);

    return selectitemList;
  }

  public static getStatusForEsewaSettlementLogFilter(): StatusSelectItem[] {
    let selectitemList: StatusSelectItem[] = new Array();
    let statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = 'ESEWA SETTLEMENT FAILED';
    statusSelectItem1.value = StatusDescriptionConstants.ESEWA_SETTLEMENT_FAILED;
    selectitemList.push(statusSelectItem1);

    let statusSelectItem2 = new StatusSelectItem();
    statusSelectItem2.label = 'ESEWA TRANSACTION PENDING';
    statusSelectItem2.value = StatusDescriptionConstants.ESEWA_TRANSACTION_PENDING;
    selectitemList.push(statusSelectItem2);

    return selectitemList;
  }


  public static getStatusOfDiscountForFilter(): StatusSelectItem[] {
    let selectitemList: StatusSelectItem[] = new Array();
    let statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = 'ACTIVE';
    statusSelectItem1.value = 'CREATED';
    selectitemList.push(statusSelectItem1);

    let statusSelectItem3 = new StatusSelectItem();
    statusSelectItem3.label = 'IN ACTIVE';
    statusSelectItem3.value = 'BLOCKED';
    selectitemList.push(statusSelectItem3);

    return selectitemList;
  }

  public static inActiveschemeStatusList =
    [
      'BLOCKED',
      'DELETED'
    ];

  public static isDiscountActiveActive(statusDescription: string): boolean {
    if (statusDescription) {
      statusDescription = statusDescription.toLocaleUpperCase();
      if (this.inActiveschemeStatusList.includes(statusDescription)) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  public static getTypeList(): StatusSelectItem[] {
    let selectitemList: StatusSelectItem[] = new Array();
    let statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = 'Discount';
    statusSelectItem1.value = 'DISCOUNT';
    selectitemList.push(statusSelectItem1);

    let statusSelectItem3 = new StatusSelectItem();
    statusSelectItem3.label = 'Cashback';
    statusSelectItem3.value = 'CASHBACK';
    selectitemList.push(statusSelectItem3);

    return selectitemList;
  }

  public static getModesSuspiciousFilter(): StatusSelectItem[] {
    let selectitemList: StatusSelectItem[] = new Array();
    let statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = 'REVERSAL';
    statusSelectItem1.value = StatusDescriptionConstants.REVERSAL;
    selectitemList.push(statusSelectItem1);

    let statusSelectItem2 = new StatusSelectItem();
    statusSelectItem2.label = 'REFUND';
    statusSelectItem2.value = StatusDescriptionConstants.REFUND;
    selectitemList.push(statusSelectItem2);

    return selectitemList;
  }

  public static getInitiatorModuleForFilter(): StatusSelectItem[] {
    let selectitemList: StatusSelectItem[] = new Array();
    let statusSelectItem1 = new StatusSelectItem();
    statusSelectItem1.label = 'APP';
    statusSelectItem1.value = 'APP';
    selectitemList.push(statusSelectItem1);

    let statusSelectItem3 = new StatusSelectItem();
    statusSelectItem3.label = 'WEB';
    statusSelectItem3.value = 'WEB';
    selectitemList.push(statusSelectItem3);

    return selectitemList;
  }

  public static getIsMasterProfile(merchantProfile: any) {
    if (merchantProfile) {
      if (merchantProfile.isMasterProfile == true) {
        return 'Yes';
      } else if (merchantProfile.isMasterProfile == false) {
        return 'No'
      } else {
        return 'N/A';
      }
    } else {
      return 'N/A';
    }
  }
}
