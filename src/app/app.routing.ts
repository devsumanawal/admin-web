import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Import Containers
import { FullLayoutComponent } from './containers';
import { AppRoutes } from 'app/constants/app-routes';
import { AuthGuardService } from './module/core/guard/auth-guard.service';
import { ChangePasswordComponent } from 'app/module/change-password/change-password.component';
import { IsFirstLoginGuard } from 'app/module/core/guard/is-first-login.guard';

export const routes: Routes = [
    {
        path: '',
        loadChildren: './module/landing/landing.module#LandingModule'
    },
    {
        path: '',
        loadChildren: './module/pages/page.module#PageModule'
    },
    {
        path: '',
        canActivate: [AuthGuardService],
        children: [
            {
                path: AppRoutes.CHANGE_PASSWORD,
                canActivate: [IsFirstLoginGuard],
                component: ChangePasswordComponent
            },
            {
                path: '',
                component: FullLayoutComponent,
                children: [
                    {
                        path: AppRoutes.ADMIN,
                        loadChildren: 'app/module/admin/admin.module#AdminModule'
                    },
                    {
                        path: AppRoutes.ADMIN_DETAIL,
                        loadChildren: 'app/module/admin-details/admin-details.module#AdminDetailsModule'
                    },
                    {
                        path: AppRoutes.DASHBOARD,
                        loadChildren: 'app/module/dashboard/dashboard.module#DashboardModule'
                    },
                    {
                        path: AppRoutes.META_TABLE,
                        loadChildren: 'app/module/meta-table/meta-table.module#MetaTableModule'
                    },
                    {
                        path: AppRoutes.MANAGE_PASSWORD_POLICY,
                        loadChildren: 'app/module/password-policy/password-policy.module#PasswordPolicyModule'
                    },
                    {
                        path: AppRoutes.ADMIN_PROFILE,
                        loadChildren: 'app/module/admin-profile/admin-profile.module#AdminProfileModule'
                    },
                    {
                        path: AppRoutes.OFFICE_TYPE,
                        loadChildren: 'app/module/office-type/office-type.module#OfficeTypeModule'
                    },
                    {
                        path: AppRoutes.USER_GROUP,
                        loadChildren: 'app/module/user-group/user-group.module#UserGroupModule'
                    },
                    {
                        path: AppRoutes.OFFICE,
                        loadChildren: 'app/module/office/office.module#OfficeModule'
                    },
                    {
                        path: AppRoutes.DEPARTMENT,
                        loadChildren: 'app/module/department/department.module#DepartmentModule'
                    },
                    {
                        path: AppRoutes.MINISTRY,
                        loadChildren: 'app/module/ministry/ministry.module#MinistryModule'
                    },
                    {
                        path: AppRoutes.CURRENCY,
                        loadChildren: 'app/module/currency/currency.module#CurrencyModule'
                    },
                    {
                        path: AppRoutes.SOURCE,
                        loadChildren: 'app/module/source/source.module#SourceModule'
                    },
                    {
                        path: AppRoutes.LINE_ITEM,
                        loadChildren: 'app/module/line-item/line-item.module#LineItemModule'
                    },
                    {
                        path: AppRoutes.DONOR,
                        loadChildren: 'app/module/donor/donor.module#DonorModule'
                    },
                    {
                        path: AppRoutes.FISCAL_YEAR,
                        loadChildren: 'app/module/fiscal-year/fiscal-year.module#FiscalYearModule'
                    },
                    {
                        path: AppRoutes.PROJECT_DATA,
                        loadChildren: 'app/module/project/project.module#ProjectModule'
                    },
                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
