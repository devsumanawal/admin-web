import { NgModule } from '@angular/core';

import { SourceRoutingModule } from './source-routing.module';
import { SourceCreateComponent } from './create/source-create.component';
import { ViewSourceResolverService } from './shared/resolvers/view-source-resolver.service';
import { ManageSourceResolverService } from './shared/resolvers/manage-source-resolver.service';
import { EditSourceResolverService } from './shared/resolvers/edit-source-resolver.service';
import { SourceDetailsComponent } from './details/source-details.component';
import { SourceService } from 'app/module/source/shared/service/source.service';
import { SharedModule } from 'app/module/shared/shared.module';
import { SourceManageComponent } from 'app/module/source/manage/source-manage.component';
import { SourceDetailsGuardService } from 'app/module/source/shared/guard/source-details-guard.service';
import { SourceEditComponent } from './manage/edit/source-edit.component';


@NgModule({
  imports: [
    SharedModule,
    SourceRoutingModule,
  ],
  declarations: [
    SourceCreateComponent,
    SourceManageComponent,
    SourceEditComponent,
    SourceDetailsComponent
  ],
  providers: [
    SourceService,
    SourceDetailsGuardService,
    ViewSourceResolverService,
    ManageSourceResolverService,
    EditSourceResolverService,
  ]
})
export class SourceModule { }
