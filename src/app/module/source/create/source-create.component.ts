import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';

import { ToastrService } from 'ngx-toastr';
import { SourceCreateModel } from '../shared/model/source-create-model';
import { SourceService } from '../shared/service/source.service';


@Component({
  selector: 'app-source-create',
  templateUrl: './source-create.component.html'
})
export class SourceCreateComponent implements OnInit {
  @ViewChild('createSourceForm')
  createSourceForm: any;
  source: SourceCreateModel = new SourceCreateModel();
  serverErrorMessages: string[] = [];
  buttonPressed = false;

  constructor(
    private location: Location,
    private sourceService: SourceService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
  }

  create(form: NgForm) {
    this.buttonPressed = true;
    this.source.status="Y";
    this.sourceService
      .create(this.source)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.resetForm();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (form.form.contains(error.fieldType)) {
                form.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }


  back() {
    this.location.back();
  }

  resetForm() {
    this.source = new SourceCreateModel();
    this.createSourceForm.form.markAsPristine();
    this.createSourceForm.form.markAsUntouched();
  }


}
