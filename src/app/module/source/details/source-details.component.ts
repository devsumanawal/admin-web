import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { SourceService } from '../shared/service/source.service';
import { SourceDTO } from '../shared/model/source-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';


@Component({
  selector: 'app-source-details',
  templateUrl: './source-details.component.html',
  styleUrls: ['./source-details.component.scss']
})
export class SourceDetailsComponent implements OnInit {

  source: SourceDTO;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ngRoute: Router,
    private sourceService: SourceService,
    private location: Location
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.LINE_ITEM]);
      }

      this.firstAlreadyLoaded = true;

    });

    this.source = this.sourceService.getSelectedSource();
  }

  ngOnDestroy() {
    this.sourceService.setSelectedSource(null);
  }

  goBack() {
    this.location.back();
  }

  isActive(statusDescription: string) {
    return statusDescription === "Y";
  }
}