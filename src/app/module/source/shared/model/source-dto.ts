import { ConvergentAdminLoginDto } from "app/module/admin/shared/model/convergent-admin-login-dto";


export class SourceDTO {
    id: number;
    name: string;
    unicodeName: string;
    code: string;
    status: string;
}