import { DateUtil } from 'app/module/core/util/date-util.ts';

export class SourceCreateModel {

  name: string;
  unicodeName: string;
  code: string;
  status: string;
}
