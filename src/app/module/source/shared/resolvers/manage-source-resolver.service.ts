import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { SourceService } from '../service/source.service';

@Injectable()
export class ManageSourceResolverService implements Resolve<any> {
  constructor(
    private sourceService: SourceService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.sourceService.getAllSourcesForManage().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
