import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { SourceDTO } from '../model/source-dto';
import { SourceCreateModel } from '../model/source-create-model';
import { SourceEditModel } from '../model/source-edit-model';

@Injectable()
export class SourceService {

  selectedSource: SourceDTO;

  constructor(private http: HttpClient) { }


  setSelectedSource(source: SourceDTO) {
    this.selectedSource = source;
  }

  getSelectedSource(): SourceDTO {
    return this.selectedSource;
  }

  getAllSources() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.SOURCE);
  }

  searchSources(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.SOURCE + ApiConstants.SEARCH, searchParam);
  }

  searchSourcesForManage() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.SOURCE + ApiConstants.MANAGE);
  }

  getAllSourceForCreate() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.SOURCE);
  }

  getAllSourcesForManage() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.SOURCE + ApiConstants.MANAGE);
  }

  getSourceById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.SOURCE + '/' + id);
  }

  create(sourceCreateModel: SourceCreateModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.SOURCE, sourceCreateModel);
  }

  edit(id: number, sourceEditModel: SourceEditModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.SOURCE + ApiConstants.MODIFY + '/' + id, sourceEditModel);
  }

}
