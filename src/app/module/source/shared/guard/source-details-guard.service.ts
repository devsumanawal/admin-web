import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { SourceService } from '../service/source.service';

@Injectable()
export class SourceDetailsGuardService implements CanActivate {

  constructor(
    private sourceService: SourceService,
    public router: Router
  ) { }

  canActivate() {
    if (this.sourceService.getSelectedSource() == null) {
      this.router.navigate([AppRoutes.SOURCE]);
      return false;
    } else {
      return true;
    }
  }



}
