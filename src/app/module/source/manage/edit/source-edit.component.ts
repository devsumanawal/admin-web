import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SourceService } from 'app/module/source/shared/service/source.service';
import { SourceEditModel } from 'app/module/source/shared/model/source-edit-model';
import { AppRoutes } from 'app/constants/app-routes';
import { SourceDTO } from '../../shared/model/source-dto';

@Component({
  selector: 'app-source-edit',
  templateUrl: './source-edit.component.html',
  styleUrls: ['./source-edit.component.scss']
})
export class SourceEditComponent implements OnInit {

  editSource: SourceEditModel = new SourceEditModel();
  sourceId: number;
  @ViewChild('editSourceForm') editSourceForm: NgForm;
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private sourceService: SourceService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.LINE_ITEM]);
      }

      this.firstAlreadyLoaded = true;
      this.sourceId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditSource();
  }

  ngOnDestroy() {
    this.sourceService.setSelectedSource(null);
  }

  setEditSource() {
    const sourceDto: SourceDTO = this.sourceService.getSelectedSource();
    this.editSource.name = sourceDto.name;
    this.editSource.unicodeName = sourceDto.unicodeName;
    this.editSource.code = sourceDto.code;
    this.editSource.status = sourceDto.status;
  }

  submitEditSource() {

    this.buttonPressed = true;

    this.sourceService
      .edit(this.sourceId, this.editSource)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editSourceForm.form.contains(error.fieldType)) {
                this.editSourceForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}
