import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { CommonModule } from '@angular/common';
import { SourceDetailsComponent } from './details/source-details.component';
import { SourceEditComponent } from './manage/edit/source-edit.component';
import { EditSourceResolverService } from './shared/resolvers/edit-source-resolver.service';
import { SourceDetailsGuardService } from './shared/guard/source-details-guard.service';
import { SourceManageComponent } from './manage/source-manage.component';
import { SourceCreateComponent } from './create/source-create.component';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'Source'
    },
    children: [
      {
        path: '',
        component: SourceManageComponent,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.CREATE,
        component: SourceCreateComponent,
        pathMatch: 'full',

      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: SourceManageComponent,
            pathMatch: 'full',

          },
          {
            path: AppRoutes.EDIT_ID,
            component: SourceEditComponent,
            pathMatch: 'full',
            resolve: {
              lsourceDto: EditSourceResolverService,
            }
          }
        ]
      },
      {
        path: AppRoutes.DETAILS,
        component: SourceDetailsComponent,
        pathMatch: 'full',
        canActivate: [
          SourceDetailsGuardService
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class SourceRoutingModule { }
