import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { CurrencyCreateComponent } from 'app/module/currency/create/currency-create.component';
import { CommonModule } from '@angular/common';
import { CurrencyManageComponent } from 'app/module/currency/manage/currency-manage.component';
import { CurrencyEditComponent } from './manage/edit/currency-edit.component';
import { EditCurrencyResolverService } from './shared/resolvers/edit-currency-resolver.service';
import { ViewCurrencyResolverService } from './shared/resolvers/view-currency-resolver.service';
import { ManageCurrencyResolverService } from './shared/resolvers/manage-currency-resolver.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'currency'
    },
    children: [
      {
        path: '',
        component: CurrencyManageComponent,
        pathMatch: 'full',
        resolve: {
          userGroups: ViewCurrencyResolverService
        }
      },
      {
        path: AppRoutes.CREATE,
        component: CurrencyCreateComponent,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: CurrencyManageComponent,
            pathMatch: 'full',
            resolve: {
              userGroups: ManageCurrencyResolverService
            }
          },
          {
            path: AppRoutes.EDIT_ID,
            component: CurrencyEditComponent,
            pathMatch: 'full',
            resolve: {
              userGroupDto: EditCurrencyResolverService,
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class CurrencyRoutingModule { }
