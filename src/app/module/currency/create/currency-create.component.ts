import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToastrService } from 'ngx-toastr';
import { CurrencyCreateModel } from 'app/module/currency/shared/model/currency-create-model';
import { CurrencyService } from '../shared/service/currency.service';


@Component({
    selector: 'app-currency-create',
    templateUrl: './currency-create.component.html'
})
export class CurrencyCreateComponent implements OnInit {

    @ViewChild('createCurrencyForm')
    createCurrencyForm: any;

    currency: CurrencyCreateModel = new CurrencyCreateModel();
    serverErrorMessages: string[] = [];
    buttonPressed = false;
    profiles = [];

    constructor(
        private location: Location,
        private currencyService: CurrencyService,
        private toastr: ToastrService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    create(form: NgForm) {
        this.buttonPressed = true;
        this.currencyService
            .create(this.currency)
            .subscribe(response => {
                this.toastr.success(response.message);
                this.resetForm();
                this.buttonPressed = false;
            },
                errorResponse => {
                    this.buttonPressed = false;
                    if (errorResponse.error.message) {
                        this.toastr.error(errorResponse.error.message);
                    } else {
                        errorResponse.error.forEach(error => {
                            if (form.form.contains(error.fieldType)) {
                                form.controls[error.fieldType].setErrors({ server: true });
                                this.serverErrorMessages[error.fieldType] = error.message;
                            } else {
                                this.toastr.error(error.message);
                            }
                        });
                    }
                });
    }


    back() {
        this.location.back();
    }

    resetForm() {
        this.currency = new CurrencyCreateModel();
        this.createCurrencyForm.form.markAsPristine();
        this.createCurrencyForm.form.markAsUntouched();
    }

}
