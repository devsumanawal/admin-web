import { NgModule } from '@angular/core';


import { SharedModule } from 'app/module/shared/shared.module';
import { CurrencyEditComponent } from './manage/edit/currency-edit.component';
import { CurrencyCreateComponent } from './create/currency-create.component';
import { CurrencyManageComponent } from './manage/currency-manage.component';
import { CurrencyService } from './shared/service/currency.service';
import { ViewCurrencyResolverService } from './shared/resolvers/view-currency-resolver.service';
import { ManageCurrencyResolverService } from './shared/resolvers/manage-currency-resolver.service';
import { EditCurrencyResolverService } from './shared/resolvers/edit-currency-resolver.service';
import { CurrencyRoutingModule } from './currency-routing.module';
import { CurrencyDetailsGuardService } from './shared/guard/currency-details-guard.service';


@NgModule({
  imports: [
    SharedModule,
    CurrencyRoutingModule
  ],
  declarations: [
    CurrencyCreateComponent,
    CurrencyEditComponent,
    CurrencyManageComponent
  ],
  providers: [
    CurrencyService,
    CurrencyDetailsGuardService,
    ViewCurrencyResolverService,
    ManageCurrencyResolverService,
    EditCurrencyResolverService
  ]
})
export class CurrencyModule { }
