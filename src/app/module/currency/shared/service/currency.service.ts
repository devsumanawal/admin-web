import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { CurrencyDTO } from '../model/currency-dto';
import { CurrencyCreateModel } from '../model/currency-create-model';
import { CurrencyEditModel } from '../model/currency-edit-model';


@Injectable()
export class CurrencyService {

  selectedCurrency: CurrencyDTO;;

  constructor(private http: HttpClient) { }

  getAllCurrency() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.CURRENCY);
  }

  searchAllCurrencys(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.CURRENCY + ApiConstants.SEARCH, searchParam);
  }

  create(currencyCreateModel: CurrencyCreateModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.CURRENCY, currencyCreateModel);
  }

  edit(id, currencyEditModel: CurrencyEditModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.CURRENCY + ApiConstants.MODIFY + '/' + id, currencyEditModel);
  }

  getCurrencyById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.CURRENCY + '/' + id);
  }

  setSelectedCurrency(selectedCurrency: CurrencyDTO) {
    this.selectedCurrency = selectedCurrency;
  }

  getSelectedCurrency(): CurrencyDTO {
    return this.selectedCurrency;
  }
}
