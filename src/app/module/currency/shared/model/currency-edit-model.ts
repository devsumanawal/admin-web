export class CurrencyEditModel {
  id: number;
  name: string;
  code: string;
}
