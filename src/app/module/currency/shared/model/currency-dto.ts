export class CurrencyDTO {
    id: number;
    name: string;
    code: string;
}