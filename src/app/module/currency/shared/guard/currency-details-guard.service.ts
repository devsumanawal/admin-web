import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { CurrencyService } from '../service/currency.service';

@Injectable()
export class CurrencyDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.officeService.getSelectedCurrency() == null) {
      this.router.navigate([AppRoutes.DEPARTMENT]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private officeService: CurrencyService,
    public router: Router
  ) { }

}
