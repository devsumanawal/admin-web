import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { CurrencyService } from '../service/currency.service';

@Injectable()
export class ViewCurrencyResolverService implements Resolve<Observable<any>> {
  constructor(
    private officeService: CurrencyService,
    private toastr: ToastrService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.officeService.getAllCurrency().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
