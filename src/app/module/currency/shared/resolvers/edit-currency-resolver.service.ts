import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { CurrencyService } from '../service/currency.service';

@Injectable()
export class EditCurrencyResolverService implements Resolve<Observable<any>> {
  constructor(
    private currencyService: CurrencyService,
    private toastr: ToastrService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.currencyService.getCurrencyById(route.paramMap.get('id')).catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
