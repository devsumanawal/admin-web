import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'app/constants/app-routes';
import { CurrencyDTO } from '../../shared/model/currency-dto';
import { CurrencyEditModel } from '../../shared/model/currency-edit-model';
import { CurrencyService } from '../../shared/service/currency.service';

@Component({
  selector: 'app-currency-edit',
  templateUrl: './currency-edit.component.html'
})

export class CurrencyEditComponent implements OnInit {

  editCurrency: CurrencyEditModel = new CurrencyEditModel();
  currencyId: number;


  @ViewChild('editCurrencyForm')
  editCurrencyForm: NgForm;

  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private currencyService: CurrencyService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.CURRENCY]);
      }

      this.firstAlreadyLoaded = true;
      this.currencyId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditCurrency();
  }

  ngOnDestroy() {
    this.currencyService.setSelectedCurrency(null);
  }

  setEditCurrency() {
    const currencyDTO: CurrencyDTO = this.currencyService.getSelectedCurrency();
    this.editCurrency.name = currencyDTO.name;
    this.editCurrency.code = currencyDTO.code;
  }

  submitEditCurrency() {
    this.buttonPressed = true;

    this.currencyService
      .edit(this.currencyId, this.editCurrency)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editCurrencyForm.form.contains(error.fieldType)) {
                this.editCurrencyForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}