import { NgModule } from '@angular/core';


import { SharedModule } from 'app/module/shared/shared.module';
import { ActivatedRouteSnapshot } from '@angular/router';
import { DepartmentRoutingModule } from './department-routing.module';
import { DepartmentEditComponent } from './manage/edit/department-edit.component';
import { DepartmentCreateComponent } from './create/department-create.component';
import { DepartmentManageComponent } from './manage/department-manage.component';
import { DepartmentService } from './shared/service/department.service';
import { DepartmentDetailsGuardService } from './shared/guard/department-details-guard.service';
import { ViewDepartmentResolverService } from './shared/resolvers/view-department-resolver.service';
import { ManageDepartmentResolverService } from './shared/resolvers/manage-department-resolver.service';
import { EditDepartmentResolverService } from './shared/resolvers/edit-department-resolver.service';


@NgModule({
  imports: [
    SharedModule,
    DepartmentRoutingModule
  ],
  declarations: [
    DepartmentCreateComponent,
    DepartmentEditComponent,
    DepartmentManageComponent
  ],
  providers: [
    DepartmentService,
    DepartmentDetailsGuardService,
    ViewDepartmentResolverService,
    ManageDepartmentResolverService,
    EditDepartmentResolverService
  ]
})
export class DepartmentModule { }
