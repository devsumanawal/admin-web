import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective, PaginationComponent } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { AppRoutes } from 'app/constants/app-routes';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { DepartmentDTO } from '../shared/model/department-dto';
import { DepartmentService } from '../shared/service/department.service';

@Component({
  selector: 'app-department-manage',
  templateUrl: './department-manage.component.html'
})
export class DepartmentManageComponent implements OnInit {
  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
  departments: Array<DepartmentDTO> = [];

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private departmentService: DepartmentService,
    private route: ActivatedRoute,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.resetSearch();
    this.departmentService.setSelectedDepartment(null);
  }

  resetSearch() {
    this.filterParam = [
      { key: 'name', value: '', label: 'Name', alias: 'Name' },
      { key: 'code', value: '', label: 'Code', alias: 'Code' },
    ];
    this.searchDepartmentOnUserInput();
  }

  searchDepartmentOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllDepartmentForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllDepartmentForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }

    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };

    this.departmentService
      .searchAllDepartments(this.searchParam)
      .subscribe(successResponse => {
        this.departments = successResponse.object;
        this.totalItems = successResponse.totalCount;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
        });
    this.searchButtonpressed = false;
  }

  clickViewDetailButton(departmentDto: DepartmentDTO) {
    this.departmentService.setSelectedDepartment(departmentDto);
    this.router.navigate([AppRoutes.DEPARTMENT, AppRoutes.DETAILS]);
  }

  clickedEditDepartment(departmentDto: DepartmentDTO) {
    this.departmentService.setSelectedDepartment(departmentDto);
    this.router.navigate([AppRoutes.DEPARTMENT, AppRoutes.MANAGE, AppRoutes.EDIT, departmentDto.id]);
  }


  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  goBack() {
    this.location.back();
  }


  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('Department', 'Department Setup', 'Manage Department', childRoleName);
  }

  isActive(active: string) {
    return active === "Y";
  }
}
