import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'app/constants/app-routes';
import { DepartmentDTO } from '../../shared/model/department-dto';
import { DepartmentEditModel } from '../../shared/model/department-edit-model';
import { DepartmentService } from '../../shared/service/department.service';
import { MinistryService } from 'app/module/core/service/office/ministry.service';

@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html'})

export class DepartmentEditComponent implements OnInit {

  editDepartment: DepartmentEditModel = new DepartmentEditModel();
  departmentId: number;


  @ViewChild('editDepartmentForm')
  editDepartmentForm: NgForm;

  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private departmentService: DepartmentService,
    private ministryService: MinistryService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.OFFICE]);
      }

      this.firstAlreadyLoaded = true;
      this.departmentId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditDepartment();
  }

  ngOnDestroy() {
    this.departmentService.setSelectedDepartment(null);
  }

  setEditDepartment() {
    const departmentDTO: DepartmentDTO = this.departmentService.getSelectedDepartment();
    this.editDepartment.name = departmentDTO.name;
    this.editDepartment.unicodeName = departmentDTO.unicodeName;
    this.editDepartment.code = departmentDTO.code;
    this.editDepartment.status = departmentDTO.status;
    this.editDepartment.ministryId = departmentDTO.ministryDto.id;
  }

  submitEditDepartment() {
    this.buttonPressed = true;

    this.departmentService
      .edit(this.departmentId, this.editDepartment)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editDepartmentForm.form.contains(error.fieldType)) {
                this.editDepartmentForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}