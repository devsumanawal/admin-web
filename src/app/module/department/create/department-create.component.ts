import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToastrService } from 'ngx-toastr';
import { DepartmentCreateModel } from 'app/module/department/shared/model/department-create-model';
import { DepartmentService } from '../shared/service/department.service';
import { MinistryService } from 'app/module/core/service/office/ministry.service';


@Component({
    selector: 'app-department-create',
    templateUrl: './department-create.component.html'
})
export class DepartmentCreateComponent implements OnInit {

    @ViewChild('createDepartmentForm')
    createDepartmentForm: any;

    department: DepartmentCreateModel = new DepartmentCreateModel();
    serverErrorMessages: string[] = [];
    buttonPressed = false;
    profiles = [];

    constructor(
        private location: Location,
        private departmentService: DepartmentService,
        private ministryService: MinistryService,
        private toastr: ToastrService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    create(form: NgForm) {
        this.buttonPressed = true;
        this.department.status="Y";
        this.departmentService
            .create(this.department)
            .subscribe(response => {
                this.toastr.success(response.message);
                this.resetForm();
                this.buttonPressed = false;
            },
                errorResponse => {
                    this.buttonPressed = false;
                    if (errorResponse.error.message) {
                        this.toastr.error(errorResponse.error.message);
                    } else {
                        errorResponse.error.forEach(error => {
                            if (form.form.contains(error.fieldType)) {
                                form.controls[error.fieldType].setErrors({ server: true });
                                this.serverErrorMessages[error.fieldType] = error.message;
                            } else {
                                this.toastr.error(error.message);
                            }
                        });
                    }
                });
    }


    back() {
        this.location.back();
    }

    resetForm() {
        this.department = new DepartmentCreateModel();
        this.createDepartmentForm.form.markAsPristine();
        this.createDepartmentForm.form.markAsUntouched();
    }

}
