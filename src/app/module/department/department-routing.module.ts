import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { DepartmentCreateComponent } from 'app/module/department/create/department-create.component';
import { CommonModule } from '@angular/common';
import { DepartmentManageComponent } from 'app/module/department/manage/department-manage.component';
import { DepartmentEditComponent } from './manage/edit/department-edit.component';
import { EditDepartmentResolverService } from './shared/resolvers/edit-department-resolver.service';
import { ViewDepartmentResolverService } from './shared/resolvers/view-department-resolver.service';
import { ManageDepartmentResolverService } from './shared/resolvers/manage-department-resolver.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'department'
    },
    children: [
      {
        path: '',
        component: DepartmentCreateComponent,
        pathMatch: 'full',
        resolve: {
          userGroups: ViewDepartmentResolverService
        }
      },
      {
        path: AppRoutes.CREATE,
        component: DepartmentCreateComponent,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: DepartmentManageComponent,
            pathMatch: 'full',
            resolve: {
              userGroups: ManageDepartmentResolverService
            }
          },
          {
            path: AppRoutes.EDIT_ID,
            component: DepartmentEditComponent,
            pathMatch: 'full',
            resolve: {
              userGroupDto: EditDepartmentResolverService,
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class DepartmentRoutingModule { }
