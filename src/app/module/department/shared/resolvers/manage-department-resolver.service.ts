import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { DepartmentService } from '../service/department.service';

@Injectable()
export class ManageDepartmentResolverService implements Resolve<any> {
  constructor(
    private officeService: DepartmentService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.officeService.getAllDepartment().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
