import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { DepartmentDTO } from '../model/department-dto';
import { DepartmentCreateModel } from '../model/department-create-model';
import { DepartmentEditModel } from '../model/department-edit-model';


@Injectable()
export class DepartmentService {

  selectedDepartment: DepartmentDTO;;

  constructor(private http: HttpClient) { }

  getAllDepartment() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.DEPARTMENT);
  }

  searchAllDepartments(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.DEPARTMENT + ApiConstants.SEARCH, searchParam);
  }

  create(departmentCreateModel: DepartmentCreateModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.DEPARTMENT, departmentCreateModel);
  }

  edit(id, departmentEditModel: DepartmentEditModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.DEPARTMENT + ApiConstants.MODIFY + '/' + id, departmentEditModel);
  }

  getDepartmentById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.DEPARTMENT + '/' + id);
  }

  setSelectedDepartment(selectedDepartment: DepartmentDTO) {
    this.selectedDepartment = selectedDepartment;
  }

  getSelectedDepartment(): DepartmentDTO {
    return this.selectedDepartment;
  }

  searchDepartmentForManage(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.DEPARTMENT + ApiConstants.MANAGE + ApiConstants.SEARCH, searchParam);
  }
}
