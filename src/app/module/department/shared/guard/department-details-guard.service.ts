import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { DepartmentService } from '../service/department.service';

@Injectable()
export class DepartmentDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.officeService.getSelectedDepartment() == null) {
      this.router.navigate([AppRoutes.DEPARTMENT]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private officeService: DepartmentService,
    public router: Router
  ) { }

}
