export class DepartmentEditModel {
  id: number;
  name: string;
  unicodeName: string;
  code: string;
  status: string;
  ministryId: number;
}
