export class DepartmentDTO {
    id: number;
    name: string;
    code: string;
    status: string;
    unicodeName: string;
    ministryDto: MinistryDTO;
}

export class MinistryDTO {
    id: number;
    name: string;
    code: string;
    unicodeName: string;
}