
export class DepartmentCreateModel {

  name: string;
  unicodeName: string;
  code: string;
  ministryId: number;
  status: string;
}
