import {SearchParam} from '../../core/models/search-param';

export class FonepaySearchUtil {

  public static getValueFromSearchParam(searchParam: SearchParam, key: string) {
    const matchSearchParam = searchParam.search.filter(obj => {
      return obj.key === key
    });
    if (matchSearchParam && matchSearchParam[0]) {
      return matchSearchParam[0].value;
    } else {
      return null;
    }
  }

}
