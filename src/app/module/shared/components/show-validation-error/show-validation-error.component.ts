import { Component, OnInit, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-show-validation-error',
  templateUrl: './show-validation-error.component.html',
  styleUrls: ['./show-validation-error.component.scss']
})
export class ShowValidationErrorComponent implements OnInit {

  private static readonly errorMessage = {
    'required': () => 'is required',
    'minlength': (params) => 'min number of characters is ' + params.requiredLength,
    'maxlength': (params) => 'max allowed number of characters is ' + params.requiredLength,
    'pattern': (params) => ' is Invalid',
    'email': () => 'must be valid',
    'validateEqual': (params) => 'And ' + params.notEqualWith + ' does not match',
    'server': (params) => params,
    'url': (params) => 'Enter valid url',
  }

  @Input()
  control: AbstractControl;

  @Input()
  isEditForm: Boolean = false;

  constructor() { }

  ngOnInit() {
    this.removeServerValidationErrorOnChange();
  }

  removeServerValidationErrorOnChange() {
    this.control.valueChanges.forEach(value => {
      if (this.control.getError('server')) {
        this.control.setErrors(null);
      }
    });
  }

  shouldShowError(): boolean {
    if (this.isEditForm) {
      if (this.control && this.control.errors) {
        return true;
      } else {
        return false;
      }
    } else {
      return this.control && this.control.errors && this.control.dirty;
    }
  }

  listOfErrors(): string[] {
    if (this.control.errors) {
      return Object.keys(this.control.errors)
        .filter(field => {
          return this.control.errors[field] !== null;
        })
        .map(field => {
          return this.getMessage(field, this.control.errors[field]);
        }
        );
    }
  }

  getMessage(type: string, params: any) {
    if (type === 'server' || type === 'url') {
      return ShowValidationErrorComponent.errorMessage[type](params);
    } else {
      return this.getFormattedControlName() + ShowValidationErrorComponent.errorMessage[type](params);
    }
  }

  getFormattedControlName() {
    return this.getControlName().charAt(0).toUpperCase() + this.getControlName().slice(1) + ' ';
  }

  getControlName(): string | null {
    const formGroup = this.control.parent.controls;
    return Object.keys(formGroup).find(name => this.control === formGroup[name]) || null;
  }
}
