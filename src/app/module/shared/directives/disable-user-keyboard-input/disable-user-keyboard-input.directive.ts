import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appDisableUserKeyboardInput]'
})
export class DisableUserKeyboardInputDirective {
  constructor() { }

  @HostListener('keydown', ['$event'])
  public onKeyDown(event: any): void {
    event.preventDefault();
  }
}
