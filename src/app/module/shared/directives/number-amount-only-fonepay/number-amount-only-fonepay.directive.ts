import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appNumberAmountOnlyFonepay]'
})
export class NumberAmountOnlyFonepayDirective {
  constructor() { }

  @HostListener('paste', ['$event'])
  @HostListener('keydown', ['$event'])
  public onKeyDownAndPasteForNumberOnlyForAmount(event: any): boolean {
    return this.numberOnlyForAmount(event);
  }

  numberOnlyForAmount(event): boolean {
    if (event.type == "paste") {

      if (isNaN(event.clipboardData.getData('Text'))) {
        // Do not Allow to paste non number
        return false;
      } else {
        // Only Allow to paste number
        return true;
      }
    }

    const charCode = (event.which) ? event.which : event.keyCode;

    if (event.ctrlKey || event.metaKey) {
      // Allow ctrl,alt key
      return true;
    }

    if (charCode === 46 || charCode == 110 || charCode == 190) {
      // Allow to enter . (190 period , 110 decimal point), del (46)
      if (event.target.value && event.target.value.split('.').length < 2) {
        // Allow only one . (allow only one decimal point)
        return true;
      } else {
        return false;
      }
    }

    if (charCode > 95 && charCode < 106) {
      // Allow numpad number
      return true;
    }

    if (charCode == 110 || charCode == 190) {
      // Allow to enter . (190 period , 110 decimal point)
      return true;
    }

    if (charCode == 38 || charCode == 39 || charCode == 37 || charCode == 40) {
      // Allow to use arrow key
      return true;
    }

    if (charCode != 8 && charCode != 0 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      // Do not allow non number
      return false;
    }

    return true;
  }
}
