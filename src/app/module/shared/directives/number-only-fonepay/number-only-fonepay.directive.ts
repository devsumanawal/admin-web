import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appNumberOnlyFonepay]'
})
export class NumberOnlyFonepayDirective {
  constructor() { }

  @HostListener('paste', ['$event'])
  @HostListener('keydown', ['$event'])
  public onKeyDownAndPaste(event: any): boolean {
    return this.numberOnly(event);
  }

  numberOnly(event): boolean {
    if (event.type == "paste") {

      if (event.clipboardData.getData('Text').match(/[^\d]/)) {
        return false;
      } else {
        return true;
      }

    }

    const charCode = (event.which) ? event.which : event.keyCode;

    if (event.ctrlKey || event.metaKey) {
      return true;
    }

    if (charCode > 95 && charCode < 106) {
      return true;
    }

    if (charCode == 38 || charCode == 39 || charCode == 37 || charCode == 40) {
      // Allow to use arrow key
      return true;
    }

    if (charCode != 8 && charCode != 0 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }

    return true;
  }
}
