import { Directive, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { NgControl, NgForm } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Directive({
  selector: '[onDebounce]'
})
export class DebounceDirective implements OnInit {
  @Output()
  public onDebounce = new EventEmitter<any>();

  debounceTime = 1200;
  firstRequest: Boolean = true;

  constructor(public model: NgForm) {
  }

  ngOnInit() {
    this.onDebounce.emit(this.model.value);

    this.model.valueChanges
      .debounceTime(this.debounceTime)
      .distinctUntilChanged((x, y) => {
        return JSON.stringify(x) === JSON.stringify(y);
      })
      .subscribe(modelValue => {
        if (!this.firstRequest) {
          this.onDebounce.emit(modelValue);
        }
        this.firstRequest = false;
      });
  }

}
