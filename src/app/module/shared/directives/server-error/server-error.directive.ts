import { Directive, HostListener } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appServerError]'
})
export class ServerErrorDirective {
  constructor() { }

  @HostListener('change') onChange(control: AbstractControl) {
    if (control) {
      control.setErrors({ server: false });
    }
  }
}
