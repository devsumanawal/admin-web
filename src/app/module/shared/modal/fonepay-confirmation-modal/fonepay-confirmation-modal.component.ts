import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-fonepay-confirmation-modal',
  templateUrl: './fonepay-confirmation-modal.component.html',
  exportAs: 'fonepayConfirmationModalComp'
})
export class FonepayConfirmationModalComponent implements OnInit {

  @ViewChild('confirmationModal') confirmationModal: ModalDirective;
  @Output() sendConfirmationObject = new EventEmitter<any>();

  isButtonPressed: boolean;
  confirmationMsg: string;
  confirmationObject: any;

  constructor() { }

  ngOnInit() {
  }


  hideModal() {
    this.confirmationModal.hide();
  }

  showModal(object: any, message: string) {
    this.isButtonPressed = false;
    this.confirmationMsg = message;
    this.confirmationObject = object;
    this.confirmationModal.show();
  }

  confirm() {
    this.isButtonPressed = true;
    this.sendConfirmationObject.emit(this.confirmationObject);
    this.hideModal();
  }

}
