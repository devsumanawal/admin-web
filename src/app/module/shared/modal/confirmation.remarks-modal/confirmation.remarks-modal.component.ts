import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-confimation-remarks-modal',
  templateUrl: './confirmation.remarks-modal.component.html',
  exportAs: 'confirmationRemarksModal'
})
export class ConfirmationRemarksModalComponent implements OnInit {

  @ViewChild('confirmationRemarksModal') confirmationRemarksModal: ModalDirective;
  @Output() sendConfirmationObject = new EventEmitter<any>();

  isButtonPressed: boolean;
  confirmationModalForm: FormGroup;
  confirmationMsg: string;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createConfirmationtModalForm();
  }

  createConfirmationtModalForm() {
    this.confirmationModalForm = this.formBuilder.group({
      id: [],
      other: '',
      remarks: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(150)]]
    })
  }

  hideModal() {
    this.confirmationRemarksModal.hide();
  }

  showModal(object: any, action: string) {
    this.isButtonPressed = false;
    this.confirmationMsg = 'Are you sure, you want to ' + action  + (object.username ? object.username : object.name ? object.name : '');
    this.confirmationModalForm.controls['id'].setValue(object.id);
    this.confirmationModalForm.controls['other'].setValue(action);
    this.confirmationModalForm.controls['remarks'].setValue(null);
    this.confirmationRemarksModal.show();
  }

  confirm() {
    this.isButtonPressed = true;
    this.sendConfirmationObject.emit(this.confirmationModalForm.value);
    this.hideModal();
  }

}
