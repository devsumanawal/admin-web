import { Component, OnInit, ViewChild, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss'],
  exportAs: 'deleteModalComp'
})
export class DeleteModalComponent implements OnInit {
  @ViewChild('deleteModal') deleteModal: ModalDirective;
  @Output() sendDeleteObject = new EventEmitter<any>();
  deleteModalForm: FormGroup;
  deletingObject: any;
  buttonPressed: boolean;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createDeleteModalForm();
  }

  createDeleteModalForm() {
    this.deleteModalForm = this.formBuilder.group({
      id: [],
      remarks: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(150)]]
    })
  }

  showModal(object: any) {
    this.buttonPressed = false;
    this.deletingObject = object;
    this.deleteModalForm.controls['id'].setValue(object.id);
    this.deleteModalForm.controls['remarks'].setValue(null);
    this.deleteModalForm.markAsPristine();
    this.deleteModal.show();
  }

  hideModal() {
    this.deleteModalForm.reset();
    this.deleteModal.hide();
  }

  confirmRemove() {
    this.buttonPressed = true;
    this.sendDeleteObject.emit(this.deleteModalForm.value);
    this.hideModal();
    this.buttonPressed = false;
  }
}
