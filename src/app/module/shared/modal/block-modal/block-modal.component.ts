import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-block-modal',
  templateUrl: './block-modal.component.html',
  exportAs: 'blockModalComp'
})
export class BlockModalComponent implements OnInit {

  @ViewChild('blockModal') blockModal: ModalDirective;
  @Output() sendBlockObject = new EventEmitter<any>();

  isButtonPressed: boolean;
  blockModalForm: FormGroup;
  blockingObject: any;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createBlockModalForm();
  }

  createBlockModalForm() {
    this.blockModalForm = this.formBuilder.group({
      id: [],
      remarks: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(150)]]
    })
  }

  hideModal() {
    this.blockModal.hide();
  }

  showModal(object: any) {
    this.isButtonPressed = false;
    this.blockingObject = object;
    this.blockModalForm.controls['id'].setValue(this.blockingObject.id);
    this.blockModalForm.controls['remarks'].setValue(null);
    this.blockModalForm.markAsPristine();
    this.blockModal.show();
  }

  confirmBlock() {
    this.isButtonPressed = true;
    this.sendBlockObject.emit(this.blockModalForm.value);
    this.hideModal();
  }

}
