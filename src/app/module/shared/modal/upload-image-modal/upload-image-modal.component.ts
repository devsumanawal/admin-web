import { OnInit, Component, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'app-upload-image-modal',
    templateUrl: './upload-image-modal.component.html',
    exportAs: 'uploadImageModalComp'
})
export class UploadImageModalComponent implements OnInit {

    @ViewChild('uploadImageModal') uploadImageModal: ModalDirective;
    @Output() sendUploadImageObject = new EventEmitter<any>();
    @ViewChild('fileInput') fileInput;
    clientImageURL: string;

    isButtonPressed: boolean;
    uploadImageModalForm: FormGroup;
    uploadImageObject: any;

    constructor(
        private formBuilder: FormBuilder
    ) { };

    ngOnInit() {
        this.createUploadImageModalForm();
    }

    createUploadImageModalForm() {
        this.uploadImageModalForm = this.formBuilder.group({
            id: [],
            photo: [null, Validators.required]
        })
    }

    hideModal() {
        this.uploadImageModal.hide();
    }

    showModal(object: any, imageURL) {
        this.clearPhotoFile();
        this.clientImageURL = imageURL;
        this.isButtonPressed = false;
        this.uploadImageObject = object;
        this.uploadImageModalForm.controls['id'].setValue(this.uploadImageObject.id);
        this.uploadImageModal.show();
    }

    confirmUploadImage() {
        this.isButtonPressed = true;
        this.sendUploadImageObject.emit(this.uploadImageModalForm);
        this.hideModal();
    }

    onFileChange(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.uploadImageModalForm.get('photo').setValue(file);

            const fileReader = new FileReader();
            fileReader.onload = (eventFileReader: any) => {
                this.clientImageURL = eventFileReader.target.result;
            }
            fileReader.readAsDataURL(file);
        } else {
            this.uploadImageModalForm.get('photo').setValue(null);
            this.clientImageURL = null;
        }
    }

    clearPhotoFile() {
        this.uploadImageModalForm.get('photo').setValue(null);
        this.clientImageURL = null;
        this.fileInput.nativeElement.value = '';
    }
}
