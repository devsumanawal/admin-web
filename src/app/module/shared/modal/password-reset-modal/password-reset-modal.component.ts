import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-password-reset-modal',
  templateUrl: './password-reset-modal.component.html',
  styleUrls: ['./password-reset-modal.component.scss'],
  exportAs: 'passwordResetModalComp'
})
export class PasswordResetModalComponent implements OnInit {

  @ViewChild('passwordResetModal') passwordResetModal: ModalDirective;
  @Output() sendPasswordResetObject = new EventEmitter<any>();

  isButtonPressed: boolean;
  passwordResetModalForm: FormGroup;
  passwordResetObject: any;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createPasswordResetModalForm();
  }

  createPasswordResetModalForm() {
    this.passwordResetModalForm = this.formBuilder.group({
      id: [],
      remarks: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(150)]]
    })
  }

  hideModal() {
    this.passwordResetModal.hide();
  }

  showModal(object: any) {
    this.isButtonPressed = false;
    this.passwordResetObject = object;
    this.passwordResetModalForm.controls['id'].setValue(this.passwordResetObject.id);
    this.passwordResetModalForm.controls['remarks'].setValue(null);
    this.passwordResetModalForm.markAsPristine();
    this.passwordResetModal.show();
  }

  confirmPasswordReset() {
    this.isButtonPressed = true;
    this.sendPasswordResetObject.emit(this.passwordResetModalForm.value);
    this.hideModal();
  }

}
