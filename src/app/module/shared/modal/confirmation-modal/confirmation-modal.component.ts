import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-confimation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss'],
  exportAs: 'confirmationModalComp'
})
export class ConfirmationModalComponent implements OnInit {

  @ViewChild('confirmationModal') confirmationModal: ModalDirective;
  @Output() sendConfirmationObject = new EventEmitter<any>();

  isButtonPressed: boolean;
  confirmationModalForm: FormGroup;
  confirmationMsg: string;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createConfirmationtModalForm();
  }

  createConfirmationtModalForm() {
    this.confirmationModalForm = this.formBuilder.group({
      id: [],
      other: '',
      remarks: ''
    })
  }

  hideModal() {
    this.confirmationModal.hide();
  }

  showModal(object: any, action: string) {
    this.isButtonPressed = false;
    this.confirmationMsg = 'Are you sure, you want to ' + action + ' ' + (object.username ? object.username : object.name ? object.name : '');
    this.confirmationModalForm.controls['id'].setValue(object.id);
    this.confirmationModalForm.controls['other'].setValue(action);
    this.confirmationModalForm.controls['remarks'].setValue('NONE');
    this.confirmationModal.show();
  }

  confirm() {
    this.isButtonPressed = true;
    this.sendConfirmationObject.emit(this.confirmationModalForm.value);
    this.hideModal();
  }

}