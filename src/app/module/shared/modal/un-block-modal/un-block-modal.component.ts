import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-un-block-modal',
  templateUrl: './un-block-modal.component.html',
  styleUrls: ['./un-block-modal.component.scss'],
  exportAs: 'unBlockModalComp'
})
export class UnBlockModalComponent implements OnInit {
  @ViewChild('unBlockModal') unBlockModal: ModalDirective;
  @Output() sendUnBlockObject = new EventEmitter<any>();
  isButtonPressed: boolean;
  unBlockModalForm: FormGroup;
  unBlockingObject: any;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createUnBlockModalForm();
  }

  createUnBlockModalForm() {
    this.unBlockModalForm = this.formBuilder.group({
      id: [],
      remarks: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(150)]]
    })
  }

  hideModal() {
    this.unBlockModal.hide();
  }

  showModal(object: any) {
    this.isButtonPressed = false;
    this.unBlockingObject = object;
    this.unBlockModalForm.controls['id'].setValue(this.unBlockingObject.id);
    this.unBlockModalForm.controls['remarks'].setValue(null);
    this.unBlockModalForm.markAsPristine();
    this.unBlockModal.show();
  }

  confirmBlock() {
    this.isButtonPressed = true;
    this.sendUnBlockObject.emit(this.unBlockModalForm.value);
    this.hideModal();
  }

}
