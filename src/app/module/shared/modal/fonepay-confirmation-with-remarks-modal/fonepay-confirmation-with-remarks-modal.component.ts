import {Component, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-fonepay-confirmation-with-remarks-modal',
  templateUrl: './fonepay-confirmation-with-remarks-modal.component.html',
  exportAs: 'fonepayConfirmationWithRemarksModalComp'
})
export class FonepayConfirmationWithRemarksComponent implements OnInit {

  @ViewChild('confirmationModal') confirmationModal: ModalDirective;
  @Output() sendConfirmationObject = new EventEmitter<any>();

  isButtonPressed: boolean;
  confirmationModalForm: FormGroup;
  message: any;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.createBlockModalForm();
  }

  createBlockModalForm() {
    this.confirmationModalForm = this.formBuilder.group({
      object: [],
      remarks: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(100)]]
    })
  }

  hideModal() {
    this.confirmationModal.hide();
  }

  showModal(object: any, message: any) {
    this.isButtonPressed = false;
    this.message = message;
    this.confirmationModalForm.controls['object'].setValue(object);
    this.confirmationModalForm.controls['remarks'].setValue(null);
    this.confirmationModalForm.markAsPristine();
    this.confirmationModal.show();
  }

  confirmBlock() {
    this.isButtonPressed = true;
    this.sendConfirmationObject.emit(this.confirmationModalForm.value);
    this.hideModal();
  }

}
