import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ModalModule, PaginationModule, TabsModule, TimepickerModule, TooltipModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ServerErrorDirective } from 'app/module/shared/directives/server-error/server-error.directive';
import { BlockModalComponent } from './modal/block-modal/block-modal.component';
import { DeleteModalComponent } from './modal/delete-modal/delete-modal.component';
import { EqualValidator } from 'app/directives/equal-validator';
import { UnBlockModalComponent } from 'app/module/shared/modal/un-block-modal/un-block-modal.component';
import { ClickStopPropagationDirective } from 'app/module/shared/directives/click-stop-propagation/click-stop-propagation.directive';
import { UploadImageModalComponent } from 'app/module/shared/modal/upload-image-modal/upload-image-modal.component';
import { NoRecordsFoundComponent } from 'app/module/shared/components/no-records-found/no-records-found.component';
import { ShowValidationErrorComponent } from './components/show-validation-error/show-validation-error.component';
import { DebounceDirective } from 'app/module/shared/directives/debounce-directive';
import { 
    DisableUserKeyboardInputDirective 
} from 'app/module/shared/directives/disable-user-keyboard-input/disable-user-keyboard-input.directive';
import { PasswordResetModalComponent } from 'app/module/shared/modal/password-reset-modal/password-reset-modal.component';
import { ConfirmationModalComponent } from 'app/module/shared/modal/confirmation-modal/confirmation-modal.component';
import { VerticalTimelineModule } from 'angular-vertical-timeline';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { NgProgressModule } from 'ngx-progressbar';
import { NumberOnlyFonepayDirective } from './directives/number-only-fonepay/number-only-fonepay.directive';
import { NumberAmountOnlyFonepayDirective } from './directives/number-amount-only-fonepay/number-amount-only-fonepay.directive';
import { ConfirmationRemarksModalComponent } from './modal/confirmation.remarks-modal/confirmation.remarks-modal.component';
import { FonepayConfirmationModalComponent } from './modal/fonepay-confirmation-modal/fonepay-confirmation-modal.component';
import { 
    FonepayConfirmationWithRemarksComponent 
} from './modal/fonepay-confirmation-with-remarks-modal/fonepay-confirmation-with-remarks-modal.component';
import { NoDefaultCommissionComponent } from './components/no-default-commission/no-default-commission.component';
import { NoServiceProvidersComponent } from './components/no-service-providers/no-service-providers.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        TooltipModule.forRoot(),
        TabsModule.forRoot(),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        PaginationModule.forRoot(),
        RecaptchaModule.forRoot(),
        RecaptchaFormsModule,
        NgxQRCodeModule,
        ModalModule,
        NgProgressModule
    ],
    declarations: [
        NoRecordsFoundComponent,
        EqualValidator,
        ServerErrorDirective,
        DebounceDirective,
        ClickStopPropagationDirective,
        DisableUserKeyboardInputDirective,
        NumberOnlyFonepayDirective,
        NumberAmountOnlyFonepayDirective,
        DeleteModalComponent,
        BlockModalComponent,
        UnBlockModalComponent,
        UploadImageModalComponent,
        PasswordResetModalComponent,
        ShowValidationErrorComponent,
        ConfirmationModalComponent,
        ConfirmationRemarksModalComponent,
        FonepayConfirmationModalComponent,
        FonepayConfirmationWithRemarksComponent,
        NoDefaultCommissionComponent,
        NoServiceProvidersComponent
    ],
    exports: [
        NoRecordsFoundComponent,
        EqualValidator,
        ServerErrorDirective,
        DebounceDirective,
        ClickStopPropagationDirective,
        DisableUserKeyboardInputDirective,
        NumberOnlyFonepayDirective,
        NumberAmountOnlyFonepayDirective,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        TooltipModule,
        TabsModule,
        TimepickerModule,
        BsDatepickerModule,
        ModalModule,
        DeleteModalComponent,
        BlockModalComponent,
        UnBlockModalComponent,
        UploadImageModalComponent,
        PasswordResetModalComponent,
        ShowValidationErrorComponent,
        PaginationModule,
        NgxQRCodeModule,
        ConfirmationModalComponent,
        RecaptchaModule,
        RecaptchaFormsModule,
        VerticalTimelineModule,
        NgProgressModule,
        ConfirmationRemarksModalComponent,
        FonepayConfirmationModalComponent,
        FonepayConfirmationWithRemarksComponent,
        NoDefaultCommissionComponent,
        NoServiceProvidersComponent
    ]
})
export class SharedModule {
}
