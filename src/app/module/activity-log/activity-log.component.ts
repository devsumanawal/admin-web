import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ToastrService } from 'ngx-toastr';
import { ActivityLogViewModel } from 'app/module/activity-log/shared/activity-log-view-model';
import { ActivityLogServiceService } from 'app/module/activity-log/shared/activity-log-service.service';
import { StartEndDateModel } from 'app/module/core/models/start-end-date-model';
import { DateUtil } from 'app/module/core/util/date-util.ts';

@Component({
  selector: 'app-activity-log',
  templateUrl: './activity-log.component.html',
  styleUrls: ['./activity-log.component.scss']
})
export class ActivityLogComponent implements OnInit {
  activityLogList: Array<ActivityLogViewModel> = [];
  dateModel: StartEndDateModel = new StartEndDateModel();
  maxDate = DateUtil.getTodayDate();
  buttonPressed = false;

  constructor(
    private activityLogService: ActivityLogServiceService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  loadActivityLogComponent() {
    this.activityLogList = Array<ActivityLogViewModel>();
    this.dateModel = new StartEndDateModel();
    this.getAdminActivityLog();
  }

  getAdminActivityLog() {
    const startEndDateModified: StartEndDateModel = new StartEndDateModel();
    startEndDateModified.startDate = new Date(this.dateModel.startDate);
    startEndDateModified.endDate = new Date(this.dateModel.endDate);

    startEndDateModified.endDate.setDate(startEndDateModified.endDate.getDate() + 1);
    this.setDateHourMinSecToZero(startEndDateModified);

    if (startEndDateModified.startDate >= startEndDateModified.endDate) {
      this.toastr.error('Start Date Is After End Date');
      return;
    }

    this.buttonPressed = true;
    this.activityLogService.getActivityLog(startEndDateModified)
      .subscribe((successResponse) => {
        this.buttonPressed = false;
        this.activityLogList = successResponse;
        DateUtil.dateSort(this.activityLogList);
      },
        (errorResponse) => {
          this.buttonPressed = false;
          this.toastr.error(errorResponse.error.message);
        });
  }

  private setDateHourMinSecToZero(startEndDateModified: StartEndDateModel) {
    startEndDateModified.startDate.setHours(0);
    startEndDateModified.startDate.setMinutes(0);
    startEndDateModified.startDate.setSeconds(0);
    startEndDateModified.endDate.setHours(0);
    startEndDateModified.endDate.setMinutes(0);
    startEndDateModified.endDate.setSeconds(0);
  }
}
