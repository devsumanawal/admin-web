import { NgModule } from '@angular/core';

import { ActivityLogServiceService } from './shared/activity-log-service.service';
import { ActivityLogComponent } from './activity-log.component';
import { SharedModule } from 'app/module/shared/shared.module';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [ActivityLogComponent],
  providers: [ActivityLogServiceService],
  exports: [ActivityLogComponent]
})
export class ActivityLogModule { }
