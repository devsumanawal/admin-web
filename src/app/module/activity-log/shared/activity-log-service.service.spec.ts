import { TestBed, inject } from '@angular/core/testing';

import { ActivityLogServiceService } from './activity-log-service.service';

describe('ActivityLogServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityLogServiceService]
    });
  });

  it('should be created', inject([ActivityLogServiceService], (service: ActivityLogServiceService) => {
    expect(service).toBeTruthy();
  }));
});
