import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { ApiConstants } from 'app/constants/api-constants';
import { ActivityLogViewModel } from './activity-log-view-model';
import { StartEndDateModel } from 'app/module/core/models/start-end-date-model';

@Injectable()
export class ActivityLogServiceService {
  constructor(private http: HttpClient) { }

  getActivityLog(startEndDateModified: StartEndDateModel) {
    return this.http.post<ActivityLogViewModel[]>(
      ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.ADMIN_ACTIVITY,
      startEndDateModified
    );
  }
}
