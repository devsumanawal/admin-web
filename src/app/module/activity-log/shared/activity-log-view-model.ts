export class ActivityLogViewModel {
  version: string;
  device: string;
  description: string;
  ipAddress: string;
  recordedDate: Date;
}
