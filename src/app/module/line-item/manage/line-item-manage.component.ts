import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective, PaginationComponent } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { AppRoutes } from 'app/constants/app-routes';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { LineItemDTO } from '../shared/model/line-item-dto';
import { LineItemService } from '../shared/service/line-item.service';

@Component({
  selector: 'app-line-item-manage',
  templateUrl: './line-item-manage.component.html',
  styleUrls: ['./line-item-manage.component.scss']
})
export class LineItemManageComponent implements OnInit {
  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
  lineItems: Array<LineItemDTO> = [];

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private lineItemService: LineItemService,
    private route: ActivatedRoute,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.lineItemService.setSelectedLineItem(null);
    this.getAllLineItemForSearch(null);
  }

  searchLineItemOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllLineItemForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllLineItemForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }

    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };

    this.lineItemService
      .searchLineItems(this.searchParam)
      .subscribe(successResponse => {
        this.lineItems = successResponse;
        this.totalItems = successResponse.totalCount;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
        });
    this.searchButtonpressed = false;
  }

  clickViewDetailButton(lineItemDto: LineItemDTO) {
    this.lineItemService.setSelectedLineItem(lineItemDto);
    this.router.navigate([AppRoutes.LINE_ITEM, AppRoutes.DETAILS]);
  }

  clickedEditLineItem(lineItemDto: LineItemDTO) {
    this.lineItemService.setSelectedLineItem(lineItemDto);
    this.router.navigate([AppRoutes.LINE_ITEM, AppRoutes.MANAGE, AppRoutes.EDIT, lineItemDto.id]);
  }


  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  goBack() {
    this.location.back();
  }


  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('Line Item', 'Line Item Setup', 'Manage Line Item', childRoleName);
  }


  isActive(active: string) {
    return active === "Y";
  }

}
