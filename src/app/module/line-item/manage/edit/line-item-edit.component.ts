import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LineItemService } from 'app/module/line-item/shared/service/line-item.service';
import { LineItemEditModel } from 'app/module/line-item/shared/model/line-item-edit-model';
import { AppRoutes } from 'app/constants/app-routes';
import { LineItemDTO } from '../../shared/model/line-item-dto';

@Component({
  selector: 'app-line-item-edit',
  templateUrl: './line-item-edit.component.html',
  styleUrls: ['./line-item-edit.component.scss']
})
export class LineItemEditComponent implements OnInit {

  editLineItem: LineItemEditModel = new LineItemEditModel();
  lineItemId: number;
  @ViewChild('editLineItemForm') editLineItemForm: NgForm;
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private lineItemService: LineItemService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.LINE_ITEM]);
      }

      this.firstAlreadyLoaded = true;
      this.lineItemId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditLineItem();
  }

  ngOnDestroy() {
    this.lineItemService.setSelectedLineItem(null);
  }

  setEditLineItem() {
    const lineItemDto: LineItemDTO = this.lineItemService.getSelectedLineItem();
    console.log('Line Item data::: ', lineItemDto);
    this.editLineItem.name = lineItemDto.name;
    this.editLineItem.unicodeName = lineItemDto.unicodeName;
    this.editLineItem.code = lineItemDto.code;
    this.editLineItem.type = lineItemDto.type;
    this.editLineItem.enabled = lineItemDto.enabled;
  }

  submitEditLineItem() {

    this.buttonPressed = true;
    console.log('Edit Line Item::: ', this.editLineItem);

    this.lineItemService
      .edit(this.lineItemId, this.editLineItem)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editLineItemForm.form.contains(error.fieldType)) {
                this.editLineItemForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}
