import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { ViewLineItemResolverService } from 'app/module/line-item/shared/resolvers/view-line-item-resolver.service';
import { LineItemCreateComponent } from 'app/module/line-item/create/line-item-create.component';
// import { AdminProfileForCreateResolverService } from 'app/module/admin/shared/resolvers/admin-profile-for-create-resolver.service';
import { ManageLineItemResolverService } from 'app/module/line-item/shared/resolvers/manage-line-item-resolver.service';
import { CommonModule } from '@angular/common';
import { LineItemManageComponent } from 'app/module/line-item/manage/line-item-manage.component';
import { LineItemDetailsComponent } from './details/line-item-details.component';
import { LineItemEditComponent } from './manage/edit/line-item-edit.component';
import { EditLineItemResolverService } from './shared/resolvers/edit-line-item-resolver.service';
import { LineItemDetailsGuardService } from './shared/guard/line-item-details-guard.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'LineItem'
    },
    children: [
      {
        path: '',
        component: LineItemManageComponent,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.CREATE,
        component: LineItemCreateComponent,
        pathMatch: 'full',
       
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: LineItemManageComponent,
            pathMatch: 'full',
           
          },
          {
            path: AppRoutes.EDIT_ID,
            component: LineItemEditComponent,
            pathMatch: 'full',
            resolve: {
              lineItemDto: EditLineItemResolverService,
            }
          }
        ]
      },
      {
        path: AppRoutes.DETAILS,
        component: LineItemDetailsComponent,
        pathMatch: 'full',
        canActivate: [
          LineItemDetailsGuardService
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class LineItemRoutingModule { }
