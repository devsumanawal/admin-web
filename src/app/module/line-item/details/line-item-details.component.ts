import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { LineItemService } from '../shared/service/line-item.service';
import { LineItemDTO } from '../shared/model/line-item-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';


@Component({
  selector: 'app-line-item-details',
  templateUrl: './line-item-details.component.html',
  styleUrls: ['./line-item-details.component.scss']
})
export class LineItemDetailsComponent implements OnInit {

  lineItem: LineItemDTO;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ngRoute: Router,
    private lineItemService: LineItemService,
    private location: Location
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.LINE_ITEM]);
      }

      this.firstAlreadyLoaded = true;

    });

    this.lineItem = this.lineItemService.getSelectedLineItem();
  }

  ngOnDestroy() {
    this.lineItemService.setSelectedLineItem(null);
  }

  goBack() {
    this.location.back();
  }

  isActive(statusDescription: string) {
    return statusDescription === "Y";
  }
}