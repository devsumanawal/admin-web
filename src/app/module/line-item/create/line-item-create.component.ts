import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';

import { ToastrService } from 'ngx-toastr';
import { LineItemCreateModel } from '../shared/model/line-item-create-model';
import { LineItemService } from '../shared/service/line-item.service';


@Component({
  selector: 'app-line-item-create',
  templateUrl: './line-item-create.component.html'
})
export class LineItemCreateComponent implements OnInit {
  @ViewChild('createLineItemForm')
  createLineItemForm: any;
  lineItem: LineItemCreateModel = new LineItemCreateModel();
  serverErrorMessages: string[] = [];
  buttonPressed = false;

  constructor(
    private location: Location,
    private lineItemService: LineItemService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
  }

  create(form: NgForm) {
    this.buttonPressed = true;
    this.lineItemService
      .create(this.lineItem)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.resetForm();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (form.form.contains(error.fieldType)) {
                form.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }


  back() {
    this.location.back();
  }

  resetForm() {
    this.lineItem = new LineItemCreateModel();
    this.createLineItemForm.form.markAsPristine();
    this.createLineItemForm.form.markAsUntouched();
  }


}
