import { NgModule } from '@angular/core';

import { LineItemRoutingModule } from './line-item-routing.module';
import { LineItemCreateComponent } from './create/line-item-create.component';
import { ViewLineItemResolverService } from './shared/resolvers/view-line-item-resolver.service';
import { ManageLineItemResolverService } from './shared/resolvers/manage-line-item-resolver.service';
import { EditLineItemResolverService } from './shared/resolvers/edit-line-item-resolver.service';
import { LineItemDetailsComponent } from './details/line-item-details.component';
import { LineItemService } from 'app/module/line-item/shared/service/line-item.service';
import { SharedModule } from 'app/module/shared/shared.module';
import { LineItemManageComponent } from 'app/module/line-item/manage/line-item-manage.component';
import { LineItemEditComponent } from 'app/module/line-item/manage/edit/line-item-edit.component';
import { ActivatedRouteSnapshot } from '@angular/router';
import { LineItemDetailsGuardService } from 'app/module/line-item/shared/guard/line-item-details-guard.service';


@NgModule({
  imports: [
    SharedModule,
    LineItemRoutingModule,
  ],
  declarations: [
    LineItemCreateComponent,
    LineItemManageComponent,
    LineItemEditComponent,
    LineItemDetailsComponent
  ],
  providers: [
    LineItemService,
    LineItemDetailsGuardService,
    ViewLineItemResolverService,
    ManageLineItemResolverService,
    EditLineItemResolverService,
  ]
})
export class LineItemModule { }
