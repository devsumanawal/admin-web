export class LineItemEditModel {

  name: string;
  unicodeName: string;
  code: string;
  type: string;
  enabled: boolean;
}
