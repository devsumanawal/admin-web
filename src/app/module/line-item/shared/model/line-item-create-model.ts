import { DateUtil } from 'app/module/core/util/date-util.ts';

export class LineItemCreateModel {

  name: string;
  unicodeName: string;
  code: string;
  type: string;
  enabled: boolean;

}
