import { ConvergentAdminLoginDto } from "app/module/admin/shared/model/convergent-admin-login-dto";


export class LineItemDTO {
    id: number;
    name: string;
    unicodeName: string;
    code: string;
    type: string;
    enabled: boolean;
    ccreatedDate: Date;
    modifiedDate: Date;
    createdBy: string;
    modifiedBy: string;
}