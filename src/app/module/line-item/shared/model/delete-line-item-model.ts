export class LineItemDeleteModel {
  id: number;
  remarks: string;
  constructor(id: number, remarks: string) {
    this.id = id;
    this.remarks = remarks;

  }
}
