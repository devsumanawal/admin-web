import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { LineItemService } from '../service/line-item.service';

@Injectable()
export class EditLineItemResolverService implements Resolve<Observable<any>> {
  constructor(
    private lineItemService: LineItemService,
    private toastr: ToastrService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.lineItemService.getLineItemById(route.paramMap.get('id')).catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
