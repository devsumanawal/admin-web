import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { LineItemDTO } from '../model/line-item-dto';
import { LineItemCreateModel } from '../model/line-item-create-model';
import { LineItemEditModel } from '../model/line-item-edit-model';

@Injectable()
export class LineItemService {

  selectedLineItem: LineItemDTO;

  constructor(private http: HttpClient) { }


  setSelectedLineItem(lineItem: LineItemDTO) {
    this.selectedLineItem = lineItem;
  }

  getSelectedLineItem(): LineItemDTO {
    return this.selectedLineItem;
  }

  getAllLineItems() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.LINE_ITEM);
  }

  searchLineItems(searchParam) {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.LINE_ITEM);
  }

  searchLineItemsForManage() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.LINE_ITEM + ApiConstants.MANAGE);
  }

  getAllLineItemForCreate() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.LINE_ITEM);
  }

  getAllLineItemsForManage() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.LINE_ITEM + ApiConstants.MANAGE);
  }

  getLineItemById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.LINE_ITEM + '/' + id);
  }

  create(lineItemCreateModel: LineItemCreateModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.LINE_ITEM, lineItemCreateModel);
  }

  edit(id: number, lineItemEditModel: LineItemEditModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.LINE_ITEM + ApiConstants.MODIFY + '/' + id, lineItemEditModel);
  }

}
