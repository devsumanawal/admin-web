import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { LineItemService } from '../service/line-item.service';

@Injectable()
export class LineItemDetailsGuardService implements CanActivate {

  constructor(
    private lineItemService: LineItemService,
    public router: Router
  ) { }

  canActivate() {
    if (this.lineItemService.getSelectedLineItem() == null) {
      this.router.navigate([AppRoutes.LINE_ITEM]);
      return false;
    } else {
      return true;
    }
  }



}
