import {NgModule} from '@angular/core';
import {ChartsModule} from 'ng2-charts/ng2-charts';

import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashBoardService} from './shared/service/dashboard.service';
import {SharedModule} from 'app/module/shared/shared.module';
import {LoadingModule} from 'ngx-loading';
// Import angular-fusioncharts
import {FusionChartsModule} from 'angular-fusioncharts';
// Import FusionCharts library and chart modules
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';

import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import {QrDashboardComponent} from './inner-components/qr-dashboard/qr-dashboard.component';
import {RewardsCustomerComparisionReportComponent} from './inner-components/reward-point-report/rewards-customer-comparision-report/rewards-customer-comparision-report.component';
import {RewardsCustomerReportComponent} from './inner-components/reward-point-report/rewards-customer-report.component';
import {TotalIssuerTxnsDashboardComponent} from './inner-components/total-issuer-txns-dashboard/total-issuer-txns-dashboard.component';
import {MerchantOwnershipReportComponent} from './inner-components/merchant-ownership-report/merchant-ownership-report.component';
import {IssuedTicketsReportComponent} from './inner-components/issued-airline-ticket/issued-tickets-report/issued-tickets-report.component';
import { UniqueCustomerComponent } from './inner-components/unique-customer/unique-customer.component';
import { UniqueCustomerComparisionComponent } from './inner-components/unique-customer/unique-customer-comparision/unique-customer-comparision.component';


FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

@NgModule({
  imports: [
    DashboardRoutingModule,
    ChartsModule,
    SharedModule,
    FusionChartsModule,
    LoadingModule
  ],
  declarations:
    [DashboardComponent, RewardsCustomerReportComponent, RewardsCustomerComparisionReportComponent, QrDashboardComponent, TotalIssuerTxnsDashboardComponent, MerchantOwnershipReportComponent, IssuedTicketsReportComponent, UniqueCustomerComponent, UniqueCustomerComparisionComponent],
  providers: [DashBoardService]
})
export class DashboardModule {
}
