import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TransactionResponseParam } from './model/transaction-response-param';
import { BackendResponse } from '../core/models/backend-response';
import { Chart } from 'chart.js';
import { ChartResponse } from './model/chart-response';
import { DashBoardService } from './shared/service/dashboard.service';
import { ToastrService } from 'ngx-toastr';
import * as FusionCharts from 'fusioncharts';
import { AppRoutes } from 'app/constants/app-routes';
import { CustomerRegisteredThroughPartnerInRPS } from './model/customer-registered-through-partner-in-RPS';
import { DateUtil } from '../core/util/date-util.ts';
import { DashboardRequest } from "./model/dashboard-request";
import { UniqueCustomerRequest } from './model/unique-customer-request';

@Component({
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

    clientTxnDetails: Array<TransactionResponseParam> = new Array<TransactionResponseParam>();
    merchantTxnDetails: Array<TransactionResponseParam> = new Array<TransactionResponseParam>();
    pieChartResponse: Array<ChartResponse> = new Array<ChartResponse>();
    doughnoutChartResponse: Array<ChartResponse> = new Array<ChartResponse>();
    barChartResponse: Array<ChartResponse> = new Array<ChartResponse>();
    countRegisteredMerchant: BackendResponse;
    countQrTransactions: BackendResponse;
    countRegisteredClient: BackendResponse;
    countSuspiciousRequest: BackendResponse;

    monthlyTotalSuccessfulTxnData: Array<number> = [];
    monthlyQrTxnWeb: Array<number> = [];
    monthlyQrTxnOnline: Array<number> = [];
    monthlyQrTxnOffline: Array<number> = [];
    monthsLabels: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    chart = {};
    fusionChart = new FusionCharts({});


    countRegisteredMerchantError = false;
    countRegisteredMerchantLoading = true;

    countQrTxnError = false;
    countQrTxnLoading = true;

    countRegisteredClientError = false;
    countRegisteredClientLoading = true;

    countSuspiciousTxnError = false;
    countSuspiciousTxnLoading = true;

    loadPrymaidChartCustError = false;
    loadingPrymaidChartCust = true;

    loadPrymaidChartBankError = false;
    loadingPrymaidChartBank = true;

    loadPieChartError = false;
    loadingPieChart = true;

    loadDoughnoutChartError = false;
    loadingDoughnoutChart = true;

    loadBarChartError = false;
    loadingBarChart = true;

    loadLineChartError = false;
    loadingLineChart = true;

    loadScrollableBarChartForClientTxnError = false;
    loadingScrollableBarChartForClientTxn = true;

    loadScrollableBarChartForMerTxnError = false;
    loadingScrollableBarChartForMerTxn = true;

    scrollEventedCalledOnce = false;
    loadRow4_5And6Done = false;

    totalCustomersInRewardPoint = 0;
    loadCustomersInRewardPoint = false;
    loadCustomersInRewardError = false;

    totalAirlineTicketCount = 0;
    loadAirlineTicketCount = false;
    loadAirlineTicketCountError = false;

    totalUniqueCustomer = 0;
    loadUniqueCustomer = false;
    loadUniqueCustomerError = false;

    totalMerchantsInRewardPoint = 0;
    loadMerchantsInRewardPoint = false;
    loadMerchantsInRewardError = false;

    constructor(private route: ActivatedRoute,
        private dashboardService: DashBoardService,
        private toastr: ToastrService,
        private router: Router) {
    }

    ngOnInit() {
        // this.loadRow1And2();
    }

    loadRow1And2() {
        this.getRegisteredMerchantsCount();
        this.getQrTransactionsCount();
        this.getRegisteredClientCount();
        this.getSuspiciousRequestCount();
        this.loadPieChart();
        this.loadDoughnoutChart();
        this.fetchCustomerInRewardPoint();
        this.fetchTodayIssuedAirlineTicket();
        this.fetchUniqueCustomer();
        this.fetchMerchantInRewardPoint();
    }

    loadRow3() {
        this.loadBarChart();
        this.loadLineChart();
    }

    loadRow4_5And6() {
        if (!this.loadRow4_5And6Done) {
            this.loadRow4_5And6Done = true;
            this.loadScrollableBarChartForClientTxnDetails();
            this.loadScrollableBarChartForMerchantDetails();
            this.loadPyramidChartForTop5Cust();
            this.loadPyramidChartForTop5TxnBank();

        }
    }

    getRegisteredMerchantsCount() {
        this.dashboardService.getRegisteredMechantCount().subscribe(
            (data) => {
                this.countRegisteredMerchant = data;
                this.countRegisteredMerchantLoading = false;
            },

            (error) => {
                this.countRegisteredMerchantLoading = false;
                this.countRegisteredMerchantError = true;
            }
        )
    }

    getQrTransactionsCount() {
        this.dashboardService.countAllQrTransactions().subscribe(
            (data) => {
                this.countQrTransactions = data;
                this.countQrTxnLoading = false;
            },

            (error) => {
                this.countQrTxnLoading = false;
                this.countQrTxnError = true;
            }
        )
    }

    getRegisteredClientCount() {
        this.dashboardService.countAllRegisteredClient().subscribe(
            (data) => {
                this.countRegisteredClient = data;
                this.countRegisteredClientLoading = false;
            },
            (error) => {
                this.countRegisteredClientLoading = false;
                this.countRegisteredClientError = true;
            }
        )
    }

    getSuspiciousRequestCount() {
        this.dashboardService.countAllSuspiciousRequest().subscribe(
            (data) => {
                this.countSuspiciousRequest = data;
                this.countSuspiciousTxnLoading = false;
            },
            (error) => {
                this.countSuspiciousTxnLoading = false;
                this.countSuspiciousTxnError = true;
            }
        )
    }


    loadPieChart() {
        this.dashboardService.countAllQrTypeTxn().subscribe(
            (data) => {
                this.pieChartResponse = data;
                this.prepareChartData('pie', 'canvas', this.pieChartResponse);
                this.loadingPieChart = false;
            },
            (error) => {
                this.loadingPieChart = false;
                this.loadPieChartError = true;
            }
        )
    }

    loadDoughnoutChart() {
        this.dashboardService.countMerchantByOwnership().subscribe(
            (data) => {
                this.loadRow3();
                this.doughnoutChartResponse = data;
                this.prepareChartData('doughnut', 'canvas1', this.doughnoutChartResponse);
                this.loadingDoughnoutChart = false;
            },
            (error) => {
                this.loadRow3();
                this.loadingDoughnoutChart = false;
                this.loadDoughnoutChartError = true;
            }
        )
    }

    loadBarChart() {
        this.dashboardService.getMonthlyTotalSuccessfulTxn().subscribe(
            (data) => {
                for (let i = 0; i < data.length; i++) {
                    this.monthlyTotalSuccessfulTxnData.push(data[i]);
                }
                this.getChart('bar', 'barChart', this.monthsLabels, this.monthlyTotalSuccessfulTxnData, '#EE9EB3', 'Total Successful Transactions');
                this.loadingBarChart = false;
            },
            (error) => {
                this.loadingBarChart = false;
                this.loadBarChartError = true;
            }
        )
    }

    loadLineChart() {
        this.dashboardService.getMonthlyQrTxn().subscribe(
            (data) => {
                //Web QR
                for (let i = 0; i < data.webQr.length; i++) {
                    this.monthlyQrTxnWeb.push(data.webQr[i]);
                }

                //Online QR
                for (let i = 0; i < data.onlineQr.length; i++) {
                    this.monthlyQrTxnOnline.push(data.onlineQr[i]);
                }

                //OfflineQR
                for (let i = 0; i < data.offlineQr.length; i++) {
                    this.monthlyQrTxnOffline.push(data.offlineQr[i]);
                }
                this.getLineChart('line', 'lineChart', this.monthsLabels, this.monthlyQrTxnWeb, this.monthlyQrTxnOnline, this.monthlyQrTxnOffline, '#EE9EB3');
                this.loadingLineChart = false;
            },
            (error) => {
                this.loadingLineChart = false;
                this.loadLineChartError = true;
            }
        )
    }

    loadScrollableBarChartForClientTxnDetails() {
        this.dashboardService.getClientTxnDetail().subscribe(
            (data) => {
                this.fusionChart = new FusionCharts({
                    type: 'scrollcolumn2d',
                    renderAt: 'chart-container',
                    width: '1050',
                    height: '400',
                    dataFormat: 'json',
                    dataSource: data,
                });
                this.fusionChart.render();
                this.loadingScrollableBarChartForClientTxn = false;
            },
            (error) => {
                this.loadingScrollableBarChartForClientTxn = false;
                this.loadScrollableBarChartForClientTxnError = true;
            }
        )
    }

    loadScrollableBarChartForMerchantDetails() {
        this.dashboardService.getTop20MerchantTxnDetails().subscribe(
            (data) => {
                this.fusionChart = new FusionCharts({
                    type: 'scrollcolumn2d',
                    renderAt: 'chart-container1',
                    width: '1050',
                    height: '400',
                    dataFormat: 'json',
                    dataSource: data,
                });
                this.fusionChart.render();
                this.loadingScrollableBarChartForMerTxn = false;
            },
            (error) => {
                this.loadingScrollableBarChartForMerTxn = false;
                this.loadScrollableBarChartForMerTxnError = true;
            }
        )
    }

    loadPyramidChartForTop5Cust() {
        this.dashboardService.getTop5FrequentCustomers().subscribe(
            (data) => {
                this.fusionChart = new FusionCharts({
                    type: 'pyramid',
                    renderAt: 'pyramid-container',
                    dataFormat: 'json',
                    dataSource: data,
                });
                this.fusionChart.render();
                this.loadingPrymaidChartCust = false;
            },
            (error) => {
                this.loadingPrymaidChartCust = false;
                this.loadPrymaidChartCustError = true;
            }
        )
    }

    loadPyramidChartForTop5TxnBank() {
        this.dashboardService.getTop5TxnBanks().subscribe(
            (data) => {
                this.fusionChart = new FusionCharts({
                    type: 'pyramid',
                    renderAt: 'pyramid-container1',
                    dataFormat: 'json',
                    dataSource: data,
                });
                this.fusionChart.render();
                this.loadingPrymaidChartBank = false;
            },
            (error) => {
                this.loadingPrymaidChartBank = false;
                this.loadPrymaidChartBankError = true;
            }
        )
    }

    prepareChartData(chartType, chartId, chartResponse) {
        let labels = [];
        let value = [];
        let colour = [];
        for (let chart of chartResponse) {
            labels.push(chart.chartLabel);
            value.push(chart.chartData);
            colour.push(chart.chartColour);
        }
        this.getChart(chartType, chartId, labels, value, colour, '');
    }

    getChart(type: string, chartId, labels, value, colour, datasetLabel) {
        this.chart = new Chart(chartId, {
            type: type,
            data: {
                labels: labels,
                datasets: [
                    {
                        data: value,
                        label: datasetLabel, //For Bar Chart
                        backgroundColor: colour
                    }
                ]
            },
            options: {
                legend: {
                    display: true
                }
            }
        });
    }

    getLineChart(type: string, chartId, labels, webQr, onlineQr, offlineQr, colour) {
        this.chart = new Chart(chartId, {
            type: type,
            data: {
                labels: labels,
                datasets: [
                    {
                        data: webQr,
                        label: 'Web QR',
                        backgroundColor: 'rgba(148,159,177,0.2)',
                        borderColor: 'rgba(148,159,177,1)',
                        pointBackgroundColor: 'rgba(148,159,177,1)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
                    },
                    {
                        data: onlineQr,
                        label: 'Online QR',
                        backgroundColor: 'rgba(77,83,96,0.2)',
                        borderColor: 'rgba(77,83,96,1)',
                        pointBackgroundColor: 'rgba(77,83,96,1)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgba(77,83,96,1)'
                    },
                    {
                        data: offlineQr,
                        label: 'Offline QR',
                        backgroundColor: ' rgba(108, 122, 137, 0.2)',
                        borderColor: ' rgba(108, 122, 137, 1)',
                        pointBackgroundColor: 'rgba(108, 122, 137, 1)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: ' rgba(108, 122, 137, 0.8)'
                    }
                ]
            },
            options: {
                legend: {
                    display: true
                },
                scales: {
                    xAxes: [{
                        display: true
                    }],
                    yAxes: [{
                        display: true
                    }]
                }
            }
        });
    }

    // @HostListener('window:scroll', ['$event']) // for window scroll events
    // onScroll(event) {
    //     if (!this.scrollEventedCalledOnce) {
    //         this.scrollEventedCalledOnce = true;
    //         this.loadRow4_5And6();
    //     }
    // }

    fetchCustomerInRewardPoint() {
        this.loadCustomersInRewardError = false;
        this.loadCustomersInRewardPoint = true;
        var customerRegisteredThroughPartnerInRPS = new CustomerRegisteredThroughPartnerInRPS();
        customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getFirstDateOfCurrentYear();
        customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
        customerRegisteredThroughPartnerInRPS.registeredThroughCategory = 'CUSTOMER';
        this.dashboardService.fetchCustomerResgisteredThroughPartnerInRPS(customerRegisteredThroughPartnerInRPS).subscribe(
            (data) => {
                this.loadCustomersInRewardPoint = false;
                this.dashboardService.rewardCustomerData = data;
                this.totalCustomersInRewardPoint = data.total;
            },
            (error) => {
                this.loadCustomersInRewardPoint = false;
                this.loadCustomersInRewardError = true;

            }
        );
    }

    goToInnerPartnersRewardPointReport(registeredThroughCategory) {
        this.dashboardService.registeredThroughCategory = registeredThroughCategory;
        this.router.navigate([AppRoutes.DASHBOARD, AppRoutes.DASHBOARD_REWARDS_CUSTOMER_REPORT]);
    }

    goToInnerIssuerReport() {
        this.router.navigate([AppRoutes.DASHBOARD, AppRoutes.DASHBOARD_ISSUER_TXNS_REPORT]);
    }

    fetchTodayIssuedAirlineTicket() {
        this.loadAirlineTicketCountError = false;
        this.loadAirlineTicketCount = true;
        var dashboardRequest = new DashboardRequest();
        dashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
        dashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
        this.dashboardService.fetchAirlineTicketCount(dashboardRequest).subscribe(
            (data) => {
                this.loadAirlineTicketCount = false;
                this.dashboardService.airlineData = data;
                this.totalAirlineTicketCount = data.total;
            },
            (error) => {
                this.loadAirlineTicketCount = false;
                this.loadAirlineTicketCountError = true;

            }
        );
    }

    goToInnerAirlineTicketCountReport() {
        this.router.navigate([AppRoutes.DASHBOARD, AppRoutes.ISSUED_AIRLINE_TICKET]);
    }

    fetchUniqueCustomer() {
        this.loadUniqueCustomerError = false;
        this.loadUniqueCustomer = true;
        var uniqueCustomerRequest = new UniqueCustomerRequest();
        uniqueCustomerRequest.startDate = DateUtil.getFirstDateOfCurrentYear();
        uniqueCustomerRequest.endDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
        this.dashboardService.fetchUniqueCustomer(uniqueCustomerRequest).subscribe(
            (data) => {
                this.loadUniqueCustomer = false;
                this.dashboardService.uniqueCustomerData = data;
                this.totalUniqueCustomer = data.total;
            },
            (error) => {
                this.loadUniqueCustomer = false;
                this.loadUniqueCustomerError = true;

            }
        );
    }

    goToInnerUniqueCustomerReport() {
        this.router.navigate([AppRoutes.DASHBOARD, AppRoutes.UNIQUE_CUSTOMER_REPORT]);
    }

    fetchMerchantInRewardPoint() {
        this.loadMerchantsInRewardError = false;
        this.loadMerchantsInRewardPoint = true;
        var customerRegisteredThroughPartnerInRPS = new CustomerRegisteredThroughPartnerInRPS();
        customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getFirstDateOfCurrentYear();
        customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
        customerRegisteredThroughPartnerInRPS.registeredThroughCategory = 'MERCHANT';
        this.dashboardService.fetchCustomerResgisteredThroughPartnerInRPS(customerRegisteredThroughPartnerInRPS).subscribe(
            (data) => {
                this.loadMerchantsInRewardPoint = false;
                this.dashboardService.rewardMerchantData = data;
                this.totalMerchantsInRewardPoint = data.total;
            },
            (error) => {
                this.loadMerchantsInRewardPoint = false;
                this.loadMerchantsInRewardError = true;

            }
        );
    }
}
