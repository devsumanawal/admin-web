import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';
import { DashboardRequest } from '../../model/dashboard-request';


@Injectable()
export class DashBoardService {

  data: any;
  customerRegisteredThroughPartnerInRPS: any;
  airlineDashboardRequest: any;
  airlineData: any;
  rewardCustomerData: any;
  uniqueCustomerData: any;
  requestObj: any;
  registeredThroughCategory: string;
  rewardMerchantData: any;

  constructor(private http: HttpClient) {
  }

  getSuccessFullFonePayTxnAmountByClient() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.DASHBOARD + ApiConstants.TOTAL_TXN_AMOUNT_BY_CLIENT);
  }

  getSuccessFullFonePayTxnAmountByMerchant() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.DASHBOARD + ApiConstants.TOTAL_TXN_AMOUNT_BY_MERCHANT);
  }

  countAllRegisteredMechant(searchParam) {
    return this.http.post<any>
      (ApiConstants.ENDPOINT + ApiConstants.DASHBOARD + ApiConstants.TOTAL_REGISTERED_MERCHANT, searchParam);
  }

  countAllQrTransactions() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.TOTAL_QR_TRANSACTIONS);
  }

  countAllRegisteredClient() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.TOTAL_REGISTERED_CLIENT);
  }

  getRegisteredMechantCount() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.TOTAL_REGISTERED_MERCHANT);
  }

  countAllSuspiciousRequest() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.TOTAL_SUSPICIOUS_REQUEST);
  }

  countAllQrTypeTxn() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.ALL_QR_TYPE_TXN);
  }

  countMerchantByOwnership() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.MERCHANT_BY_OWNERSHIP);
  }

  getMonthlyTotalSuccessfulTxn() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.MONTHLY_TOTAL_SUCCESSFUL_TXN);
  }

  getMonthlyQrTxn() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.MONTHLY_QR_TRANSACTION);
  }

  getClientTxnDetail() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.CLIENT_TXN_DETAILS);
  }

  getTop20MerchantTxnDetails() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.TOP_TWENTY_MERCHANT_TXN_DETAILS);
  }

  getTop5FrequentCustomers() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.TOP_FIVE_FREQUENNT_CUSTOMERS);
  }

  getTop5TxnBanks() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.TOP_FIVE_TXN_BANKS);
  }

  fetchCustomerResgisteredThroughPartnerInRPS(customerRegisteredThroughPartner) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.CUSTOMER_REGISTERED_THROUGH_PARTNER_IN_RPS, customerRegisteredThroughPartner);
  }

  fetchMonthlyCustomerResgisteredThroughPartnerInRPS(customerRegisteredThroughPartner) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.MONTHLY_REGISTERED_CUSTOMER_IN_RPS, customerRegisteredThroughPartner);
  }

  fetchDailyCustomerResgisteredThroughPartnerInRPS(customerRegisteredThroughPartner) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.DAILY_REGISTERED_CUSTOMER_IN_RPS, customerRegisteredThroughPartner);
  }

  fetchAcquirerMerchantDetailWithDate(qrDashboardRequest: DashboardRequest, callCountApi: boolean) {
    if (callCountApi) {
      return this.http.post<any>(
        ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD +
        ApiConstants.ACQUIRER_MERCHANT_DETAILS + ApiConstants.COUNT,
        qrDashboardRequest
      );
    } else {
      return this.http.post<any>(
        ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD +
        ApiConstants.ACQUIRER_MERCHANT_DETAILS,
        qrDashboardRequest
      );
    }
  }

  getTotalClientTxnDetailWithDate(apiToCall: string) {
    return this.http.get<any>(
      ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.TOTAL_CLIENT_TXN_DETAILS
      + apiToCall
    );
  }

  getTotalCountClientTxnDetailWithDate(apiToCall: string) {
    return this.http.get<any>(
      ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.TOTAL_CLIENT_TXN_DETAILS
      + apiToCall + ApiConstants.COUNT
    );
  }


  getAirlineTicketCountToday() {
    return this.http.get<any>(
      ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.AIRLINE_TICKET_COUNT
      + ApiConstants.TODAY + ApiConstants.COUNT
    );
  }

  getCountIssuedTicketWithDate(apiToCall: string) {
    return this.http.get<any>(
      ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.AIRLINE_TICKET_COUNT
      + apiToCall + ApiConstants.COUNT
    );
  }

  fetchAirlineTicketCount(airlineDashboardRequest) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.AIRLINE_TICKET_COUNT, airlineDashboardRequest);
  }

  fetchQrTransationIssureWise(qrDashboardRequest: DashboardRequest, callCountApi: boolean) {
    if (callCountApi) {
      return this.http.post<any>(
        ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD +
        ApiConstants.QR_ISSUER_WISE_CHART + ApiConstants.COUNT,
        qrDashboardRequest
      );
    } else {
      return this.http.post<any>(
        ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD +
        ApiConstants.QR_ISSUER_WISE_CHART,
        qrDashboardRequest
      );
    }
  }

  fetchUniqueCustomer(uniqueCustomerRequest) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.UNIQUE_CUSTOMER, uniqueCustomerRequest);
  }

  fetchDailyRegisteredUniqueCustomer(uniqueCustomerRequest) {
    return this.http.put<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.UNIQUE_CUSTOMER + "/" + 'day', uniqueCustomerRequest);
  }

  fetchMonthlyRegisteredUniqueCustomer(uniqueCustomerRequest) {
    return this.http.put<any>(ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD + ApiConstants.UNIQUE_CUSTOMER + "/" + 'month', uniqueCustomerRequest);
  }

  fetchTotalTransationIssureWise(dashboardRequest: DashboardRequest, callCountApi: boolean) {
    if (callCountApi) {
      return this.http.post<any>(
        ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD +
        ApiConstants.TOTAL_CLIENT_TXN_DETAILS + ApiConstants.COUNT,
        dashboardRequest
      );
    } else {
      return this.http.post<any>(
        ApiConstants.ENDPOINT + ApiConstants.CHART_DASHBOARD +
        ApiConstants.TOTAL_CLIENT_TXN_DETAILS,
        dashboardRequest
      );
    }
  }
}
