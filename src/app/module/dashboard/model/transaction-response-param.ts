export class TransactionResponseParam {
    id: number;
    name: String;
    totalAmount: String;
    count: number;
}
