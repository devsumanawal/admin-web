export class ChartResponse {
  chartLabel: String;
  chartData: number;
  chartColour: String;
}
