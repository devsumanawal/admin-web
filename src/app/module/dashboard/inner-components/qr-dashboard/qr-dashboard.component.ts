import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Chart } from 'chart.js';
import { ToastrService } from 'ngx-toastr';
import * as FusionCharts from 'fusioncharts';
import { DashBoardService } from '../../shared/service/dashboard.service';
import { ApiConstants } from '../../../../constants/api-constants';
import { AppRoutes } from '../../../../constants/app-routes';
import { CustomerRegisteredThroughPartnerInRPS } from '../../model/customer-registered-through-partner-in-RPS';
import { DateUtil } from '../../../core/util/date-util.ts';
import { DashboardRequest } from '../../model/dashboard-request';
import { ChartDataValue } from '../../model/chart-data-value';

@Component({
  templateUrl: 'qr-dashboard.component.html'
})
export class QrDashboardComponent implements OnInit {

  loadQrDashboardReportChart = false;
  showCountChart = false;

  fusionChart = new FusionCharts({});
  state = { 'day': false, 'month': false, 'year': true };
  days = [{ 'label': '', 'value': 0 }];
  months = [{ 'label': '', 'value': 0 }];
  years = [{ 'label': 0, 'value': 0 }];
  day: number;
  month: number;
  year: number;
  totalCount: number;

  constructor(private route: ActivatedRoute,
    private dashboardService: DashBoardService,
    private router: Router,
    private toastr: ToastrService) {
  }

  ngOnInit() {
    this.state = { 'day': false, 'month': false, 'year': true };
    this.year = new Date().getFullYear();
    this.initializeDayAndMonthList(new Date().getFullYear(), new Date().getMonth());
    this.fetchReportOnClick(null);

  }

  // initializeDayAndMonthList() {
  //   const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
  //     'July', 'August', 'September', 'October', 'November', 'December'
  //   ];
  //   this.days = [];
  //   for (let i = 1; i <= new Date().getDate(); i++) {
  //     var day = {'label': 'day ' + i, 'value': i};
  //     this.days.push(day);
  //   }
  //   this.months = [];
  //   for (let i = 0; i <= new Date().getMonth(); i++) {
  //     var month = {'label': monthNames[i], 'value': i};
  //     this.months.push(month);
  //   }

  //   this.years = [];
  //   let startingYear = 2018;
  //   let currentYear = new Date().getFullYear();
  //   for (let i = 0; i <= currentYear - startingYear; i++) {
  //     var year = {'label': startingYear + i, 'value': startingYear + i};
  //     this.years.push(year);
  //   }

  // }

  initializeDayAndMonthList(selectedYear, selectedMonth) {
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    this.days = [];
    var maxDay = 0;
    if (selectedMonth != undefined && selectedYear != undefined
      && (selectedMonth != new Date().getMonth() || selectedYear != new Date().getFullYear())
      && !(selectedYear == new Date().getFullYear() && selectedMonth > new Date().getMonth())) {
      maxDay = new Date(selectedYear, selectedMonth, 0).getDate();
    } else {
      maxDay = new Date().getDate();
    }

    if(this.day > maxDay){
      this.day = new Date().getDate();
    }

    for (let i = 1; i <= maxDay; i++) {
      var day = { 'label': 'day ' + i, 'value': i };
      this.days.push(day);
    }

    this.months = [];
    var maxMonth = 0;
    if (selectedYear === new Date().getFullYear()) {
      maxMonth = new Date().getMonth();
    } else {
      maxMonth = 11;
    }
    if (selectedMonth > maxMonth) {
      this.month = new Date().getMonth();
    }

    for (let i = 0; i <= maxMonth; i++) {
      var month = { 'label': monthNames[i], 'value': i };
      this.months.push(month);
    }

    this.years = [];
    let startingYear = 2018;
    let currentYear = new Date().getFullYear();
    for (let i = 0; i <= currentYear - startingYear; i++) {
      var year = { 'label': startingYear + i, 'value': startingYear + i };
      this.years.push(year);
    }
  }


  goBack() {
    this.router.navigate(['/' + AppRoutes.DASHBOARD]);
  }

  fetchReportOnClick(state) {
    this.state = { 'day': false, 'month': false, 'year': false };
    let qrDashboardRequest = new DashboardRequest();
    this.day = new Date().getDate();
    this.month = new Date().getMonth();
    this.year = new Date().getFullYear();

    if (state == 'day') {
      this.state.day = true;
      qrDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
      qrDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());

    } else if (state == 'month') {
      this.state.month = true;
      qrDashboardRequest.fromDate = DateUtil.getStartingDateOfCurrentMonth();
      qrDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
    } else {
      this.state.year = true;
      qrDashboardRequest.fromDate = DateUtil.getFirstDateOfCurrentYear();
      qrDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
    }
    this.fetchQrTransationIssuerWiseFromServer(qrDashboardRequest);
  }

  showCountWiseOrVolumeWise() {
    this.showCountChart = !this.showCountChart;
    this.fetchReportOnChange();
  }

  fetchReportOnChange() {
    if (this.year === undefined) this.year = new Date().getFullYear();
    if (this.month === undefined) this.month = new Date().getMonth();
    this.initializeDayAndMonthList(this.year, this.month);
    let qrDashboardRequest = new DashboardRequest();
    let date = new Date();
    if (this.state.day == true) {
      qrDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month, this.day));
      qrDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month, this.day + 1));
      this.fetchQrTransationIssuerWiseFromServer(qrDashboardRequest);
    } else if (this.state.month == true) {
      qrDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month, 1));
      qrDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month + 1, 0));
      this.fetchQrTransationIssuerWiseFromServer(qrDashboardRequest);
    } else if (this.state.year == true) {
      qrDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 0, 1));
      qrDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 11, 31));
      this.fetchQrTransationIssuerWiseFromServer(qrDashboardRequest);
    }
  }

  fetchQrTransationIssuerWiseFromServer(qrDashboardRequest: DashboardRequest) {
    this.loadQrDashboardReportChart = false;
    this.dashboardService.fetchQrTransationIssureWise(qrDashboardRequest, this.showCountChart).subscribe(
      (data) => {
        this.loadScrollableBarChartForQrIssuerWise(data);
      },
      (error) => {
        this.loadScrollableBarChartForQrIssuerWise(null);
      }
    );
  }


  loadScrollableBarChartForQrIssuerWise(data) {

    if (data && data.dataset && data.dataset[0]) {
      this.totalCount = 0;
      let dataSetArray: Array<ChartDataValue> = data.dataset[0].data;
      dataSetArray.forEach(v => this.totalCount = +this.totalCount + +v.value);
    } else {
      this.totalCount = 0;
    }

    this.loadQrDashboardReportChart = false;
    this.fusionChart = new FusionCharts({
      type: 'scrollcolumn2d',
      renderAt: 'chart-container1',
      width: '1050',
      height: '400',
      dataFormat: 'json',
      dataSource: data
    });
    this.fusionChart.render();
    this.loadQrDashboardReportChart = true;
  }

}
