import {Component, OnInit} from '@angular/core';
import {DashBoardService} from 'app/module/dashboard/shared/service/dashboard.service';
import {Router} from '@angular/router';
import * as FusionCharts from 'fusioncharts';
import {ToastrService} from 'ngx-toastr';
import {AppRoutes} from 'app/constants/app-routes';
import {ChartDataValue} from '../../../model/chart-data-value';

@Component({
  selector: 'app-rewards-customer-comparision-report',
  templateUrl: './rewards-customer-comparision-report.component.html'
})
export class RewardsCustomerComparisionReportComponent implements OnInit {

  loadingReport = false;
  fusionChart = new FusionCharts({});
  totalCount: number;

  constructor(
    private dashboardService: DashBoardService,
    private toastr: ToastrService,
    private router: Router
  ) {
  }

  ngOnInit() {
    if (this.dashboardService.requestObj != null && this.dashboardService.requestObj != undefined) {
      if (this.dashboardService.requestObj.fetchType === 'monthly') {
        this.fetchMonthlyRegisteredCustomerThroughPartnersInRPS(this.dashboardService.requestObj);
      } else if (this.dashboardService.requestObj.fetchType === 'daily') {
        this.fetchDailyRegisteredCustomerThroughPartnersInRPS(this.dashboardService.requestObj);
      } else {
        this.toastr.error('No Data Available');
        this.goBack();
      }
    } else {
      this.toastr.error('No Data Available');
      this.goBack();
    }
  }

  fetchMonthlyRegisteredCustomerThroughPartnersInRPS(customerRegisteredThroughPartnerInRPS) {
    this.loadingReport = false;
    this.dashboardService.fetchMonthlyCustomerResgisteredThroughPartnerInRPS(customerRegisteredThroughPartnerInRPS).subscribe(
      (data) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForPartnerRewardPoint(data);
      },
      (error) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForPartnerRewardPoint(null);
      }
    );
  }

  fetchDailyRegisteredCustomerThroughPartnersInRPS(customerRegisteredThroughPartnerInRPS) {
    this.loadingReport = false;
    this.dashboardService.fetchDailyCustomerResgisteredThroughPartnerInRPS(customerRegisteredThroughPartnerInRPS).subscribe(
      (data) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForPartnerRewardPoint(data);
      },
      (error) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForPartnerRewardPoint(null);
      }
    );
  }

  loadScrollableBarChartForPartnerRewardPoint(data) {

    if (data && data.dataset && data.dataset[0]) {
      this.totalCount = 0;
      let dataSetArray: Array<ChartDataValue> = data.dataset[0].data;
      dataSetArray.forEach(v => this.totalCount = +this.totalCount + +v.value);
    } else {
      this.totalCount = 0;
    }

    this.loadingReport = false;
    this.fusionChart = new FusionCharts({
      type: 'scrollcolumn2d',
      renderAt: 'chart-container1',
      width: '1024',
      height: '400',
      dataFormat: 'json',
      dataSource: data
    });
    this.fusionChart.render();
    this.loadingReport = true;
  }

  goBack() {
    this.router.navigate([AppRoutes.DASHBOARD, AppRoutes.DASHBOARD_REWARDS_CUSTOMER_REPORT]);
  }

}
