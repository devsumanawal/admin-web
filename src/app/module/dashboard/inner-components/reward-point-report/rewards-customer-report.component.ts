import { Component, OnInit } from '@angular/core';
import { DashBoardService } from '../../shared/service/dashboard.service';
import { ToastrService } from 'ngx-toastr';
import * as FusionCharts from 'fusioncharts';
import { AppRoutes } from 'app/constants/app-routes';
import { Router } from '@angular/router';
import { CustomerRegisteredThroughPartnerInRPS } from '../../model/customer-registered-through-partner-in-RPS';
import { DateUtil } from 'app/module/core/util/date-util.ts';
import { routes } from 'app/app.routing';
import { ChartDataValue } from '../../model/chart-data-value';
@Component({
  selector: 'app-rewards-customer-report',
  templateUrl: './rewards-customer-report.component.html'
})
export class RewardsCustomerReportComponent implements OnInit {

  loadingReport = false;
  fusionChart = new FusionCharts({});
  state = { 'day': false, 'month': false, 'year': true };
  days = [{ 'label': '', 'value': 0 }];
  months = [{ 'label': '', 'value': 0 }];
  years = [{ 'label': 0, 'value': 0 }];
  day: number;
  month: number;
  year: number;
  totalCount: number;

  constructor(
    private dashboardService: DashBoardService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.state = { 'day': false, 'month': false, 'year': true };
    this.year = new Date().getFullYear();
    this.initializeDayAndMonthList();
    if (this.dashboardService.registeredThroughCategory != null && this.dashboardService.registeredThroughCategory != undefined) {
      if (this.dashboardService.registeredThroughCategory === 'CUSTOMER') {
        if (this.dashboardService.rewardCustomerData != null && this.dashboardService.rewardCustomerData != undefined) {
          this.loadScrollableBarChartForPartnerRewardPoint(this.dashboardService.rewardCustomerData);
        } else {
          this.goBack();
        }
        
      } else {
        if (this.dashboardService.rewardMerchantData != null && this.dashboardService.rewardMerchantData != undefined) {
          this.loadScrollableBarChartForPartnerRewardPoint(this.dashboardService.rewardMerchantData);
        } else {
          this.goBack();
        }
      }
    } else {
          this.goBack();
    }
  }

  initializeDayAndMonthList() {
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    this.days = [];
    for (let i = 1; i <= new Date().getDate(); i++) {
      var day = { 'label': 'day ' + i, 'value': i };
      this.days.push(day);
    }
    this.months = [];
    for (let i = 0; i <= new Date().getMonth(); i++) {
      var month = { 'label': monthNames[i], 'value': i };
      this.months.push(month);
    }

    this.years = [];
    let startingYear = 2019;
    let currentYear = new Date().getFullYear();
    for (let i = 0; i <= currentYear - startingYear; i++) {
      var year = { 'label': startingYear + i, 'value': startingYear + i };
      this.years.push(year);
    }

  }


  fetchReportOnClick(state) {
    this.state = { 'day': false, 'month': false, 'year': false };
    var customerRegisteredThroughPartnerInRPS = new CustomerRegisteredThroughPartnerInRPS();
    customerRegisteredThroughPartnerInRPS.registeredThroughCategory = this.dashboardService.registeredThroughCategory;
    if (state == 'day') {
      this.day = new Date().getDate();
      this.state.day = true;
      customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
      customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
    } else if (state == 'month') {
      this.state.month = true;
      this.month = new Date().getMonth();
      customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getStartingDateOfCurrentMonth();
      customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
    } else {
      this.state.year = true;
      this.year = new Date().getFullYear();
      customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getFirstDateOfCurrentYear();
      customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
    }
    this.fetchRegisteredCustomerThroughPartnersInRPS(customerRegisteredThroughPartnerInRPS);
  }

  fetchReportOnChange(event, state) {
    var customerRegisteredThroughPartnerInRPS = new CustomerRegisteredThroughPartnerInRPS();
    var date = new Date();
    customerRegisteredThroughPartnerInRPS.registeredThroughCategory = this.dashboardService.registeredThroughCategory;
    if (state == 'day') {
      customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), date.getMonth(), this.day));
      customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), date.getMonth(), this.day));
      this.fetchRegisteredCustomerThroughPartnersInRPS(customerRegisteredThroughPartnerInRPS);
    } else if (state == 'month') {
      customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), this.month, 1));
      customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), this.month + 1, 0));
      this.fetchRegisteredCustomerThroughPartnersInRPS(customerRegisteredThroughPartnerInRPS);
    } else if (state == 'year') {
      customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 0, 1));
      customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 11, 31));
      this.fetchRegisteredCustomerThroughPartnersInRPS(customerRegisteredThroughPartnerInRPS);
    }
  }

  fetchRegisteredCustomerThroughPartnersInRPS(customerRegisteredThroughPartnerInRPS) {
    this.loadingReport = false;
    this.dashboardService.fetchCustomerResgisteredThroughPartnerInRPS(customerRegisteredThroughPartnerInRPS).subscribe(
      (data) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForPartnerRewardPoint(data);
      },
      (error) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForPartnerRewardPoint(null);
      }
    );
  }


  loadScrollableBarChartForPartnerRewardPoint(data) {

    if (data && data.dataset && data.dataset[0]) {
      this.totalCount = 0;
      let dataSetArray: Array<ChartDataValue> = data.dataset[0].data;
      dataSetArray.forEach(v => this.totalCount = +this.totalCount + +v.value);
    } else {
      this.totalCount = 0;
    }

    this.loadingReport = false;
    this.fusionChart = new FusionCharts({
      type: 'scrollcolumn2d',
      renderAt: 'chart-container1',
      width: '1050',
      height: '400',
      dataFormat: 'json',
      dataSource: data
    });
    this.fusionChart.render();
    var self = this;
    this.fusionChart.addEventListener('dataPlotClick', function (ev, props) {
      let evenObj: any = props;
      var customerRegisteredThroughPartnerInRPS = new CustomerRegisteredThroughPartnerInRPS();
      var date = new Date();
      customerRegisteredThroughPartnerInRPS.registeredThrough = evenObj.categoryLabel;
      customerRegisteredThroughPartnerInRPS.registeredThroughCategory = self.dashboardService.registeredThroughCategory;
      if (self.state.year == true) {
        customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getFirstDateOfCurrentYear();
        customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
        customerRegisteredThroughPartnerInRPS.fetchType = 'monthly';
        self.dashboardService.requestObj = customerRegisteredThroughPartnerInRPS;
        self.router.navigate([AppRoutes.DASHBOARD, AppRoutes.DASHBOARD_REWARDS_CUSTOMER_COMPARISION_REPORT]);
      } else if (self.state.month == true) {
        customerRegisteredThroughPartnerInRPS.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), self.month, 1));
        customerRegisteredThroughPartnerInRPS.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), self.month + 1, 0));
        customerRegisteredThroughPartnerInRPS.fetchType = 'daily';
        self.dashboardService.requestObj = customerRegisteredThroughPartnerInRPS;
        self.router.navigate([AppRoutes.DASHBOARD, AppRoutes.DASHBOARD_REWARDS_CUSTOMER_COMPARISION_REPORT]);
      }

    }, self);
    this.loadingReport = true;
  }


  goBack() {
    this.router.navigate(['/' + AppRoutes.DASHBOARD]);
  }
}
