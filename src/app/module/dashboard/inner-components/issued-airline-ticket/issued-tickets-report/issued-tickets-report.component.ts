import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DashBoardService} from '../../../shared/service/dashboard.service';
import * as FusionCharts from 'fusioncharts';
import {ToastrService} from 'ngx-toastr';
import {CustomerRegisteredThroughPartnerInRPS} from '../../../model/customer-registered-through-partner-in-RPS';
import {DateUtil} from '../../../../core/util/date-util.ts';
import {AppRoutes} from '../../../../../constants/app-routes';
import {DashboardRequest} from '../../../model/dashboard-request';

@Component({
  selector: 'app-issued-tickets-report',
  templateUrl: './issued-tickets-report.component.html'
})
export class IssuedTicketsReportComponent implements OnInit {
  loadAirlineTicketReportChart = false;
  fusionChart = new FusionCharts({});
  state = {'day': false, 'month': false, 'year': true};
  days = [{'label': '', 'value': 0}];
  months = [{'label': '', 'value': 0}];
  years = [{'label': 0, 'value': 0}];
  day: number;
  month: number;
  year: number;
  totalCount: number;

  constructor(
    private dashboardService: DashBoardService,
    private toastr: ToastrService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.state = {'day': true, 'month': false, 'year': false};
    this.day = new Date().getDate();
    this.initializeDayAndMonthList(new Date().getFullYear(),new Date().getMonth());
    this.fetchReportOnClick('day');
  }

  initializeDayAndMonthList(selectedYear, selectedMonth) {
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    this.days = [];
    var maxDay = 0;
    if (selectedMonth != undefined && selectedYear != undefined
      && (selectedMonth != new Date().getMonth() || selectedYear != new Date().getFullYear())
      && !(selectedYear == new Date().getFullYear() && selectedMonth > new Date().getMonth())) {
      maxDay = new Date(selectedYear, selectedMonth, 0).getDate();
    } else {
      maxDay = new Date().getDate();
    }

    if(this.day > maxDay){
      this.day = new Date().getDate();
    }

    for (let i = 1; i <= maxDay; i++) {
      var day = { 'label': 'day ' + i, 'value': i };
      this.days.push(day);
    }

    this.months = [];
    var maxMonth = 0;
    if (selectedYear === new Date().getFullYear()) {
      maxMonth = new Date().getMonth();
    } else {
      maxMonth = 11;
    }
    if (selectedMonth > maxMonth) {
      this.month = new Date().getMonth();
    }

    for (let i = 0; i <= maxMonth; i++) {
      var month = { 'label': monthNames[i], 'value': i };
      this.months.push(month);
    }

    this.years = [];
    let startingYear = 2018;
    let currentYear = new Date().getFullYear();
    for (let i = 0; i <= currentYear - startingYear; i++) {
      var year = { 'label': startingYear + i, 'value': startingYear + i };
      this.years.push(year);
    }
  }

  // initializeDayAndMonthList() {
  //   const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
  //     'July', 'August', 'September', 'October', 'November', 'December'
  //   ];
  //   this.days = [];
  //   for (let i = 1; i <= new Date().getDate(); i++) {
  //     var day = {'label': 'day ' + i, 'value': i};
  //     this.days.push(day);
  //   }
  //   this.months = [];
  //   for (let i = 0; i <= new Date().getMonth(); i++) {
  //     var month = {'label': monthNames[i], 'value': i};
  //     this.months.push(month);
  //   }

  //   this.years = [];
  //   let startingYear = 2018;
  //   let currentYear = new Date().getFullYear();
  //   for (let i = 0; i <= currentYear - startingYear; i++) {
  //     var year = {'label': startingYear + i, 'value': startingYear + i};
  //     this.years.push(year);
  //   }

  // }


  fetchReportOnClick(state) {
    this.state = {'day': false, 'month': false, 'year': false};
    var dashboardRequest = new DashboardRequest();
    this.day = new Date().getDate();
    this.month = new Date().getMonth();
    this.year = new Date().getFullYear();
    if (state == 'day') {
      this.state.day = true;
      dashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
      dashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());

    } else if (state == 'month') {
      this.state.month = true;
      dashboardRequest.fromDate = DateUtil.getStartingDateOfCurrentMonth();
      dashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
    } else {
      this.state.year = true;
      dashboardRequest.fromDate = DateUtil.getFirstDateOfCurrentYear();
      dashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
    }
    this.fetchAirlineTicketCountReport(dashboardRequest);
  }

  fetchReportOnChange() {
    var airlineDashboardRequest = new DashboardRequest();
    if(this.year === undefined) this.year = new Date().getFullYear();
    if(this.month === undefined) this.month = new Date().getMonth();
    this.initializeDayAndMonthList(this.year,this.month);
    var date = new Date();
    if (this.state.day == true) {
      airlineDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month, this.day));
      airlineDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month, this.day + 1));
      this.fetchAirlineTicketCountReport(airlineDashboardRequest);
    } else if (this.state.month == true) {
      airlineDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month, 1));
      airlineDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month + 1, 0));
      this.fetchAirlineTicketCountReport(airlineDashboardRequest);
    } else if (this.state.year == true) {
      airlineDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 0, 1));
      airlineDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 11, 31));
      this.fetchAirlineTicketCountReport(airlineDashboardRequest);
    }
  }

  fetchAirlineTicketCountReport(airlineDashboardRequest) {
    this.loadAirlineTicketReportChart = false;
    this.dashboardService.fetchAirlineTicketCount(airlineDashboardRequest).subscribe(
      (data) => {
        this.loadAirlineTicketReportChart = true;
        this.loadScrollableBarChartForAirlineTicketCount(data);
      },
      (error) => {
        this.loadAirlineTicketReportChart = true;
        this.loadScrollableBarChartForAirlineTicketCount(null);
      }
    );
  }


  loadScrollableBarChartForAirlineTicketCount(data) {
    this.totalCount = data ? data.total : 0;

    this.loadAirlineTicketReportChart = false;
    this.fusionChart = new FusionCharts({
      type: 'scrollcolumn2d',
      renderAt: 'chart-container1',
      width: '1050',
      height: '400',
      dataFormat: 'json',
      dataSource: data
    });
    this.fusionChart.render();
    var self = this;
    this.fusionChart.addEventListener('dataPlotClick', function (ev, props) {
      let evenObj: any = props;
      var airlineDashboardRequest = new CustomerRegisteredThroughPartnerInRPS();
      var date = new Date();
      airlineDashboardRequest.registeredThrough = evenObj.categoryLabel;
      if (self.state.year == true) {
        airlineDashboardRequest.fromDate = DateUtil.getFirstDateOfCurrentYear();
        airlineDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
        airlineDashboardRequest.fetchType = 'monthly';
        // self.dashboardService.airlineDashboardRequest = airlineDashboardRequest;
        // self.router.navigate([AppRoutes.DASHBOARD, AppRoutes.DASHBOARD_REWARDS_CUSTOMER_COMPARISION_REPORT]);
      } else if (self.state.month == true) {
        airlineDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), self.month, 1));
        airlineDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), self.month + 1, 0));
        airlineDashboardRequest.fetchType = 'daily';
        // self.dashboardService.airlineDashboardRequest = airlineDashboardRequest;
        // self.router.navigate([AppRoutes.DASHBOARD, AppRoutes.DASHBOARD_REWARDS_CUSTOMER_COMPARISION_REPORT]);
      }

    }, self);
    this.loadAirlineTicketReportChart = true;
  }

  goBack() {
    this.router.navigate(['/' + AppRoutes.DASHBOARD]);
  }


}
