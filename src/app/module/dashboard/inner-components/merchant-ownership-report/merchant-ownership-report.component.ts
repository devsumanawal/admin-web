import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Chart} from 'chart.js';
import {ToastrService} from 'ngx-toastr';
import * as FusionCharts from 'fusioncharts';
import {DashBoardService} from '../../shared/service/dashboard.service';
import {AppRoutes} from 'app/constants/app-routes';
import {DateUtil} from 'app/module/core/util/date-util.ts';
import {DashboardRequest} from '../../model/dashboard-request';
import {CustomerRegisteredThroughPartnerInRPS} from '../../model/customer-registered-through-partner-in-RPS';
import {ChartDataValue} from '../../model/chart-data-value';

@Component({
  selector: 'app-merchant-ownership-report',
  templateUrl: 'merchant-ownership-report.component.html'
})
export class MerchantOwnershipReportComponent implements OnInit {


  loadAcquirerMerchantReportChart = false;
  showCountChart = false;

  fusionChart = new FusionCharts({});
  state = {'day': false, 'month': false, 'year': true};
  days = [{'label': '', 'value': 0}];
  months = [{'label': '', 'value': 0}];
  years = [{'label': 0, 'value': 0}];
  day: number;
  month: number;
  year: number;

  selectedChart = '';
  totalCount: number;

  constructor(private route: ActivatedRoute,
              private dashboardService: DashBoardService,
              private router: Router,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.state = {'day': false, 'month': false, 'year': true};
    this.year = new Date().getFullYear();
    this.initializeDayAndMonthList(new Date().getFullYear(),new Date().getMonth());

    this.fetchReportOnClick(null);
  }

  goBack() {
    this.router.navigate(['/' + AppRoutes.DASHBOARD]);
  }

  fetchReportOnClick(state) {
    this.state = {'day': false, 'month': false, 'year': false};
    let AcquirerMerchantDashboardRequest = new DashboardRequest();

    this.day = new Date().getDate();
    this.month = new Date().getMonth();
    this.year = new Date().getFullYear();
    if (state == 'day') {
      this.state.day = true;
      AcquirerMerchantDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
      AcquirerMerchantDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());

    } else if (state == 'month') {
      this.state.month = true;
      AcquirerMerchantDashboardRequest.fromDate = DateUtil.getStartingDateOfCurrentMonth();
      AcquirerMerchantDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
    } else {
      this.state.year = true;
      AcquirerMerchantDashboardRequest.fromDate = DateUtil.getFirstDateOfCurrentYear();
      AcquirerMerchantDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
    }
    this.fetchAcquirerMerchantDetailWithDate(AcquirerMerchantDashboardRequest);
  }

  fetchAcquirerMerchantDetailWithDate(AcquirerMerchantDashboardRequest: DashboardRequest) {
    this.loadAcquirerMerchantReportChart = false;
    this.dashboardService.fetchAcquirerMerchantDetailWithDate(AcquirerMerchantDashboardRequest, this.showCountChart).subscribe(
      (data) => {
        this.loadScrollableBarChartForQrIssuerWise(data);
      },
      (error) => {
        this.loadScrollableBarChartForQrIssuerWise(null);
      }
    );
  }

  loadScrollableBarChartForQrIssuerWise(data) {

    if (data && data.dataset && data.dataset[0]) {
      this.totalCount = 0;
      let dataSetArray: Array<ChartDataValue> = data.dataset[0].data;
      dataSetArray.forEach(v => this.totalCount = +this.totalCount + +v.value);
    } else {
      this.totalCount = 0;
    }

    this.loadAcquirerMerchantReportChart = false;
    this.fusionChart = new FusionCharts({
      type: 'scrollcolumn2d',
      renderAt: 'chart-container1',
      width: '1100',
      height: '400',
      dataFormat: 'json',
      dataSource: data
    });
    this.fusionChart.render();
    this.loadAcquirerMerchantReportChart = true;
  }

  initializeDayAndMonthList(selectedYear, selectedMonth) {
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    this.days = [];
    var maxDay = 0;
    if (selectedMonth != undefined && selectedYear != undefined
      && (selectedMonth != new Date().getMonth() || selectedYear != new Date().getFullYear())
      && !(selectedYear == new Date().getFullYear() && selectedMonth > new Date().getMonth())) {
      maxDay = new Date(selectedYear, selectedMonth, 0).getDate();
    } else {
      maxDay = new Date().getDate();
    }

    if(this.day > maxDay){
      this.day = new Date().getDate();
    }

    for (let i = 1; i <= maxDay; i++) {
      var day = { 'label': 'day ' + i, 'value': i };
      this.days.push(day);
    }

    this.months = [];
    var maxMonth = 0;
    if (selectedYear === new Date().getFullYear()) {
      maxMonth = new Date().getMonth();
    } else {
      maxMonth = 11;
    }
    if (selectedMonth > maxMonth) {
      this.month = new Date().getMonth();
    }

    for (let i = 0; i <= maxMonth; i++) {
      var month = { 'label': monthNames[i], 'value': i };
      this.months.push(month);
    }

    this.years = [];
    let startingYear = 2018;
    let currentYear = new Date().getFullYear();
    for (let i = 0; i <= currentYear - startingYear; i++) {
      var year = { 'label': startingYear + i, 'value': startingYear + i };
      this.years.push(year);
    }
  }

  // showCountWiseOrVolumeWise() {
  //   this.showCountChart = !this.showCountChart;
  //   if (this.state.day) {
  //     this.fetchReportOnChange('day');
  //   } else if (this.state.month) {
  //     this.fetchReportOnChange('month');
  //   } else {
  //     this.fetchReportOnChange('year');
  //   }
  // }


  fetchReportOnChange() {
    if(this.year === undefined) this.year = new Date().getFullYear();
    if(this.month === undefined) this.month = new Date().getMonth();
    this.initializeDayAndMonthList(this.year,this.month);
    let AcquirerMerchantDashboardRequest = new CustomerRegisteredThroughPartnerInRPS();
    let date = new Date();
    if (this.state.day == true) {
      AcquirerMerchantDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month, this.day));
      AcquirerMerchantDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month, this.day + 1));
      this.fetchAcquirerMerchantDetailWithDate(AcquirerMerchantDashboardRequest);
    } else if (this.state.month == true) {
      AcquirerMerchantDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month, 1));
      AcquirerMerchantDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, this.month + 1, 0));
      this.fetchAcquirerMerchantDetailWithDate(AcquirerMerchantDashboardRequest);
    } else if (this.state.year == true) {
      AcquirerMerchantDashboardRequest.fromDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 0, 1));
      AcquirerMerchantDashboardRequest.toDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 11, 31));
      this.fetchAcquirerMerchantDetailWithDate(AcquirerMerchantDashboardRequest);
    }
  }
}
