import { Component, OnInit } from '@angular/core';
import * as FusionCharts from 'fusioncharts';
import { DashBoardService } from 'app/module/dashboard/shared/service/dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import {ChartDataValue} from '../../../model/chart-data-value';
@Component({
  selector: 'app-unique-customer-comparision',
  templateUrl: './unique-customer-comparision.component.html'
})
export class UniqueCustomerComparisionComponent implements OnInit {
  loadingReport = false;
  fusionChart = new FusionCharts({});
  totalCount: number;

  constructor(
    private dashboardService: DashBoardService,
    private toastr: ToastrService,
    private router: Router
  ) { }
  ngOnInit() {
    if (this.dashboardService.requestObj != null && this.dashboardService.requestObj != undefined) {
      if (this.dashboardService.requestObj.fetchType === 'monthly') {
        this.fetchMonthlyRegisteredUniqueCustomer(this.dashboardService.requestObj);
      } else if (this.dashboardService.requestObj.fetchType === 'daily') {
        this.fetchDailyRegisteredUniqueCustomer(this.dashboardService.requestObj);
      } else {
        this.toastr.error("No Data Available");
        this.goBack();
      }
    } else {
      this.toastr.error("No Data Available");
      this.goBack();
    }
  }
  loadScrollableBarChartForUniqueCustomer(data) {

    if (data && data.dataset && data.dataset[0]) {
      this.totalCount = 0;
      let dataSetArray: Array<ChartDataValue> = data.dataset[0].data;
      dataSetArray.forEach(v => this.totalCount = +this.totalCount + +v.value);
    } else {
      this.totalCount = 0;
    }

    this.loadingReport = false;
        this.fusionChart = new FusionCharts({
          type: 'scrollcolumn2d',
          renderAt: 'chart-container1',
          width: '1024',
          height: '400',
          dataFormat: 'json',
          dataSource: data
        });
        this.fusionChart.render();
        this.loadingReport = true;
  }

  fetchDailyRegisteredUniqueCustomer(uniqueCustomerRequest) {
    console.log('daily',uniqueCustomerRequest);
    this.loadingReport = false;
    this.dashboardService.fetchDailyRegisteredUniqueCustomer(uniqueCustomerRequest).subscribe(
      (data) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForUniqueCustomer(data);
      },
      (error) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForUniqueCustomer(null);
      }
    );
  }

  fetchMonthlyRegisteredUniqueCustomer(uniqueCustomerRequest) {

    this.loadingReport = false;
    this.dashboardService.fetchMonthlyRegisteredUniqueCustomer(uniqueCustomerRequest).subscribe(
      (data) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForUniqueCustomer(data);
      },
      (error) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForUniqueCustomer(null);
      }
    );
  }

  goBack() {
    this.router.navigate([AppRoutes.DASHBOARD, AppRoutes.UNIQUE_CUSTOMER_REPORT]);
  }

}
