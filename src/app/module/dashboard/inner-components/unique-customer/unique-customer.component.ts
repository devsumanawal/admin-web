import { Component, OnInit } from '@angular/core';
import { DashBoardService } from '../../shared/service/dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as FusionCharts from 'fusioncharts';
import { AppRoutes } from 'app/constants/app-routes';
import { UniqueCustomerRequest } from '../../model/unique-customer-request';
import { DateUtil } from 'app/module/core/util/date-util.ts';
import {ChartDataValue} from '../../model/chart-data-value';

@Component({
  selector: 'app-unique-customer',
  templateUrl: './unique-customer.component.html'
})
export class UniqueCustomerComponent implements OnInit {

  loadingReport = false;
  fusionChart = new FusionCharts({});
  state = { 'day': false, 'month': false, 'year': true };
  days = [{ 'label': '', 'value': 0 }];
  months = [{ 'label': '', 'value': 0 }];
  years = [{ 'label': 0, 'value': 0 }];
  day: number;
  month: number;
  year: number;
  totalCount: number;

  constructor(
    private dashboardService: DashBoardService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.state = { 'day': false, 'month': false, 'year': true };
    this.year = new Date().getFullYear();
    this.initializeDayAndMonthList();
    if (this.dashboardService.uniqueCustomerData != null && this.dashboardService.uniqueCustomerData != undefined) {
      this.loadScrollableBarChartForUniqueCustomer(this.dashboardService.uniqueCustomerData);
    } else {
      this.toastr.error("No Data Available");
      this.goBack();
    }
  }

  initializeDayAndMonthList() {
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    this.days = [];
    for (let i = 1; i <= new Date().getDate(); i++) {
      var day = { 'label': 'day ' + i, 'value': i };
      this.days.push(day);
    }
    this.months = [];
    for (let i = 0; i <= new Date().getMonth(); i++) {
      var month = { 'label': monthNames[i], 'value': i };
      this.months.push(month);
    }

    this.years = [];
    let startingYear = 2018;
    let currentYear = new Date().getFullYear();
    for (let i = 0; i <= currentYear - startingYear; i++) {
      var year = { 'label': startingYear + i, 'value': startingYear + i };
      this.years.push(year);
    }
  
  }

  loadScrollableBarChartForUniqueCustomer(data) {

    if (data && data.dataset && data.dataset[0]) {
      this.totalCount = 0;
      let dataSetArray: Array<ChartDataValue> = data.dataset[0].data;
      dataSetArray.forEach(v => this.totalCount = +this.totalCount + +v.value);
    } else {
      this.totalCount = 0;
    }

    this.loadingReport = false;
    this.fusionChart = new FusionCharts({
      type: 'scrollcolumn2d',
      renderAt: 'chart-container1',
      width: '1050',
      height: '400',
      dataFormat: 'json',
      dataSource: data
    });
    this.fusionChart.render();
    var self = this;
    this.fusionChart.addEventListener('dataPlotClick', function (ev, props) {
      let evenObj: any = props;
      var uniqueCustomerRequest = new UniqueCustomerRequest();
      var date = new Date();
      // uniqueCustomerRequest.clientId = evenObj.categoryLabel.split("(")[1].split(")")[0];
      uniqueCustomerRequest.clientName = evenObj.categoryLabel;
      if (self.state.year == true) {
        uniqueCustomerRequest.startDate = DateUtil.getFirstDateOfCurrentYear();
        uniqueCustomerRequest.endDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(DateUtil.getNextDayDate());
        uniqueCustomerRequest.fetchType = 'monthly';
        self.dashboardService.requestObj = uniqueCustomerRequest;
        self.router.navigate([AppRoutes.DASHBOARD, AppRoutes.UNIQUE_CUSTOMRER_COMPARISION_REPORT]);
      } else if (self.state.month == true) {
        uniqueCustomerRequest.startDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), self.month, 1));
        uniqueCustomerRequest.endDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), self.month + 1, 0));
        uniqueCustomerRequest.fetchType = 'daily';
        self.dashboardService.requestObj = uniqueCustomerRequest;
        self.router.navigate([AppRoutes.DASHBOARD, AppRoutes.UNIQUE_CUSTOMRER_COMPARISION_REPORT]);
      }

    }, self);
    this.loadingReport = true;
  }

  fetchUniqueCustomer(uniqueCustomer) {
    console.log(uniqueCustomer);
    this.loadingReport = false;
    this.dashboardService.fetchUniqueCustomer(uniqueCustomer).subscribe(
      (data) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForUniqueCustomer(data);
      },
      (error) => {
        this.loadingReport = true;
        this.loadScrollableBarChartForUniqueCustomer(null);
      }
    );
  }


  fetchReportOnClick(state) {
    this.state = { 'day': false, 'month': false, 'year': false };
    var uniqueCustomerRequest = new UniqueCustomerRequest();
    if (state == 'day') {
      this.day = new Date().getDate();
      this.state.day = true;
      uniqueCustomerRequest.startDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
      uniqueCustomerRequest.endDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());

    } else if (state == 'month') {
      this.state.month = true;
      this.month = new Date().getMonth();
      uniqueCustomerRequest.startDate = DateUtil.getStartingDateOfCurrentMonth();
      uniqueCustomerRequest.endDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
    } else {
      this.state.year = true;
      this.year = new Date().getFullYear();
      uniqueCustomerRequest.startDate = DateUtil.getFirstDateOfCurrentYear();
      uniqueCustomerRequest.endDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date());
    }
    this.fetchUniqueCustomer(uniqueCustomerRequest);
  }

  fetchReportOnChange(event, state) {
    var uniqueCustomerRequest = new UniqueCustomerRequest();
    var date = new Date();
    if (state == 'day') {
      uniqueCustomerRequest.startDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), date.getMonth(), this.day));
      uniqueCustomerRequest.endDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), date.getMonth(), this.day));
      this.fetchUniqueCustomer(uniqueCustomerRequest);
    } else if(state == 'month') {
      uniqueCustomerRequest.startDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), this.month, 1));
      uniqueCustomerRequest.endDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(date.getFullYear(), this.month + 1, 0));;
      this.fetchUniqueCustomer(uniqueCustomerRequest);
    } else if(state == 'year'){
      uniqueCustomerRequest.startDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 0, 1));
      uniqueCustomerRequest.endDate = DateUtil.getLocalDateIn_Java_yyyy_MM_dd_Format(new Date(this.year, 11, 31));
      this.fetchUniqueCustomer(uniqueCustomerRequest);
    }
  }



  goBack() {
    this.router.navigate(['/' + AppRoutes.DASHBOARD]);
  }

  getStateOfObject() {
    return this;
  }

}
