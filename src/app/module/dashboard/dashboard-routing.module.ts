import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AppRoutes } from 'app/constants/app-routes';
import { RewardsCustomerComparisionReportComponent } from './inner-components/reward-point-report/rewards-customer-comparision-report/rewards-customer-comparision-report.component';
import { RewardsCustomerReportComponent } from './inner-components/reward-point-report/rewards-customer-report.component';
import { QrDashboardComponent } from './inner-components/qr-dashboard/qr-dashboard.component';
import { TotalIssuerTxnsDashboardComponent } from './inner-components/total-issuer-txns-dashboard/total-issuer-txns-dashboard.component';
import { MerchantOwnershipReportComponent } from './inner-components/merchant-ownership-report/merchant-ownership-report.component';
import { IssuedTicketsReportComponent } from "./inner-components/issued-airline-ticket/issued-tickets-report/issued-tickets-report.component";
import { UniqueCustomerComponent } from './inner-components/unique-customer/unique-customer.component';
import { UniqueCustomerComparisionComponent } from './inner-components/unique-customer/unique-customer-comparision/unique-customer-comparision.component';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        data: {
            title: 'Dashboard'
        }
    },
    {
        path: 'qr',
        component: QrDashboardComponent,
        data: {
            title: 'Qr Dashboard'
        }
    },
    {
        path: AppRoutes.DASHBOARD_REWARDS_CUSTOMER_REPORT,
        component: RewardsCustomerReportComponent,
    },
    {
        path: AppRoutes.DASHBOARD_REWARDS_CUSTOMER_COMPARISION_REPORT,
        component: RewardsCustomerComparisionReportComponent,
    },
    {
        path: 'totalIssuerTxn',
        component: TotalIssuerTxnsDashboardComponent,
        data: {
            title: 'Issuer Transactions Dashboard'
        }
    },
    {
        path: 'merchantOwnershipDetails',
        component: MerchantOwnershipReportComponent,
        data: {
            title: 'Merchant Ownership Report'
        }
    },
    {
        path: 'issuedAirlineTicket',
        component:
            IssuedTicketsReportComponent,
        data:
        {
            title: 'Issued Airline Ticket'
        }
    },
    {
        path: AppRoutes.UNIQUE_CUSTOMER_REPORT,
        component: UniqueCustomerComponent,
    },
    {
        path: AppRoutes.UNIQUE_CUSTOMRER_COMPARISION_REPORT,
        component: UniqueCustomerComparisionComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule {
}
