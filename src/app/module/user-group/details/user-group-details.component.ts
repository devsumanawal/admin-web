import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UserGroupService } from '../shared/service/user-group.service';
import { UserGroupDTO } from '../shared/model/user-group-dto';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { AdminProfileDto } from 'app/module/admin-profile/shared/model/admin-profile-dto-model';


@Component({
  selector: 'app-user-group-details',
  templateUrl: './user-group-details.component.html',
  styleUrls: ['./user-group-details.component.scss']
})
export class UserGroupDetailsComponent implements OnInit {


  userGroup: UserGroupDTO = new UserGroupDTO();

  constructor(
    userGroupService: UserGroupService,
    adminProfileService: AdminProfileService,
    private location: Location) {
    this.userGroup = userGroupService.getSelectedForUserGroup();
  }

  ngOnInit() {

  }

  goBack() {
  
    this.location.back();
  }


  isActive(visibleDescription: string) {
    return visibleDescription === 'Y';
  }
}
