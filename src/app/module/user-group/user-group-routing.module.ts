import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { UserGroupCreateComponent } from 'app/module/user-group/create/user-group-create.component';
import { CommonModule } from '@angular/common';
import { UserGroupManageComponent } from 'app/module/user-group/manage/user-group-manage.component';
import { UserGroupDetailsComponent } from './details/user-group-details.component';
import { UserGroupEditComponent } from './manage/edit/user-group-edit.component';
import { EditUserGroupResolverService } from './shared/resolvers/edit-user-group-resolver.service';
import { UserGroupDetailsGuardService } from './shared/guard/user-group-details-guard.service';
import { ViewUserGroupResolverService } from './shared/resolvers/view-user-group-resolver.service';
import { UserGroupProfileForCreateResolverService } from './shared/resolvers/userGroup-profile-for-create-resolver';
import { ManageUserGroupResolverService } from './shared/resolvers/manage-user-group-resolver.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'userGroup'
    },
    children: [
      {
        path: '',
        component: UserGroupCreateComponent,
        pathMatch: 'full',
        resolve: {
         userGroups: ViewUserGroupResolverService
        }
      },
      {
        path: AppRoutes.CREATE,
        component: UserGroupCreateComponent,
        pathMatch: 'full',
        resolve: {
          userGroups: UserGroupProfileForCreateResolverService
        }
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: UserGroupManageComponent,
            pathMatch: 'full',
            resolve: {
              userGroups: ManageUserGroupResolverService
            }
          },
          {
            path: AppRoutes.EDIT_ID,
            component: UserGroupEditComponent,
            pathMatch: 'full',
            resolve: {
              userGroupDto: EditUserGroupResolverService,
              // profiles: AdminProfileForCreateResolverService
            }
          }
        ]
      },
      {
        path: AppRoutes.DETAILS,
        component: UserGroupDetailsComponent,
        pathMatch: 'full',
        canActivate: [
          UserGroupDetailsGuardService
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class UserGroupRoutingModule { }
