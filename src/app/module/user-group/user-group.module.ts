import { NgModule } from '@angular/core';

import { UserGroupComponent } from './user-group.component';
import { UserGroupRoutingModule } from './user-group-routing.module';
import { UserGroupCreateComponent } from './create/user-group-create.component';
import { ViewUserGroupResolverService } from './shared/resolvers/view-user-group-resolver.service';
import { ManageUserGroupResolverService } from './shared/resolvers/manage-user-group-resolver.service';
import { EditUserGroupResolverService } from './shared/resolvers/edit-user-group-resolver.service';
import { UserGroupDetailsComponent } from './details/user-group-details.component';
import { UserGroupService } from 'app/module/user-group/shared/service/user-group.service';
import { SharedModule } from 'app/module/shared/shared.module';
import { UserGroupManageComponent } from 'app/module/user-group/manage/user-group-manage.component';
import { UserGroupEditComponent } from 'app/module/user-group/manage/edit/user-group-edit.component';
import { ActivatedRouteSnapshot } from '@angular/router';
import { UserGroupDetailsGuardService } from 'app/module/user-group/shared/guard/user-group-details-guard.service';
import { AdminProfileModule } from '../admin-profile/admin-profile.module';
import { UserGroupProfileForCreateResolverService } from './shared/resolvers/userGroup-profile-for-create-resolver';


@NgModule({
  imports: [
    SharedModule,
    UserGroupRoutingModule,
    AdminProfileModule
  ],
  declarations: [
    UserGroupComponent,
    UserGroupCreateComponent,
    UserGroupManageComponent,
    UserGroupEditComponent,
    UserGroupDetailsComponent
  ],
  providers: [
    UserGroupService,
    UserGroupDetailsGuardService,
    ViewUserGroupResolverService,
    ManageUserGroupResolverService,
    EditUserGroupResolverService,
    UserGroupProfileForCreateResolverService
  ]
})
export class UserGroupModule { }
