import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ApiConstants } from 'app/constants/api-constants';
import { UserGroupCreateModel } from 'app/module/user-group/shared/model/user-group-create-model';
import { UserGroupEditModel } from 'app/module/user-group/shared/model/user-group-edit-model';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { UserGroupDTO } from '../model/user-group-dto';
import { AppConstant } from 'app/constants/app-constant';


@Injectable()
export class UserGroupService {

  selectedForUserGroupDetail: UserGroupDTO;

  constructor(private http: HttpClient) { }

  getAllUserGroup() {
    return this.http.get<UserGroupDTO[]>(ApiConstants.ENDPOINT + ApiConstants.USER_GROUP);
  }

  searchAllUserGroups(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.USER_GROUP + ApiConstants.SEARCH, searchParam);
  }



  create(UserGroupCreateModel: UserGroupCreateModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.USER_GROUP, UserGroupCreateModel);
  }

  edit(id, UserGroupEditModel: UserGroupEditModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.USER_GROUP + ApiConstants.MODIFY + '/' + id, UserGroupEditModel);
  }

  getUserGroupById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.USER_GROUP + '/' + id);
  }

  setSelectedForUserGroupDetail(selectedForUserGroupDetail: UserGroupDTO) {
    this.selectedForUserGroupDetail = selectedForUserGroupDetail;
  }

  getSelectedForUserGroup(): UserGroupDTO {
    return this.selectedForUserGroupDetail;
  }

  searchUserGroupForSearch(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.USER_GROUP + ApiConstants.SEARCH, searchParam);
  }


  getAllUserGroupForManage(): any {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.USER_GROUP + ApiConstants.MANAGE);
  }
}
