export class UserGroupEditModel {

  name: string;
  visibility: boolean;
  profileId: number;

}
