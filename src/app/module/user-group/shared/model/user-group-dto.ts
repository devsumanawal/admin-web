import { AdminProfileDto } from "app/module/admin-profile/shared/model/admin-profile-dto-model";

export class UserGroupDTO {
    id: number;
    name: string;
    visibility: boolean;
    profileId: number;
    userProfile: AdminProfileDto;
}