import { DateUtil } from 'app/module/core/util/date-util.ts';

export class UserGroupCreateModel {

  name: string;
  visibility: string;
  profileId: any;

}
