import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { UserGroupService } from '../service/user-group.service';

@Injectable()
export class UserGroupDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.UserGroupService.getSelectedForUserGroup() == null) {
      this.router.navigate([AppRoutes.USER_GROUP]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private UserGroupService: UserGroupService,
    public router: Router
  ) { }

}
