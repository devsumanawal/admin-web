import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';

@Injectable()
export class UserGroupProfileForCreateResolverService implements Resolve<any> {
  constructor(
    private adminProfileService: AdminProfileService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.adminProfileService.getAllAdminProfileForAdminCreate().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
