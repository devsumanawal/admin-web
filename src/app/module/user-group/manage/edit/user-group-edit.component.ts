import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserGroupService } from 'app/module/user-group/shared/service/user-group.service';
import { UserGroupEditModel } from 'app/module/user-group/shared/model/user-group-edit-model';
import { AppRoutes } from 'app/constants/app-routes';
import { UserGroupDTO } from '../../shared/model/user-group-dto';
import { ProfileService } from 'app/module/core/service/profile';
import { AdminProfileDto } from 'app/module/admin-profile/shared/model/admin-profile-dto-model';

@Component({
  selector: 'app-user-group-edit',
  templateUrl: './user-group-edit.component.html',
  styleUrls: ['./user-group-edit.component.scss']
})
export class UserGroupEditComponent implements OnInit {

  editUserGroup: UserGroupEditModel = new UserGroupEditModel();
  userGroupId: number;
  @ViewChild('editUserGroupForm') editUserGroupForm: NgForm;
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private userGroupService: UserGroupService,
    private profileService : ProfileService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.USER_GROUP]);
      }

      this.firstAlreadyLoaded = true;
      this.userGroupId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditUserGroup();
  }

  ngOnDestroy() {
    this.userGroupService.setSelectedForUserGroupDetail(null);
  }

  setEditUserGroup() {
    const userGroupDTO: UserGroupDTO = this.userGroupService.getSelectedForUserGroup();
    this.editUserGroup.name = userGroupDTO.name;
    this.editUserGroup.visibility = userGroupDTO.visibility;
    this.editUserGroup.profileId = userGroupDTO.profileId;
  }

  submitEditUserGroup() {
    this.buttonPressed = true;

    this.userGroupService
      .edit(this.userGroupId, this.editUserGroup)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editUserGroupForm.form.contains(error.fieldType)) {
                this.editUserGroupForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}