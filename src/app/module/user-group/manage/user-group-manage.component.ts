import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { PaginationComponent } from 'ngx-bootstrap';
import { AppRoutes } from 'app/constants/app-routes';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { UserGroupDTO } from '../shared/model/user-group-dto';
import { UserGroupService } from '../shared/service/user-group.service';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { AdminProfileDto } from 'app/module/admin-profile/shared/model/admin-profile-dto-model';

@Component({
  selector: 'app-user-group-manage',
  templateUrl: './user-group-manage.component.html',
  styleUrls: ['./user-group-manage.component.scss']
})
export class UserGroupManageComponent implements OnInit {
  userGroupForManage: Array<UserGroupDTO> = new Array<UserGroupDTO>();
  selectedUserGroup: UserGroupDTO = new UserGroupDTO();
  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
  cardViewEnabled: Boolean = true;

  filterParam: any;
  adminProfiles: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;

  adminProfileDTO: AdminProfileDto = new AdminProfileDto();


  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private location: Location,
    private userGroupService: UserGroupService,
    private toastr: ToastrService,
    private router: Router,
    private adminProfileService: AdminProfileService,
    public navigationService: NavigationService,
  ) {
  }

  ngOnInit() {
    this.resetSearch();
    this.userGroupService.setSelectedForUserGroupDetail(null);
    this.cardViewEnabled = localStorage.getItem(SessionStorageConstants.CardViewEnabled) === 'true';
  }

  resetSearch() {
    this.filterParam = [
      { key: 'name', value: '', label: 'Name', alias: 'Name' },
    ];
    this.searchUserGroupOnUserInput();
  }


  searchUserGroupOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllUserGroupForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }


  getAllUserGroupForSearch(event?: PageChangedEvent) {

    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }
    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };
    this.userGroupService
      .searchUserGroupForSearch(this.searchParam)
      .subscribe(successResponse => {
        this.userGroupForManage = successResponse.object;
        this.searchButtonpressed = false;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
          this.searchButtonpressed = false;
        });
  }

  goBack() {
    this.router.navigate(['userGroup']);
  }


  clickViewDetailButton(UserGroupDTO: UserGroupDTO) {
    this.setUserGroupDto(UserGroupDTO);
    this.router.navigate([AppRoutes.USER_GROUP, AppRoutes.DETAILS]);
  }

  clickEditUserGroup(selected: UserGroupDTO) {
    this.userGroupService.setSelectedForUserGroupDetail(selected);
    this.router.navigate([AppRoutes.USER_GROUP, AppRoutes.MANAGE, AppRoutes.EDIT, selected.id]);
  }


  setUserGroupDto(userGroupDTO: UserGroupDTO) {
    this.userGroupService.setSelectedForUserGroupDetail(userGroupDTO);
  }

  getAdminProfiles() {
    this.adminProfileService.getAllAdminProfiles()
      .subscribe((successResponse) => {
        this.adminProfiles = successResponse;
      }, errorResponse => {
        this.toastr.error(errorResponse.error.message);
      }
      );
  }


  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  toggleCardViewEnabled() {
    this.cardViewEnabled = !this.cardViewEnabled;
    localStorage.setItem(SessionStorageConstants.CardViewEnabled, (this.cardViewEnabled ? 'true' : 'false'));
  }


  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('User Group', 'User Group Setup', 'Manage User Group', childRoleName);
  }

  isActive(active: string) {
    return active === "Y";
  }
}