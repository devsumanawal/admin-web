import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';

import { ToastrService } from 'ngx-toastr';
import { UserGroupCreateModel } from 'app/module/user-group/shared/model/user-group-create-model';
import { UserGroupService } from '../shared/service/user-group.service';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { ProfileService } from 'app/module/core/service/profile/profile.service';


@Component({
  selector: 'app-user-group-create',
  templateUrl: './user-group-create.component.html'
})
export class UserGroupCreateComponent implements OnInit {

  @ViewChild('createUserGroupForm')
  createUserGroupForm: any;

  userGroup: UserGroupCreateModel = new UserGroupCreateModel();
  adminProfileForCreate: Array<any> = [];
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  profiles = [];

  constructor(
    private location: Location,
    private userGroupService: UserGroupService,
    private profileService: ProfileService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
  }

  create(form: NgForm) {
    this.buttonPressed = true;
    this.userGroup.visibility = "Y";
    this.userGroupService
      .create(this.userGroup)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.resetForm();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (form.form.contains(error.fieldType)) {
                form.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }


  back() {
    this.location.back();
  }

  resetForm() {
    this.userGroup = new UserGroupCreateModel();
    this.createUserGroupForm.form.markAsPristine();
    this.createUserGroupForm.form.markAsUntouched();
  }

}
