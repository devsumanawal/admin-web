import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'app/constants/app-routes';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { SearchParam } from 'app/module/core/models/search-param';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { NgForm } from '@angular/forms';
import { PaginationComponent } from 'ngx-bootstrap';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { UserGroupDTO } from './shared/model/user-group-dto';
import { UserGroupService } from './shared/service/user-group.service';

@Component({
  selector: 'app-user-group',
  templateUrl: './user-group.component.html',
  styleUrls: ['./user-group.component.scss']
})
export class UserGroupComponent implements OnInit {
  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;

  UserGroup: Array<UserGroupDTO> = new Array<UserGroupDTO>();
  cardViewEnabled: Boolean = true;

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;

  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private UserGroupService: UserGroupService,
    private toastr: ToastrService,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.resetSearch();
    this.setUserGroupDto(null);
    this.cardViewEnabled = localStorage.getItem(SessionStorageConstants.CardViewEnabled) === 'true';
  }

  searchUserGroupOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllUserGroupsForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllUserGroupsForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }
    
    this.UserGroupService
      .searchAllUserGroups(this.searchParam)
      .subscribe(successResponse => {
        this.UserGroup = successResponse.object;
        this.totalItems = successResponse.totalCount;
        this.searchButtonpressed = false;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
          this.searchButtonpressed = false;
        });
  }

  resetSearch() {
    this.filterParam = [
      { key: 'adminProfile.id', value: '', condition: '=' },
      { key: 'visibility.name', value: '', condition: '=' },
      { key: 'userGroup.name', value: '' }
    ];
    this.searchUserGroupOnUserInput();
  }

  setUserGroupDto(UserGroupDTO: UserGroupDTO) {
    this.UserGroupService.setSelectedForUserGroupDetail(UserGroupDTO);
  }

  clickViewDetailButton(UserGroupDTO: UserGroupDTO) {
    this.setUserGroupDto(UserGroupDTO);
    this.router.navigate([AppRoutes.OFFICE_TYPE, AppRoutes.DETAILS]);
  }

  toggleCardViewEnabled() {
    this.cardViewEnabled = !this.cardViewEnabled;
    localStorage.setItem(SessionStorageConstants.CardViewEnabled, (this.cardViewEnabled ? 'true' : 'false'));
  }

  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRole('User Group Setup', 'Setup User Group', childRoleName);
  }
}

