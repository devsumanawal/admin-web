import { DateUtil } from 'app/module/core/util/date-util.ts';

export class StartEndDateModel {
  startDate: Date = DateUtil.getPreviousWeekDate();
  endDate: Date = DateUtil.getTodayDate();
}
