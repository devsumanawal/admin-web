export class NavigationForCoreUi {
  divider: boolean;
  title = false;
  name: string;

  url: string;
  icon: string;
  badge: Badge;
  children: NavigationForCoreUi[];
}

export class Badge {
  variant: string;
  text: string;
}
