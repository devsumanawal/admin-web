export class ProfileRoleViewModel {
    id: number;
    name: string;
    description: string;
    isActive: boolean;
    parentName: string;
    childRoles: Array<ProfileRoleViewModel> = [];
}
