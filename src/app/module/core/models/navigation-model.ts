import { NavigationRoleResponseModel } from './navigation-role-response-model';

export class NavigationModel {
  name: string;
  position: number;
  navigation: string;
  icon: string;
  uiGroupName: string;
  roles: Array<NavigationRoleResponseModel>;

  constructor(
    name: string,
    position: number,
    navigation: string,
    icon: string,
    uiGroupName: string,
    roles: Array<NavigationRoleResponseModel>
  ) {
    this.name = name;
    this.position = position;
    this.navigation = navigation;
    this.icon = icon;
    this.uiGroupName = uiGroupName;
    this.roles = roles;
  }
}
