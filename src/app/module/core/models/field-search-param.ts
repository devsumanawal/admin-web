export class FieldSearchParam {
  key: string;
  value: any;
  condition?: string;
  alias?: string;
}
