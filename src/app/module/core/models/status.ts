export class Status {
  id: number;
  name: string;
  description: string;
  info: string;
}
