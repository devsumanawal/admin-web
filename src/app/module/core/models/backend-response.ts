export class BackendResponse {
    message: string;
    status: string;
    statusCode: number;
    success: Boolean;
}
