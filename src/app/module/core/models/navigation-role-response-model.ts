export class NavigationRoleResponseModel {
  name: string;
  description: string;
  uiGroupName: string;
  navigation: string;
  parentName: string;
  icon: string;
  position: number;
  createPending: string;
  childRoles: Array<NavigationRoleResponseModel>;
  active: any;
}
