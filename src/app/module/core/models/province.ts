export class ProvinceList {
    provinceList: Province[];
}

export class Province {
    id: number;
    description: string;
    name: string;
    provinceName: string;
    districtList: string[];

}