export class MerchantKymSearchParam {
  statusName: string;
  regMode: string;
  merchantName: string;
  userName: string;
  merchantCode: string;
  clientId: number;
  ownership: string;
  merchantType: number;
  panNumber: string;
  mobileNumber: string;
  hasSubmerchant: string;
  fromDate: string;
  toDate: string;
}
