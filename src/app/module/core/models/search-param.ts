import { FieldSearchParam } from './field-search-param';
import { SearchOrderByParam } from 'app/module/core/models/search-order-by-param';


export class SearchParam {
  search: Array<FieldSearchParam>;
  page: any;
  size: any;
}
