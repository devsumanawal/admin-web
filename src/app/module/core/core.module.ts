import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptorService } from './interceptor/request-interceptor.service';
import { AuthorizationService } from './service/authorization.service';
import { AuthGuardService } from './guard/auth-guard.service';
import { PasswordPoliciesValidationService } from 'app/module/core/service/validation/password-policies-validation.service';
import { TreeViewUtilityService } from 'app/module/core/util/tree-view/tree-view-utility.service';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { OfficeService } from './service/office';
import { ProfileService } from './service/profile';
import { OfficeTypeService } from './service/office-type/officeType.service';
import { MinistryService } from './service/office/ministry.service';
import { DepartmentService } from './service/office/department.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptorService,
      multi: true
    },
    AuthorizationService,
    TreeViewUtilityService,
    AuthGuardService,
    PasswordPoliciesValidationService,
    NavigationService,
    OfficeService,
    ProfileService,
    OfficeTypeService,
    MinistryService,
    DepartmentService
  ],
  exports: []
})
export class CoreModule { }
