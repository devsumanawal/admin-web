import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { AuthorizationService } from '../service/authorization.service';


@Injectable()
export class RequestInterceptorService implements HttpInterceptor {

    clonedRequest: HttpRequest<any>;

    constructor(private authService: AuthorizationService,
        private router: Router,
        private toastr: ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        this.clonedRequest = request.clone({
            setHeaders: { Authorization: `${this.authService.getToken()}` }
        });

        return next.handle(this.clonedRequest)
            .do(
                (event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        if (event.headers.get(SessionStorageConstants.Authorization)) {
                            this.authService.setToken(event.headers.get(SessionStorageConstants.Authorization));
                        }
                    }
                },
                (error: any) => {
                    if (error instanceof HttpErrorResponse) {
                        if (error.status) {
                            switch (error.status) {
                                case 401:
                                    // if (!this.clonedRequest.url.endsWith(ApiConstants.ADMIN_AUTHENTICATION)) {
                                    this.router.navigate([AppRoutes.LOGIN]);
                                    break;
                                case 404:
                                    // this.toastr.error('Not Found');
                                    // this.router.navigate([AppRoutes.notFound], { skipLocationChange: true });
                                    break;
                                case 500:
                                    // this.toastr.error('Error');
                                    // this.router.navigate([AppRoutes.serverError], { skipLocationChange: true });
                                    break;
                            }
                        } else {
                            this.toastr.error('Connection failed');
                            // this.router.navigate([AppRoutes.serverError], { skipLocationChange: true });
                        }
                    }
                }
            );
    }

}
