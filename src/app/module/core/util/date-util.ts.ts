export class DateUtil {

  static getTodayDateString(): String {
    return new Date().toISOString().slice(0, 10);
  }

  static getHTMLDateString(date: Date): String {
    return new Date(date).toISOString().slice(0, 10);
  }

  static getPreviousMonthDateString(): String {
    const previousMonthDate: Date = new Date();
    previousMonthDate.setDate(new Date().getDate() - 30);
    return previousMonthDate.toISOString().slice(0, 10);
  }

  static getTodayDate(): Date {
    return new Date();
  }

  static getNextDayDate(): Date {
    const previousDayDate: Date = new Date();
    previousDayDate.setDate(new Date().getDate() + 1);
    return previousDayDate;
  }

  static getPreviousMonthDate(): Date {
    const previousMonthDate: Date = new Date();
    previousMonthDate.setMonth(new Date().getMonth() - 1);
    return previousMonthDate;
  }

  static getPreviousDayDate(): Date {
    const previousDayDate: Date = new Date();
    previousDayDate.setDate(new Date().getDate() - 1);
    return previousDayDate;
  }

  static getPreviousWeekDate(): Date {
    const previousWeekDate: Date = new Date();
    previousWeekDate.setDate(new Date().getDate() - 7);
    return previousWeekDate;
  }

  static dateSort(items: any) {
    const sortedDates = items.sort((a: any, b: any) =>
      b.recordedDate - a.recordedDate);
    return sortedDates;
  }

  static getLocalDateIn_Java_yyyy_MM_dd_Format(date: Date) {
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  }

  static getLocalDateIn_Java_yyyy_MM_dd_hh_mm_ss_Format(date: Date) {
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()
  }

  static getDateBeforeThreeMinutes(mintues: number): Date {
    const currentDate: Date = new Date();
    currentDate.getMinutes();
    currentDate.setMinutes(new Date().getMinutes() - mintues);
    return currentDate;

  }

  static getFirstDateOfCurrentYear(){
    var date = new Date(new Date().getFullYear(), 0, 1);
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  }

  static getStartingDateOfCurrentMonth() {
    var date = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  }

  static getFirstDayO
}
