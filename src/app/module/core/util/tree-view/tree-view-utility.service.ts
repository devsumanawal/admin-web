import { Injectable } from '@angular/core';
import { TreeviewItem, TreeviewHelper, DownlineTreeviewItem } from 'ngx-treeview';
import { ChildrenOutletContexts } from '@angular/router/src/router_outlet_context';
import { ProfileRoleViewModel } from 'app/module/core/models/profile-role-view-model';

@Injectable()
export class TreeViewUtilityService {

  // previouslySelectedRole = new Map<number, TreeviewItem[]>();

  constructor() { }


  getTreeViewFromRoles(roles: ProfileRoleViewModel[]): TreeviewItem[] {
    const rolesTreeView: TreeviewItem[] = [];
    roles.forEach(role => {
      rolesTreeView.push(this.createTreeViewItem(role));
    });
    return rolesTreeView;
  }

  private createTreeViewItem(role: ProfileRoleViewModel): TreeviewItem {
    if (role.childRoles) {
      const treeViewItem = new TreeviewItem({
        text: role.description,
        value: role.id,
        checked: role.isActive,
        children: role.childRoles ? this.getChildTreeViewItems(role.childRoles) : []
      });
      return treeViewItem;
    }
  }

  private getChildTreeViewItems(childRoles: ProfileRoleViewModel[]): TreeviewItem[] {
    const finalChildRoles = [];
    childRoles.forEach(childRole => {
      finalChildRoles.push(
        new TreeviewItem({
          text: childRole.description,
          value: childRole.id,
          checked: childRole.isActive,
          children: childRole.childRoles ? this.getChildTreeViewItems(childRole.childRoles) : []
        })
      );
    });
    return finalChildRoles;
  }

  addParentRolesId(treeViewRoles: TreeviewItem[], selectedRoles: number[]): number[] {
    const parentRolesID: number[] = [];
    selectedRoles.forEach(childRole => {
      this.getParentRoleId(treeViewRoles, childRole, parentRolesID);
    });
    return parentRolesID;
  }

  private getParentRoleId(treeViewRoles: TreeviewItem[], childRoleId: number, selectedRoles: number[]): number[] {
    const child = TreeviewHelper.findItemInList(treeViewRoles, childRoleId);
    treeViewRoles.forEach(rootTreeViewItem => {
      const parentRole = TreeviewHelper.findParent(rootTreeViewItem, child);
      if (parentRole) {
        if (!selectedRoles.includes(parentRole.value)) {
          selectedRoles.push(parentRole.value);
        }
        this.getParentRoleId(treeViewRoles, parentRole.value, selectedRoles);
      }
    });
    return selectedRoles;
  }

  addOrRemoveViewRole(treeViewRoles: TreeviewItem[], childRoleId: number[],
    previouslySelectedRoleExceptViewRole: Map<number, TreeviewItem[]>) {

    childRoleId.forEach(selectedRoleId => {
      const selectedTreeViewItem = TreeviewHelper.findItemInList(
        treeViewRoles,
        selectedRoleId
      );
      treeViewRoles.forEach(rootTreeViewItem => {
        const parentRole = TreeviewHelper.findParent(
          rootTreeViewItem,
          selectedTreeViewItem
        );
        if (parentRole) {
          // add view role
          parentRole.children.forEach(c => {
            if (c.text.toLowerCase().startsWith('view')) {
              c.disabled = false;
              c.checked =
                this.getChildrens(
                  this.findParentTreeViewItem(treeViewRoles, c)
                ).length === 1
                  ? true
                  : this.isAnyRoleSelected(
                    treeViewRoles,
                    c,
                    previouslySelectedRoleExceptViewRole
                  );
              c.disabled = c.checked;
            }
          });
          this.addOrRemoveViewRole(treeViewRoles, [].concat(parentRole.value), previouslySelectedRoleExceptViewRole);
        }
      });
    });
  }

  private isAnyRoleSelected(treeViewRoles: TreeviewItem[], viewRoleChild: TreeviewItem,
    previouslySelectedRoleExceptViewRole: Map<number, TreeviewItem[]>): boolean {

    let selected = false;
    const parentItem = this.findParentTreeViewItem(treeViewRoles, viewRoleChild);
    const childrens = this.getChildrens(parentItem);

    const checkedChildrens = [];
    childrens.map(child => {
      if (child.checked === true) {
        return checkedChildrens.push(child);
      }
    });

    const checkedChildrensExceptViewRole = [];
    childrens.map(child => {
      if (child.checked === true && !child.text.toLowerCase().startsWith('view')) {
        return checkedChildrensExceptViewRole.push(child);
      }
    });

    let previouslySelectedRoleOfParentRoleExceptView = previouslySelectedRoleExceptViewRole.get(parentItem.value);

    if (previouslySelectedRoleOfParentRoleExceptView === undefined) {
      previouslySelectedRoleOfParentRoleExceptView = [];
    }

    if (previouslySelectedRoleOfParentRoleExceptView.length > 0 && checkedChildrensExceptViewRole.length === 0) {
      selected = false;
    } else if (checkedChildrens.length > 0) {
      selected = true;
    }
    return selected;
  }

  private findParentTreeViewItem(treeViewRoles: TreeviewItem[], item: TreeviewItem): TreeviewItem {
    let parentTreeViewItem: TreeviewItem;
    treeViewRoles.forEach(rootTreeViewItem => {
      const parentRole = TreeviewHelper.findParent(rootTreeViewItem, item);
      if (parentRole) {
        // this.findSuperParentTreeViewItem(treeViewRoles, parentRole);
        parentTreeViewItem = parentRole;
      }
    });
    return parentTreeViewItem;
  }

  findSecondLastSuperParentValueOfDownlineTreeviewItem(selectedItem: DownlineTreeviewItem): number {
    let parent = selectedItem.parent;
    let previousParent = parent;
    while (parent.parent != null) {
      previousParent = parent;
      parent = parent.parent;
    }

    return previousParent.item.value;
  }

  findSuperParentValueOfDownlineTreeviewItem(selectedItem: DownlineTreeviewItem): number {
    const parent = selectedItem.parent;
    if (parent === null) {
      return selectedItem.item.value;
    } else {
      return this.findSuperParentValueOfDownlineTreeviewItem(parent);
    }
  }
  private getChildrens(parent: TreeviewItem, childrens: TreeviewItem[] = []): TreeviewItem[] {
    parent.children.forEach(child => {
      if (child.children) {
        // childrens.push(child);
        this.getChildrens(child, childrens);
      }
      childrens.push(child);
    });
    return childrens;
  }

  setAllChecked(treeViewRoles: TreeviewItem[], isChecked: boolean) {
    treeViewRoles.forEach(treeViewItem => {
      if (treeViewItem.children) {
        treeViewItem.checked = false;
        this.setAllChecked(treeViewItem.children, false);
      }
      treeViewItem.checked = false;
    });
  }

  getTreeviewCheckedValues(treeViewRoles: TreeviewItem[]): number[] {
    const checkedValues: number[] = []
    const treeviewCheckedItemArray = treeViewRoles
      .map(treeviewItem => treeviewItem.getSelection().checkedItems);

    treeviewCheckedItemArray
      .forEach(treeviewItems =>
        treeviewItems
          .forEach(treeviewItem => checkedValues.push(treeviewItem.value))
      );

    return checkedValues;
  }

  //   select(item: TreeviewItem) {
  //     if (item.children === undefined) {
  //         this.selectItem(item);
  //     }
  // }
  // private selectItem(item: TreeviewItem) {
  //   this.dropdownTreeviewComponent.dropdownDirective.close();
  //   if (this.dropdownTreeviewSelectI18n.selectedItem !== item) {
  //       this.dropdownTreeviewSelectI18n.selectedItem = item;
  //       if (this.value !== item.value) {
  //           this.value = item.value;
  //           this.valueChange.emit(item.value);
  //       }
  //   }
  // }

  // private selectAll() {
  //     const allItem = this.dropdownTreeviewComponent.treeviewComponent.allItem;
  //     this.selectItem(allItem);
  // }
}
