import { TestBed, inject } from '@angular/core/testing';

import { TreeViewUtilityService } from './tree-view-utility.service';

describe('TreeViewUtilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TreeViewUtilityService]
    });
  });

  it('should be created', inject([TreeViewUtilityService], (service: TreeViewUtilityService) => {
    expect(service).toBeTruthy();
  }));
});
