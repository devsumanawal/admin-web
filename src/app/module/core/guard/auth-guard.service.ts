import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/interfaces';
import { AuthorizationService } from 'app/module/core/service/authorization.service';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    private authService: AuthorizationService,
    private router: Router
  ) { }

  canActivate() {
    if (this.authService.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate([AppRoutes.LOGIN]);
      return false;
    }
  }
}
