import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';

@Injectable()
export class IsFirstLoginGuard implements CanActivate {
  constructor() { }

  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const isFirstLogin = sessionStorage.getItem(SessionStorageConstants.IsFirstLogin);
    if (isFirstLogin === null) {
      return false;
    }
    if (isFirstLogin || isFirstLogin.toUpperCase() === 'TRUE') {
      return true;
    }
    return false;
  }

}
