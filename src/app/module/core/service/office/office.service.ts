import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';
import { FiscalYearDTO } from 'app/module/fiscal-year/shared/model/fiscal-year-dto';

export interface Office {
    id: number;
    name: string;
    unicodeName: string;
    officeCode: string;
    officeType: {
        id: string;
        name: string;
        unicodeName: string;
    };
}

export interface Level {
    name: string;
}

@Injectable()
export class OfficeService {
    readonly offices = this.http.get<Office[]>(
        ApiConstants.ENDPOINT + ApiConstants.OFFICE
    );

    readonly fiscalYears = this.http.get<FiscalYearDTO[]>(
        ApiConstants.ENDPOINT + ApiConstants.FISCAL_YEAR
    );

    constructor(private http: HttpClient) { }
}
