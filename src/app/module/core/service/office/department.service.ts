import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';

export interface Department {
    id: number;
    name: string;
    unicodeName: string;
    code: string;
}

@Injectable()
export class DepartmentService {
    readonly departments = this.http.get<Department[]>(
        ApiConstants.ENDPOINT + ApiConstants.DEPARTMENT
    );

    constructor(private http: HttpClient) { }
}