import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';

export interface Ministry {
    id: number;
    name: string;
    unicodeName: string;
    code: string;
}

export class Donor {
    id: number;
    name: string;
    unicodeName: string;
    code: string;
}

@Injectable()
export class MinistryService {
    readonly ministries = this.http.get<Ministry[]>(
        ApiConstants.ENDPOINT + ApiConstants.MINISTRY
    );

    readonly donors = this.http.get<Donor[]>(
        ApiConstants.ENDPOINT + ApiConstants.DONOR
    );






    constructor(private http: HttpClient) { }
}