import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';

export interface Profile {
    id: number;
    name: string;
}

@Injectable()
export class ProfileService {
    readonly profiles = this.http.get<Profile[]>(
        ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE
    );
    
    constructor(private http: HttpClient) { }
}