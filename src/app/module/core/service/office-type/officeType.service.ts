import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';

export interface OfficeType {
        id: string;
        name: string;
        unicodeName: string;
}

export interface Province {
    id: string;
    name: string;
    unicodeName: string;
}

@Injectable()
export class OfficeTypeService {
    readonly officeTypes = this.http.get<OfficeType[]>(
        ApiConstants.ENDPOINT + ApiConstants.OFFICE_TYPE
    );

    readonly provinces = this.http.get<Province[]>(
        ApiConstants.ENDPOINT + ApiConstants.PROVINCE
    );
    
    constructor(private http: HttpClient) { }
}