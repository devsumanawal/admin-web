import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ApiConstants } from '../../../../constants/api-constants';
import { NavigationModel } from 'app/module/core/models/navigation-model';
import { NavigationForCoreUi } from 'app/module/core/models/navigation-for-core-ui';
import { AppRoutes } from 'app/constants/app-routes';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';

@Injectable()
export class NavigationService {

    navigationModel: NavigationModel[] = [];
    navigationListForCoreUiNav: NavigationForCoreUi[] = [];

    constructor(private http: HttpClient) { }

    syncNavigationList() {
        this.getAllNavigation().subscribe(response => {
            this.navigationModel = [];
            this.clearNavigationListForCoreUiNav();
            this.navigationModel = response;
            this.syncNavigationListForCoreUiNav();
        });
    }

    clearNavigationListForCoreUiNav() {
        this.navigationListForCoreUiNav.length = 0;
    }

    private getAllNavigation(): Observable<NavigationModel[]> {
        return this.http.get<NavigationModel[]>(
            ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + ApiConstants.NAVIGATION
        );
    }

    private syncNavigationListForCoreUiNav() {
        // this.setDashboardNavigationForCoreUi();

        this.navigationModel.forEach(navigationModel => {

            const navigationForCoreUi = new NavigationForCoreUi();
            navigationForCoreUi.name = navigationModel.uiGroupName;
            if (navigationModel.navigation) {
                navigationForCoreUi.url = navigationModel.navigation;
            } else {
                navigationForCoreUi.url = '/' + AppRoutes.DASHBOARD;
                // navigationForCoreUi.title = true;
            }
            navigationForCoreUi.icon = navigationModel.icon;

            if (navigationModel.roles) {
                navigationForCoreUi.children = [];

                navigationModel.roles.forEach(navigationModelRole => {
                    const navigationChildrenForCoreUi = new NavigationForCoreUi();

                    navigationChildrenForCoreUi.name = navigationModelRole.description;
                    if (navigationModelRole.navigation) {
                        navigationChildrenForCoreUi.url = navigationModelRole.navigation;
                    } else {
                        navigationChildrenForCoreUi.url = '/';
                        // navigationChildrenForCoreUi.title = true;
                    }
                    navigationChildrenForCoreUi.icon = navigationModelRole.icon;

                    if (navigationModelRole.childRoles) {
                        navigationChildrenForCoreUi.children = [];

                        navigationModelRole.childRoles.forEach(childRoles => {
                            const childNav = new NavigationForCoreUi();

                            childNav.name = childRoles.description;
                            if (childRoles.navigation) {
                                childNav.url = childRoles.navigation;
                            } else {
                                childNav.url = '/';
                                // navigationChildrenForCoreUi.title = true;
                            }
                            childNav.icon = childRoles.icon;

                            navigationChildrenForCoreUi.children.push(childNav);
                        });
                    }

                    navigationForCoreUi.children.push(navigationChildrenForCoreUi);
                });

            }

            this.navigationListForCoreUiNav.push(navigationForCoreUi);
        });

    }

    private setDashboardNavigationForCoreUi() {
        const navigationForCoreUi = new NavigationForCoreUi();
        navigationForCoreUi.name = 'Dashboard';
        navigationForCoreUi.url = '/' + AppRoutes.DASHBOARD;
        navigationForCoreUi.icon = 'mdi mdi-view-dashboard';
        this.navigationListForCoreUiNav.push(navigationForCoreUi);

    }

    private setDividerNavigationForCoreUi() {
        const navigationForCoreUi2 = new NavigationForCoreUi();
        navigationForCoreUi2.divider = true;
        navigationForCoreUi2.url = '/' + AppRoutes.DASHBOARD;
        this.navigationListForCoreUiNav.push(navigationForCoreUi2);
    }

    public getNavigationModelByName(name: string): NavigationModel {
        return this.navigationModel.find(navigationModel => navigationModel.name === name);
    }

    public getRole(navigationModelName: string, roleName: string): NavigationRoleResponseModel {
        const navigationModel = this.getNavigationModelByName(navigationModelName);
        if (navigationModel && navigationModel.roles) {
            return navigationModel.roles.find(role => role.name === roleName);
        }
        return null;
    }

    public getChildRole(navigationModelName: string, roleName: string, childRoleName: string): NavigationRoleResponseModel {
        const role = this.getRole(navigationModelName, roleName);
        if (role && role.childRoles) {
            return role.childRoles.find(childRole => childRole.name === childRoleName);
        }
        return null;
    }

    public getChildRoleOfChildRole(navigationModelName: string, roleName: string, childRoleName: string, innerChildRoleName: string)
        : NavigationRoleResponseModel {
        const role = this.getChildRole(navigationModelName, roleName, childRoleName);
        if (role && role.childRoles) {
            return role.childRoles.find(innerChildRole => innerChildRole.name === innerChildRoleName);
        }
        return null;
    }
}
