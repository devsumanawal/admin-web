import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { PasswordPolicyService } from './../../../password-policy/shared/password-policy.service';
import { PasswordPolicyModel } from '../../../password-policy/shared/password-policy-model';
import { PasswordPolicyToTextPipe } from 'app/module/password-policy/pipes/password-policy-to-text.pipe';

@Injectable()
export class PasswordPoliciesValidationService {
  passwordPolicies: PasswordPolicyModel[] = [];

  constructor(
    private passwordPolicyService: PasswordPolicyService,
    private toastr: ToastrService,
    private passwordPolicyToTextPipe: PasswordPolicyToTextPipe
  ) { }

  getPasswordPolicies(passwordPolicies: PasswordPolicyModel[]) {
    passwordPolicies.forEach((passwordPolicy: PasswordPolicyModel) => {
      if (passwordPolicy.regularExpression) {
        passwordPolicy.regularExpressionToRegex =
          new RegExp(passwordPolicy.regularExpression, 'g');
        this.passwordPolicies.push(passwordPolicy);
      } else {
        passwordPolicy.regularExpressionToRegex = null;
        this.passwordPolicies.push(passwordPolicy);
      }
    });
  }

  validate(password: string, passwordPolicies: PasswordPolicyModel[]): string {
    this.passwordPolicies = [];
    this.getPasswordPolicies(passwordPolicies);
    for (const passwordPolicy of this.passwordPolicies) {
      if (+passwordPolicy.passwordPolicyValue !== 0) {
        if (passwordPolicy.regularExpressionToRegex === null) {

          if (passwordPolicy.passwordPolicyDesc === 'password.max.length') {
            if (password.length > +passwordPolicy.passwordPolicyValue) {
              return this.passwordPolicyToTextPipe.transform(passwordPolicy);
            }
          } else if (passwordPolicy.passwordPolicyDesc === 'password.min.length') {
            if (password.length < +passwordPolicy.passwordPolicyValue) {
              return this.passwordPolicyToTextPipe.transform(passwordPolicy);
            }
          }

        } else {
          const result = password.match(passwordPolicy.regularExpressionToRegex);
          if (result !== null) {
            if (passwordPolicy.passwordRegularExpressionLengthType === 'MAX') {
              if (result.length > +passwordPolicy.passwordPolicyValue) {
                return this.passwordPolicyToTextPipe.transform(passwordPolicy);
              }
            } else if (passwordPolicy.passwordRegularExpressionLengthType === 'MIN') {
              if (result.length < +passwordPolicy.passwordPolicyValue) {
                return this.passwordPolicyToTextPipe.transform(passwordPolicy);
              }
            }
          } else {
            return this.passwordPolicyToTextPipe.transform(passwordPolicy);
          }
        }
      }
    }
    return '';
  }
}
