import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ApiConstants } from '../../../constants/api-constants';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';

@Injectable()
export class AuthorizationService {

  public token: string;

  constructor() {
  }

  setToken(authToken) {
    sessionStorage.setItem(SessionStorageConstants.Authorization, authToken);
  }

  getToken() {
    return sessionStorage.getItem(SessionStorageConstants.Authorization);
  }

  isAuthenticated(): boolean {
    if (this.getToken() == null) {
      return false;
    }
    return true;
  }

  removeToken() {
    sessionStorage.removeItem(SessionStorageConstants.Authorization);
    sessionStorage.removeItem(SessionStorageConstants.IsFirstLogin);
  }

}
