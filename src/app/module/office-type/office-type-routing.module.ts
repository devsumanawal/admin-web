import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OfficeTypeComponent } from 'app/module/office-type/office-type.component';
import { AppRoutes } from 'app/constants/app-routes';
import { ViewOfficeTypeResolverService } from 'app/module/office-type/shared/resolvers/view-office-type-resolver.service';
import { OfficeTypeCreateComponent } from 'app/module/office-type/create/office-type-create.component';
// import { AdminProfileForCreateResolverService } from 'app/module/admin/shared/resolvers/admin-profile-for-create-resolver.service';
import { ManageOfficeTypeResolverService } from 'app/module/office-type/shared/resolvers/manage-office-type-resolver.service';
import { CommonModule } from '@angular/common';
import { OfficeTypeManageComponent } from 'app/module/office-type/manage/office-type-manage.component';
import { OfficeTypeDetailsComponent } from './details/office-type-details.component';
import { OfficeTypeEditComponent } from './manage/edit/office-type-edit.component';
import { EditOfficeTypeResolverService } from './shared/resolvers/edit-office-type-resolver.service';
import { OfficeTypeDetailsGuardService } from './shared/guard/office-type-details-guard.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'OfficeType'
    },
    children: [
      {
        path: AppRoutes.CREATE,
        component: OfficeTypeCreateComponent,
        pathMatch: 'full',
        resolve: {
        }
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: OfficeTypeManageComponent,
            pathMatch: 'full',
            resolve: {
            }
          },
          {
            path: AppRoutes.EDIT_ID,
            component: OfficeTypeEditComponent,
            pathMatch: 'full',
            resolve: {
              officeTypeDto: EditOfficeTypeResolverService,
            }
          }
        ]
      },
      {
        path: AppRoutes.DETAILS,
        component: OfficeTypeDetailsComponent,
        pathMatch: 'full',
        canActivate: [
          OfficeTypeDetailsGuardService
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class OfficeTypeRoutingModule { }
