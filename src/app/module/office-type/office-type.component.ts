import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'app/constants/app-routes';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { SearchParam } from 'app/module/core/models/search-param';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { NgForm } from '@angular/forms';
import { PaginationComponent } from 'ngx-bootstrap';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { OfficeTypeDTO } from './shared/model/office-type-dto';
import { OfficeTypeService } from './shared/service/office-type.service';

@Component({
  selector: 'app-office-type',
  templateUrl: './office-type.component.html',
  styleUrls: ['./office-type.component.scss']
})
export class OfficeTypeComponent implements OnInit {


  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;

  officeTypes: Array<OfficeTypeDTO> = [];
  showDetails = false;

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private officeTypeService: OfficeTypeService,
    private toastr: ToastrService,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.officeTypeService.setSelectedOfficeType(null);
  }

  getAllOfficeTypeForSearch(event?: PageChangedEvent) {
    this.officeTypeService
      .searchOfficeTypes(this.searchParam)
      .subscribe(successResponse => {
        this.officeTypes = successResponse.object;
        this.totalItems = successResponse.totalCount;
        this.searchButtonpressed = false;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
          this.searchButtonpressed = false;
        });
  }

  clickViewDetailButton(officeType: OfficeTypeDTO) {
    this.officeTypeService.setSelectedOfficeType(officeType);
    this.router.navigate([AppRoutes.OFFICE_TYPE, AppRoutes.DETAILS, officeType.id]);
  }

  isActive(statusDescription: string) {
    return statusDescription === 'Y';
  }

  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('Office Type', 'Office Type Setup','Manage Office Type', childRoleName);
  }
}