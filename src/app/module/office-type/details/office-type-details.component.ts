import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { OfficeTypeService } from '../shared/service/office-type.service';
import { OfficeTypeDTO } from '../shared/model/office-type-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';


@Component({
  selector: 'app-office-type-details',
  templateUrl: './office-type-details.component.html',
  styleUrls: ['./office-type-details.component.scss']
})
export class OfficeTypeDetailsComponent implements OnInit {

  officeType: OfficeTypeDTO;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ngRoute: Router,
    private officeTypeService: OfficeTypeService,
    private location: Location
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.OFFICE_TYPE]);
      }

      this.firstAlreadyLoaded = true;

    });

    this.officeType = this.officeTypeService.getSelectedOfficeType();
  }

  ngOnDestroy() {
    this.officeTypeService.setSelectedOfficeType(null);
  }

  goBack() {
    this.location.back();
  }

  isActive(statusDescription: string) {
    return statusDescription === "Y";
  }

  status(active: boolean) {
    if (active) {
      return 'Active';
    } else {
      return 'Blocked';
    }
  }
}