import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ApiConstants } from 'app/constants/api-constants';
import { CreateAdminProfile } from 'app/module/admin-profile/shared/model/create-admin-profile';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { OfficeTypeDTO } from '../model/office-type-dto';
import { OfficeTypeCreateModel } from '../model/office-type-create-model';
import { OfficeTypeEditModel } from '../model/office-type-edit-model';


@Injectable()
export class OfficeTypeService {

  selectedOfficeType: OfficeTypeDTO;

  constructor(private http: HttpClient) { }


  setSelectedOfficeType(officeType: OfficeTypeDTO) {
    this.selectedOfficeType = officeType;
  }

  getSelectedOfficeType(): OfficeTypeDTO {
    return this.selectedOfficeType;
  }

  getAllOfficeTypess() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.OFFICE_TYPE);
  }

  searchOfficeTypes(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.OFFICE_TYPE + ApiConstants.SEARCH, searchParam);
  }

  searchOfficeTypesForManage() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.OFFICE_TYPE + ApiConstants.MANAGE);
  }

  getAllOfficeTypeForOfficeCreate() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.OFFICE_TYPE);
  }

  getAllOfficeTypesForManage() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.OFFICE_TYPE + ApiConstants.MANAGE);
  }

  getOfficeTypeById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.OFFICE_TYPE + '/' + id);
  }

  create(officeTypeCreateModel: OfficeTypeCreateModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.OFFICE_TYPE, officeTypeCreateModel);
  }

  edit(id: number, officeTypeEditModel: OfficeTypeEditModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.OFFICE_TYPE + ApiConstants.MODIFY + '/' + id, officeTypeEditModel);
  }

}
