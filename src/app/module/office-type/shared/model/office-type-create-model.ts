import { DateUtil } from 'app/module/core/util/date-util.ts';

export class OfficeTypeCreateModel {

  name: string;
  unicodeName: string;
  active: string;

}
