import { ConvergentAdminLoginDto } from "app/module/admin/shared/model/convergent-admin-login-dto";


export class OfficeTypeDTO {
    id: number;
    name: string;
    unicodeName: string;
    ccreatedDate: Date;
    modifiedDate: Date;
    createdBy: string;
    modifiedBy: string;
    active: boolean;
}