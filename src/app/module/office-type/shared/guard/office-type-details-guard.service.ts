import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { OfficeTypeService } from '../service/office-type.service';

@Injectable()
export class OfficeTypeDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.officeTypeService.getSelectedOfficeType() == null) {
      this.router.navigate([AppRoutes.OFFICE_TYPE]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private officeTypeService: OfficeTypeService,
    public router: Router
  ) { }

}
