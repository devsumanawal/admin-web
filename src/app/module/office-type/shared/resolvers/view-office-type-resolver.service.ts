import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { OfficeTypeService } from '../service/office-type.service';

@Injectable()
export class ViewOfficeTypeResolverService implements Resolve<Observable<any>> {
  constructor(
    private officeTypeService: OfficeTypeService,
    private toastr: ToastrService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.officeTypeService.getAllOfficeTypess().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
