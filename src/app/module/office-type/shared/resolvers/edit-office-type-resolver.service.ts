import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { OfficeTypeService } from '../service/office-type.service';
import { AppRoutes } from 'app/constants/app-routes';

@Injectable()
export class EditOfficeTypeResolverService implements Resolve<Observable<any>> {
  constructor(
    private officeTypeService: OfficeTypeService,
    private toastr: ToastrService,
    private ngRoute: Router,
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
 
    return this.officeTypeService.getOfficeTypeById(route.paramMap.get('id')).catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
