import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { OfficeTypeService } from '../service/office-type.service';

@Injectable()
export class ManageOfficeTypeResolverService implements Resolve<any> {
  constructor(
    private officeTypeService: OfficeTypeService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.officeTypeService.getAllOfficeTypesForManage().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
