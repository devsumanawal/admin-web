import { NgModule } from '@angular/core';

import { OfficeTypeComponent } from './office-type.component';
import { OfficeTypeRoutingModule } from './office-type-routing.module';
import { OfficeTypeCreateComponent } from './create/office-type-create.component';
import { ViewOfficeTypeResolverService } from './shared/resolvers/view-office-type-resolver.service';
import { ManageOfficeTypeResolverService } from './shared/resolvers/manage-office-type-resolver.service';
import { EditOfficeTypeResolverService } from './shared/resolvers/edit-office-type-resolver.service';
import { OfficeTypeDetailsComponent } from './details/office-type-details.component';
import { OfficeTypeService } from 'app/module/office-type/shared/service/office-type.service';
import { SharedModule } from 'app/module/shared/shared.module';
import { OfficeTypeManageComponent } from 'app/module/office-type/manage/office-type-manage.component';
import { OfficeTypeEditComponent } from 'app/module/office-type/manage/edit/office-type-edit.component';
import { ActivatedRouteSnapshot } from '@angular/router';
import { OfficeTypeDetailsGuardService } from 'app/module/office-type/shared/guard/office-type-details-guard.service';


@NgModule({
  imports: [
    SharedModule,
    OfficeTypeRoutingModule,
  ],
  declarations: [
    OfficeTypeComponent,
    OfficeTypeCreateComponent,
    OfficeTypeManageComponent,
    OfficeTypeEditComponent,
    OfficeTypeDetailsComponent
  ],
  providers: [
    OfficeTypeService,
    OfficeTypeDetailsGuardService,
    ViewOfficeTypeResolverService,
    ManageOfficeTypeResolverService,
    EditOfficeTypeResolverService,
  ]
})
export class OfficeTypeModule { }
