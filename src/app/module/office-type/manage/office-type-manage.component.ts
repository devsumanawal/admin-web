import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective, PaginationComponent } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { AppRoutes } from 'app/constants/app-routes';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { OfficeTypeDTO } from '../shared/model/office-type-dto';
import { OfficeTypeService } from '../shared/service/office-type.service';

@Component({
  selector: 'app-office-type-manage',
  templateUrl: './office-type-manage.component.html',
  styleUrls: ['./office-type-manage.component.scss']
})
export class OfficeTypeManageComponent implements OnInit {
  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
  officeTypes: Array<OfficeTypeDTO> = [];

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private officeTypeService: OfficeTypeService,
    private route: ActivatedRoute,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.resetSearch();
    this.officeTypeService.setSelectedOfficeType(null);
  }

  resetSearch() {
    this.filterParam = [
      { key: 'name', value: '', label: 'Name', alias: 'Name' },
    ];
    this.searchOfficeTypeOnUserInput();
  }

  searchOfficeTypeOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllOfficeTypeForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllOfficeTypeForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }

    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };

    this.officeTypeService
      .searchOfficeTypes(this.searchParam)
      .subscribe(successResponse => {
        this.officeTypes = successResponse.object;
        this.totalItems = successResponse.totalCount;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
        });
    this.searchButtonpressed = false;
  }

  clickViewDetailButton(officeTypeDto: OfficeTypeDTO) {
    this.officeTypeService.setSelectedOfficeType(officeTypeDto);
    this.router.navigate([AppRoutes.OFFICE_TYPE, AppRoutes.DETAILS]);
  }

  clickedEditOfficeType(officeTypeDto: OfficeTypeDTO) {
    this.officeTypeService.setSelectedOfficeType(officeTypeDto);
    this.router.navigate([AppRoutes.OFFICE_TYPE, AppRoutes.MANAGE, AppRoutes.EDIT, officeTypeDto.id]);
  }


  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  goBack() {
    this.location.back();
  }


  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('Office Type', 'Office Type Setup', 'Manage Office Type', childRoleName);
  }

  isActive(active: string) {
    return active === "Y";
  }

}
