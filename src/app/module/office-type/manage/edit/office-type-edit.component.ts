import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { OfficeTypeService } from 'app/module/office-type/shared/service/office-type.service';
import { OfficeTypeEditModel } from 'app/module/office-type/shared/model/office-type-edit-model';
import { AppRoutes } from 'app/constants/app-routes';
import { OfficeTypeDTO } from '../../shared/model/office-type-dto';

@Component({
  selector: 'app-office-type-edit',
  templateUrl: './office-type-edit.component.html',
  styleUrls: ['./office-type-edit.component.scss']
})
export class OfficeTypeEditComponent implements OnInit {

  editOfficeType: OfficeTypeEditModel = new OfficeTypeEditModel();
  officeTypeId: number;
  @ViewChild('editOfficeTypeForm') editOfficeTypeForm: NgForm;
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private officeTypeService: OfficeTypeService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.OFFICE_TYPE]);
      }

      this.firstAlreadyLoaded = true;
      this.officeTypeId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });
    this.setEditOfficeType();
  }

  ngOnDestroy() {
    this.officeTypeService.setSelectedOfficeType(null);
  }

  setEditOfficeType() {

    const officeTypeDto: OfficeTypeDTO = this.officeTypeService.getSelectedOfficeType();
    this.editOfficeType.name = officeTypeDto.name;
    this.editOfficeType.unicodeName = officeTypeDto.unicodeName;
    this.editOfficeType.active = officeTypeDto.active;
  }

  submitEditOfficeType() {

    this.buttonPressed = true;

    this.officeTypeService
      .edit(this.officeTypeId, this.editOfficeType)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editOfficeTypeForm.form.contains(error.fieldType)) {
                this.editOfficeTypeForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}
