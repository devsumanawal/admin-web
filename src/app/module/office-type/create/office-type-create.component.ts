import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';

import { ToastrService } from 'ngx-toastr';
import { OfficeTypeCreateModel } from 'app/module/office-type/shared/model/office-type-create-model';
import { DateUtil } from 'app/module/core/util/date-util.ts';
import { OfficeTypeService } from '../shared/service/office-type.service';


@Component({
  selector: 'app-office-type-create',
  templateUrl: './office-type-create.component.html'
})
export class OfficeTypeCreateComponent implements OnInit {
  @ViewChild('createOfficeTypeForm') createOfficeTypeForm: any;
  officeType: OfficeTypeCreateModel = new OfficeTypeCreateModel();
  serverErrorMessages: string[] = [];
  buttonPressed = false;

  constructor(
    private location: Location,
    private officeTypeService: OfficeTypeService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
  }

  create(form: NgForm) {
    this.buttonPressed = true;
    this.officeType.active = "Y";
    this.officeTypeService
      .create(this.officeType)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.resetForm();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (form.form.contains(error.fieldType)) {
                form.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }


  back() {
    this.location.back();
  }

  resetForm() {
    this.officeType = new OfficeTypeCreateModel();
    this.createOfficeTypeForm.form.markAsPristine();
    this.createOfficeTypeForm.form.markAsUntouched();
  }


}
