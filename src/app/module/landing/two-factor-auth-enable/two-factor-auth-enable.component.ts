import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LandingService } from '../service/landing.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { BackendLoginResponse } from '../service/backend-login-response';

@Component({
  selector: 'app-two-factor-auth-enable',
  templateUrl: './two-factor-auth-enable.component.html',
  styleUrls: ['./two-factor-auth-enable.component.scss']
})
export class TwoFactorAuthEnableComponent implements OnInit {

  enableAuthenticationForm: FormGroup;
  buttonPressed: boolean = false;
  backendLoginResponse: BackendLoginResponse;
  loginRequest: any;

  constructor(private formBuilder: FormBuilder,
    private loginService: LandingService,
    private toastr: ToastrService,
    private router: Router) {

  }

  ngOnInit() {
    this.build2fEnableForm();
    if ((this.loginService.backendLoginResponse === null || this.loginService.backendLoginResponse === undefined) && (this.loginService.loginRequest === null || this.loginService.loginRequest === undefined)) {
      this.cancel();
    } else {
      this.backendLoginResponse = this.loginService.backendLoginResponse;
      this.loginRequest = this.loginService.loginRequest;
      if ((this.backendLoginResponse.qrFormatedValue != null || this.backendLoginResponse.qrFormatedValue != undefined) && (this.backendLoginResponse.secretKey != null || this.backendLoginResponse.secretKey != undefined)) {
        this.enableAuthenticationForm.get('userName').setValue(this.loginRequest.userName);
        this.enableAuthenticationForm.get('password').setValue(this.loginRequest.password);
        this.enableAuthenticationForm.get('secretKey').setValue(this.backendLoginResponse.secretKey);
      } else {
        this.router.navigate([AppRoutes.LOGIN]);
      }
    }
  }

  build2fEnableForm() {
    this.enableAuthenticationForm = this.formBuilder.group({
      userName: [null],
      password: [null],
      otpCode: [null, [Validators.required]],
      secretKey: [null]
    })
  }

  enableTwoFactorAuth() {
    this.buttonPressed = true;
    this.loginService.enableTwoFactorAuthentication(this.enableAuthenticationForm.value)
      .subscribe((successResponse) => {
        this.buttonPressed = false;
        if (successResponse.success) {
          this.router.navigate([AppRoutes.LOGIN]);
          this.toastr.success('Two Factor Authentication Has been enabled. Please Login again.');
        } else {
          this.toastr.error('Failed to enable Two Factor Authentication');
        }
      },
        (errorResponse) => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.enableAuthenticationForm.contains(error.fieldType)) {
                this.enableAuthenticationForm.controls[error.fieldType].setErrors({ server: error.message });
                this.enableAuthenticationForm.controls[error.fieldType].markAsDirty();
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  cancel() {
    this.loginService.loginRequest = null;
    this.loginService.backendLoginResponse = null;
    this.router.navigate([AppRoutes.LOGIN]);
  }

}
