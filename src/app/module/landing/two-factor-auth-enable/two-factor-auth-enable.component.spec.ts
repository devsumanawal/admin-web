import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoFactorAuthEnableComponent } from './two-factor-auth-enable.component';

describe('TwoFactorAuthEnableComponent', () => {
  let component: TwoFactorAuthEnableComponent;
  let fixture: ComponentFixture<TwoFactorAuthEnableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoFactorAuthEnableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoFactorAuthEnableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
