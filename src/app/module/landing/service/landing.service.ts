import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendLoginResponse } from './backend-login-response';
import { BackendResponse } from 'app/module/core/models/backend-response';


@Injectable()
export class LandingService {

    backendLoginResponse: BackendLoginResponse;
    loginRequest: any;

    constructor(private http: HttpClient) { }

    login(loginForm: any) {
        return this.http.post<BackendLoginResponse>(ApiConstants.ENDPOINT + ApiConstants.ADMIN_AUTHENTICATION, loginForm);
    }

    getAuthetication(twoFactorAuthentication: any) {
        return this.http.post<BackendLoginResponse>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.ADMIN_AUTHENTICATION + ApiConstants.TWO_FACTOR_AUTHENTICATION, twoFactorAuthentication);
    }

    enableTwoFactorAuthentication(enable2f: any) {
        return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.ADMIN_AUTHENTICATION + ApiConstants.ENABLE_OTP_BEFORE_LOGIN, enable2f);
    }
}
