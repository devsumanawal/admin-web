export class BackendLoginResponse {
    statusCode: number;
    changePassword: boolean;
    active: boolean;
    has2f: boolean;
    success: boolean;
    userId: number;
    qrFormatedValue: string;
    secretKey: string;
}
