import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';
import { LandingService } from 'app/module/landing/service/landing.service';
import { ToastrService } from 'ngx-toastr';
import { OfficeService } from 'app/module/core/service/office';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
})
export class LoginComponent {

    loginForm: FormGroup;
    buttonPressed = false;
    capsOn: any;

    // isRecaptchaRequired = false;
    // @ViewChild(RecaptchaComponent)
    // recaptchaComponent: RecaptchaComponent;
    // siteKey = AppConstant.RECAPTCHA_SITE_KEY;

    constructor(
        private formBuilder: FormBuilder,
        private loginService: LandingService,
        public officeService: OfficeService,
        private toastr: ToastrService,
        private router: Router
    ) {
        this.loginForm = this.formBuilder.group({
            office: ['', Validators.required],
            userName: [null, [Validators.required]],
            password: [null, [Validators.required]],
            // recaptcha: ['']
        });
    }

    login() {
        this.buttonPressed = true;
        this.loginService.login(this.loginForm.value).subscribe(response => {
            // this.loginService.loginRequest = this.loginForm.value;
            // if (response.has2f) {
            //     this.buttonPressed = false;
            //     this.loginService.backendLoginResponse = response;
            //     this.router.navigate([AppRoutes.AUTHENTICATE]);
            // } else if (!response.has2f && response.qrFormatedValue != null) {
            //     this.buttonPressed = false;
            //     this.loginService.backendLoginResponse = response;
            //     this.router.navigate([AppRoutes.ENABLE_AUTHENTICATION]);
            // } else {
            if (response.changePassword) {
                this.router.navigate([AppRoutes.CHANGE_PASSWORD]);
                sessionStorage.setItem(SessionStorageConstants.IsFirstLogin, 'TRUE')
            } else {
                sessionStorage.removeItem(SessionStorageConstants.IsFirstLogin);
                this.router.navigate([AppRoutes.DASHBOARD]);
            }
            this.toastr.success('Successfully Logged In');
            // }
        }, errorResponse => {
            this.buttonPressed = false;
            // this.showOrHideRecaptcha(errorResponse);
            if (errorResponse.error.message) {
                this.toastr.error(errorResponse.error.message);
            } else {
                if (errorResponse.error.length) {
                    errorResponse.error.forEach(error => {
                        if (this.loginForm.contains(error.fieldType)) {
                            this.loginForm.controls[error.fieldType].setErrors({ server: error.message });
                            this.loginForm.controls[error.fieldType].markAsDirty();
                        } else {
                            this.toastr.error(error.message);
                        }
                    });
                }
            }
        }).add(() => this.buttonPressed = false);
    }
    // private showOrHideRecaptcha(errorResponse) {
    //     this.isRecaptchaRequired = errorResponse.error.isRecaptchaRequired ? errorResponse.error.isRecaptchaRequired : false;
    //     this.recaptchaComponent.reset();

    //     if (this.isRecaptchaRequired === true) {
    //         this.loginForm.get('recaptcha').setValidators([Validators.required]);
    //         this.loginForm.get('recaptcha').updateValueAndValidity();
    //     } else {
    //         this.loginForm.get('recaptcha').setValidators(null);
    //         this.loginForm.get('recaptcha').updateValueAndValidity();
    //     }
    // }

}
