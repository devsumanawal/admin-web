import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LandingService } from '../service/landing.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { BackendLoginResponse } from '../service/backend-login-response';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';

@Component({
  selector: 'app-two-factor-auth',
  templateUrl: './two-factor-auth.component.html',
  styleUrls: ['./two-factor-auth.component.scss']
})
export class TwoFactorAuthComponent implements OnInit {

  authenticationForm: FormGroup;
  buttonPressed = false;
  backendLoginResponse: BackendLoginResponse;
  loginRequest: any;

  constructor(private formBuilder: FormBuilder,
    private loginService: LandingService,
    private toastr: ToastrService,
    private router: Router) {
    this.buildAuthenticationForm();
  }

  ngOnInit() {
    if ((this.loginService.backendLoginResponse === null || this.loginService.backendLoginResponse === undefined) && (this.loginService.loginRequest === null || this.loginService.loginRequest === undefined)) {
      this.cancel();
    } else {
      this.backendLoginResponse = this.loginService.backendLoginResponse;
      this.loginRequest = this.loginService.loginRequest;
    }
  }

  buildAuthenticationForm() {
    this.authenticationForm = this.formBuilder.group({
      userName: [null, []],
      password: [null, []],
      otpCode: [null, [Validators.required]],
    })
  }


  validateOtp() {
    this.buttonPressed = true;
    this.authenticationForm.get('userName').setValue(this.loginRequest.userName);
    this.authenticationForm.get('password').setValue(this.loginRequest.password);
    this.loginService.getAuthetication(this.authenticationForm.value).subscribe(
      response => {
        if (response.changePassword) {
          this.router.navigate([AppRoutes.CHANGE_PASSWORD]);
          sessionStorage.setItem(SessionStorageConstants.IsFirstLogin, 'TRUE')
        } else {
          sessionStorage.removeItem(SessionStorageConstants.IsFirstLogin);
          this.router.navigate([AppRoutes.DASHBOARD]);
        }
        this.toastr.success('Successfully Logged In');
      },
      errorResponse => {
        this.buttonPressed = false;
        if (errorResponse.error.message) {
          this.toastr.error(errorResponse.error.message);
        } else {
          errorResponse.error.forEach(error => {
            if (this.authenticationForm.contains(error.fieldType)) {
              this.authenticationForm.controls[error.fieldType].setErrors({ server: error.message });
              this.authenticationForm.controls[error.fieldType].markAsDirty();
            } else {
              this.toastr.error(error.message);
            }
          });
        }
      }
    );
  }

  cancel() {
    this.loginService.loginRequest = null;
    this.loginService.backendLoginResponse = null;
    this.router.navigate([AppRoutes.LOGIN]);
  }

}
