import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/module/shared/shared.module';
import { Routes } from '@angular/router/src/config';
import { LandingComponent } from 'app/module/landing/landing.component';
import { RouterModule } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { LoginComponent } from 'app/module/landing/login/login.component';
import { TwoFactorAuthComponent } from './two-factor-auth/two-factor-auth.component';
import { TwoFactorAuthEnableComponent } from './two-factor-auth-enable/two-factor-auth-enable.component';

const landingRoutes: Routes = [{
  path: '', component: LandingComponent, pathMatch: 'full'
},
{
  path: AppRoutes.LOGIN,
  component: LoginComponent,
  pathMatch: 'full'
},
{
  path: 'authenticate',
  component: TwoFactorAuthComponent,
  data: {
    title: 'Authentication'
  }
},
{
  path: 'enableAuthentication',
  component: TwoFactorAuthEnableComponent,
  data: {
    title: 'Enable Authentication'
  }
},
];

@NgModule({
  imports: [
    RouterModule.forChild(landingRoutes)
  ],
  declarations: []
})
export class LandingRoutingModule { }
