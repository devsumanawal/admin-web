import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing.component';
import { LandingRoutingModule } from './landing-routing.module';
import { LoginComponent } from './login/login.component';
import { SharedModule } from 'app/module/shared/shared.module';
import { LandingService } from './service/landing.service';
import { CapsLockNotifyDirective } from './service/caps-lock-notify.directive';
import { TwoFactorAuthComponent } from './two-factor-auth/two-factor-auth.component';
import { TwoFactorAuthEnableComponent } from './two-factor-auth-enable/two-factor-auth-enable.component';

@NgModule({
  imports: [
    CommonModule,
    LandingRoutingModule,
    SharedModule
  ],
  declarations: [LandingComponent, LoginComponent, CapsLockNotifyDirective, TwoFactorAuthComponent, TwoFactorAuthEnableComponent],
  providers: [LandingService]
})
export class LandingModule { }
