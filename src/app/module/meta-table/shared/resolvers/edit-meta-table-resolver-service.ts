import { Injectable } from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { MetaTableService } from 'app/module/meta-table/shared/meta-table.service';
import { ToastrService } from 'ngx-toastr';
import {AppRoutes} from '../../../../constants/app-routes';

@Injectable()
export class EditMetaTableResolverService implements Resolve<Observable<any>> {
  constructor(
    private metaTableService: MetaTableService,
    private toastr: ToastrService,
    private ngroute: Router,
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.metaTableService.getMetaTableById(route.paramMap.get('id')).catch((error) => {
      this.ngroute.navigate([AppRoutes.META_TABLE]);
      return Observable.empty();
    });
  }
}
