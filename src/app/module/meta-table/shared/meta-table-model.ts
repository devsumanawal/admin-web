export class MetaTableModel {
  id: number;
  isEnabled: boolean;
  fieldName: string;
  fieldValue: string;
  lableName: string;
}
