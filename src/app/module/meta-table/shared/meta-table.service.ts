import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MetaTableModel } from 'app/module/meta-table/shared/meta-table-model';
import { Observable } from 'rxjs/Observable';
import { ApiConstants } from 'app/constants/api-constants';
import { ModifyMetaTableModel } from 'app/module/meta-table/shared/modify-meta-table-model';
import { BackendResponse } from 'app/module/core/models/backend-response';

@Injectable()
export class MetaTableService {
  constructor(private http: HttpClient) {
  }

  getAllMetaTable(): Observable<MetaTableModel[]> {
    return this.http.get<MetaTableModel[]>(
      ApiConstants.ENDPOINT + ApiConstants.META_TABLE
    );
  }

  getMetaTableById(metaTableId) {
    return this.http.get<MetaTableModel>(
      ApiConstants.ENDPOINT + ApiConstants.META_TABLE + '/' + metaTableId
    );
  }

  editMetaTable(modifyMetaTableModel: ModifyMetaTableModel, metaTableId) {
    return this.http.post<BackendResponse>(
      ApiConstants.ENDPOINT + ApiConstants.META_TABLE + '/' + metaTableId, modifyMetaTableModel
    );
  }

}
