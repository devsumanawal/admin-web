import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/module/shared/shared.module';
import { MetaTableRoutingModule } from 'app/module/meta-table/meta-table-routing.module';
import { MetaTableManageComponent } from 'app/module/meta-table/manage/meta-table-manage.component';
import { MetaTableEditComponent } from 'app/module/meta-table/manage/edit/meta-table-edit.component';
import { MetaTableService } from 'app/module/meta-table/shared/meta-table.service';
import { EditMetaTableResolverService } from 'app/module/meta-table/shared/resolvers/edit-meta-table-resolver-service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MetaTableRoutingModule
  ],
  declarations: [
    MetaTableManageComponent,
    MetaTableEditComponent,
  ],
  providers: [
    MetaTableService,
    EditMetaTableResolverService
  ]
})
export class MetaTableModule { }
