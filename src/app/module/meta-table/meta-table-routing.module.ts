import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { MetaTableManageComponent } from 'app/module/meta-table/manage/meta-table-manage.component';
import { MetaTableEditComponent } from 'app/module/meta-table/manage/edit/meta-table-edit.component';
import { EditMetaTableResolverService } from 'app/module/meta-table/shared/resolvers/edit-meta-table-resolver-service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'Meta Table'
    },
    children: [
      {
        path: '',
        component: MetaTableManageComponent,
        pathMatch: 'full'
      },
      {
        path: AppRoutes.EDIT_ID,
        component: MetaTableEditComponent,
        pathMatch: 'full',
        resolve:
        {
          metaTableModel: EditMetaTableResolverService,
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class MetaTableRoutingModule { }
