import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { MetaTableModel } from 'app/module/meta-table/shared/meta-table-model';
import { MetaTableService } from 'app/module/meta-table/shared/meta-table.service';

@Component({
  selector: 'app-meta-table-manage',
  templateUrl: './meta-table-manage.component.html',
  styleUrls: ['./meta-table-manage.component.scss']
})
export class MetaTableManageComponent implements OnInit {

  metaTables: Array<MetaTableModel> = [];
  searchMetaTable: string;

  constructor(
    private metaTableService: MetaTableService,
    private toastr: ToastrService,
    private location: Location
  ) { }

  ngOnInit() {
    this.metaTableService.getAllMetaTable()
      .subscribe(metaTables => {
        this.setMetaTables(metaTables);
      },
        error => {
          this.toastr.error(error.error.message);
        })
  }

  setMetaTables(metaTables: Array<MetaTableModel>) {
    this.metaTables = metaTables;
  }

  getSearchedFilterMeTable() {
    if (this.searchMetaTable) {
      return this.metaTables.filter(metaTableValue => {
        return metaTableValue.lableName.toLocaleLowerCase().indexOf(this.searchMetaTable.toLocaleLowerCase()) > -1
      });
    } else {
      return this.metaTables;
    }
  }

  goBack() {
    this.location.back();
  }
}
