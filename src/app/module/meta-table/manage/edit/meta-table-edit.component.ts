import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {ModifyMetaTableModel} from 'app/module/meta-table/shared/modify-meta-table-model';
import {MetaTableService} from 'app/module/meta-table/shared/meta-table.service';
import {MetaTableModel} from 'app/module/meta-table/shared/meta-table-model';
import {AppRoutes} from '../../../../constants/app-routes';

@Component({
  selector: 'app-meta-table-edit',
  templateUrl: './meta-table-edit.component.html',
  styleUrls: ['./meta-table-edit.component.scss']
})
export class MetaTableEditComponent implements OnInit {
  metaTableId: number;
  modifyMetaTableModel: ModifyMetaTableModel = new ModifyMetaTableModel();
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private ngroute: Router,
    private activatedRoute: ActivatedRoute,
    private metaTableService: MetaTableService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.getMetatableForEdit();
    this.route.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngroute.navigate([AppRoutes.META_TABLE]);
      }

      this.firstAlreadyLoaded = true;

      this.metaTableId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    });
  }

  getMetatableForEdit() {
    this.mapMetaTableModelToModify(this.route.snapshot.data.metaTableModel);
  }

  mapMetaTableModelToModify(metaTablesModel: MetaTableModel) {
    this.modifyMetaTableModel.fieldValue = metaTablesModel.fieldValue;
    this.modifyMetaTableModel.lableName = metaTablesModel.lableName;
  }

  edit(form: NgForm) {
    this.buttonPressed = true;
    this.metaTableService
      .editMetaTable(this.modifyMetaTableModel, this.metaTableId)
      .subscribe(response => {
          this.toastr.success(response.message);
          this.buttonPressed = false;
          this.location.back();
        },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (form.form.contains(error.fieldType)) {
                form.controls[error.fieldType].setErrors({server: true});
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}
