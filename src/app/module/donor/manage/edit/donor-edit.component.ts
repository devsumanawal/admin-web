import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DonorService } from 'app/module/donor/shared/service/donor.service';
import { DonorEditModel } from 'app/module/donor/shared/model/donor-edit-model';
import { AppRoutes } from 'app/constants/app-routes';
import { DonorDTO } from '../../shared/model/donor-dto';

@Component({
  selector: 'app-donor-edit',
  templateUrl: './donor-edit.component.html',
  styleUrls: ['./donor-edit.component.scss']
})
export class DonorEditComponent implements OnInit {

  editDonor: DonorEditModel = new DonorEditModel();
  donorId: number;
  @ViewChild('editDonorForm') editDonorForm: NgForm;
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;
  status: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private donorService: DonorService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.DONOR]);
      }

      this.firstAlreadyLoaded = true;
      this.donorId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditDonor();
  }

  ngOnDestroy() {
    this.donorService.setSelectedDonor(null);
  }

  setEditDonor() {
    const donorDto: DonorDTO = this.donorService.getSelectedDonor();
    this.editDonor.name = donorDto.name;
    this.editDonor.unicodeName = donorDto.unicodeName;
    this.editDonor.code = donorDto.code;
    this.editDonor.type = donorDto.type;
    this.editDonor.status = donorDto.status;
  }

  submitEditDonor() {

    this.buttonPressed = true;
    console.log('Edit Donor::: ', this.editDonor);

    this.donorService
      .edit(this.donorId, this.editDonor)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editDonorForm.form.contains(error.fieldType)) {
                this.editDonorForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}
