import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective, PaginationComponent } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { AppRoutes } from 'app/constants/app-routes';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { DonorService } from '../shared/service/donor.service';
import { DonorDTO } from '../shared/model/donor-dto';

@Component({
  selector: 'app-donor-manage',
  templateUrl: './donor-manage.component.html',
  styleUrls: ['./donor-manage.component.scss']
})
export class DonorManageComponent implements OnInit {
  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
  donors: Array<DonorDTO> = [];

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private donorService: DonorService,
    private route: ActivatedRoute,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.donorService.setSelectedDonor(null);
    this.getAllDonorForSearch(null);
  }

  searchDonorOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllDonorForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllDonorForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }

    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };

    this.donorService
      .searchDonors(this.searchParam)
      .subscribe(successResponse => {
        this.donors = successResponse;
        this.totalItems = successResponse.totalCount;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
        });
    this.searchButtonpressed = false;
  }

  clickViewDetailButton(donorDto: DonorDTO) {
    this.donorService.setSelectedDonor(donorDto);
    this.router.navigate([AppRoutes.DONOR, AppRoutes.DETAILS]);
  }

  clickedEditDonor(donorDto: DonorDTO) {
    this.donorService.setSelectedDonor(donorDto);
    this.router.navigate([AppRoutes.DONOR, AppRoutes.MANAGE, AppRoutes.EDIT, donorDto.id]);
  }


  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  goBack() {
    this.location.back();
  }


  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('Donor', 'Donor Setup', 'Manage Donor', childRoleName);
  }


  isActive(active: string) {
    return active === "Y";
  }

}
