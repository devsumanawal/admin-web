import { ConvergentAdminLoginDto } from "app/module/admin/shared/model/convergent-admin-login-dto";


export class DonorDTO {
    id: number;
    name: string;
    unicodeName: string;
    code: string;
    type: string;
    status: boolean;
    createdDate: Date;
    modifiedDate: Date;
    createdBy: string;
    modifiedBy: string;
}