import { DateUtil } from 'app/module/core/util/date-util.ts';

export class DonorCreateModel {

  name: string;
  unicodeName: string;
  code: string;
  type: string;
  status: string;

}
