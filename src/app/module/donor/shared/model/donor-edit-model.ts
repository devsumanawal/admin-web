export class DonorEditModel {

  name: string;
  unicodeName: string;
  code: string;
  type: string;
  status: boolean;
  
}
