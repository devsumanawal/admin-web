import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { DonorCreateModel } from '../model/donor-create-model';
import { DonorEditModel } from '../model/donor-edit-model';
import { DonorDTO } from '../model/donor-dto';

@Injectable()
export class DonorService {

  selectedDonor: DonorDTO;

  constructor(private http: HttpClient) { }


  setSelectedDonor(donor: DonorDTO) {
    this.selectedDonor = donor;
  }

  getSelectedDonor(): DonorDTO {
    return this.selectedDonor;
  }

  getAllDonors() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.DONOR);
  }

  searchDonors(searchParam) {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.DONOR);
  }

  searchDonorsForManage() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.DONOR + ApiConstants.MANAGE);
  }

  getAllDonorForCreate() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.DONOR);
  }

  getAllDonorsForManage() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.DONOR + ApiConstants.MANAGE);
  }

  getDonorById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.DONOR + '/' + id);
  }

  create(donorCreateModel: DonorCreateModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.DONOR, donorCreateModel);
  }

  edit(id: number, donorEditModel: DonorEditModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.DONOR + ApiConstants.MODIFY + '/' + id, donorEditModel);
  }

}
