import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { DonorService } from '../service/donor.service';

@Injectable()
export class ViewDonorResolverService implements Resolve<Observable<any>> {
  constructor(
    private donorService: DonorService,
    private toastr: ToastrService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.donorService.getAllDonors().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
