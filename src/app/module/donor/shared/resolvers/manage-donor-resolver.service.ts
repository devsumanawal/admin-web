import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { DonorService } from '../service/donor.service';

@Injectable()
export class ManageDonorResolverService implements Resolve<any> {
  constructor(
    private donorService: DonorService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.donorService.getAllDonorsForManage().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
