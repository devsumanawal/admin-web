import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { DonorService } from '../service/donor.service';

@Injectable()
export class DonorDetailsGuardService implements CanActivate {

  constructor(
    private donorService: DonorService,
    public router: Router
  ) { }

  canActivate() {
    if (this.donorService.getSelectedDonor() == null) {
      this.router.navigate([AppRoutes.DONOR]);
      return false;
    } else {
      return true;
    }
  }



}
