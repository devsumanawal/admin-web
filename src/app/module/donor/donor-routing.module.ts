import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { DonorEditComponent } from './manage/edit/donor-edit.component';
import { EditDonorResolverService } from './shared/resolvers/edit-donor-resolver.service';
import { DonorDetailsGuardService } from './shared/guard/donor-details-guard.service';
import { DonorManageComponent } from './manage/donor-manage.component';
import { DonorCreateComponent } from './create/donor-create.component';
import { DonorDetailsComponent } from './details/donor-details.component';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'Donor'
    },
    children: [
      {
        path: '',
        component: DonorManageComponent,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.CREATE,
        component: DonorCreateComponent,
        pathMatch: 'full',
       
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: DonorManageComponent,
            pathMatch: 'full',
           
          },
          {
            path: AppRoutes.EDIT_ID,
            component: DonorEditComponent,
            pathMatch: 'full',
            resolve: {
              donorDto: EditDonorResolverService,
            }
          }
        ]
      },
      {
        path: AppRoutes.DETAILS,
        component: DonorDetailsComponent,
        pathMatch: 'full',
        canActivate: [
          DonorDetailsGuardService
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class DonorRoutingModule { }
