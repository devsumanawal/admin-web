import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { DonorService } from '../shared/service/donor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { DonorDTO } from '../shared/model/donor-dto';


@Component({
  selector: 'app-donor-details',
  templateUrl: './donor-details.component.html',
  styleUrls: ['./donor-details.component.scss']
})
export class DonorDetailsComponent implements OnInit {

  donor: DonorDTO;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ngRoute: Router,
    private donorService: DonorService,
    private location: Location
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.DONOR]);
      }

      this.firstAlreadyLoaded = true;

    });

    this.donor = this.donorService.getSelectedDonor();
  }

  ngOnDestroy() {
    this.donorService.setSelectedDonor(null);
  }

  goBack() {
    this.location.back();
  }

  isActive(statusDescription: string) {
    return statusDescription === "Y";
  }
}