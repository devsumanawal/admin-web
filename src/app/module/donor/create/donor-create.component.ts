import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';

import { ToastrService } from 'ngx-toastr';
import { DonorCreateModel } from '../shared/model/donor-create-model';
import { DonorService } from '../shared/service/donor.service';


@Component({
  selector: 'app-donor-create',
  templateUrl: './donor-create.component.html'
})
export class DonorCreateComponent implements OnInit {
  @ViewChild('createDonorForm')
  createDonorForm: any;
  donor: DonorCreateModel = new DonorCreateModel();
  serverErrorMessages: string[] = [];
  buttonPressed = false;

  constructor(
    private location: Location,
    private donorService: DonorService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
  }

  create(form: NgForm) {
    this.buttonPressed = true;
    this.donor.status = "Y";
    this.donorService
      .create(this.donor)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.resetForm();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (form.form.contains(error.fieldType)) {
                form.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }


  back() {
    this.location.back();
  }

  resetForm() {
    this.donor = new DonorCreateModel();
    this.createDonorForm.form.markAsPristine();
    this.createDonorForm.form.markAsUntouched();
  }


}
