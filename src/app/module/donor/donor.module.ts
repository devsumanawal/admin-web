import { NgModule } from '@angular/core';

import { DonorRoutingModule } from './donor-routing.module';
import { DonorCreateComponent } from './create/donor-create.component';
import { ViewDonorResolverService } from './shared/resolvers/view-donor-resolver.service';
import { ManageDonorResolverService } from './shared/resolvers/manage-donor-resolver.service';
import { EditDonorResolverService } from './shared/resolvers/edit-donor-resolver.service';
import { DonorDetailsComponent } from './details/donor-details.component';
import { SharedModule } from '../shared/shared.module';
import { DonorManageComponent } from './manage/donor-manage.component';
import { DonorEditComponent } from './manage/edit/donor-edit.component';
import { DonorService } from './shared/service/donor.service';
import { DonorDetailsGuardService } from './shared/guard/donor-details-guard.service';


@NgModule({
  imports: [
    SharedModule,
    DonorRoutingModule,
  ],
  declarations: [
    DonorCreateComponent,
    DonorManageComponent,
    DonorEditComponent,
    DonorDetailsComponent
  ],
  providers: [
    DonorService,
    DonorDetailsGuardService,
    ViewDonorResolverService,
    ManageDonorResolverService,
    EditDonorResolverService,
  ]
})
export class DonorModule { }
