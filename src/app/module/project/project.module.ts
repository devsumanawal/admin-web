import { NgModule } from '@angular/core';


import { SharedModule } from 'app/module/shared/shared.module';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectEditComponent } from './manage/edit/project-edit.component';
import { ProjectCreateComponent } from './create/project-create.component';
import { ProjectManageComponent } from './manage/project-manage.component';
import { ProjectService } from './shared/service/project.service';
import { ProjectDetailsGuardService } from './shared/guard/project-details-guard.service';
import { ViewProjectResolverService } from './shared/resolvers/view-project-resolver.service';
import { ManageProjectResolverService } from './shared/resolvers/manage-project-resolver.service';
import { EditProjectResolverService } from './shared/resolvers/edit-project-resolver.service';


@NgModule({
  imports: [
    SharedModule,
    ProjectRoutingModule
  ],
  declarations: [
    ProjectCreateComponent,
    ProjectEditComponent,
    ProjectManageComponent
  ],
  providers: [
    ProjectService,
    ProjectDetailsGuardService,
    ViewProjectResolverService,
    ManageProjectResolverService,
    EditProjectResolverService
  ]
})
export class ProjectModule { }
