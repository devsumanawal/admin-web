import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToastrService } from 'ngx-toastr';
import { ProjectCreateModel } from 'app/module/project/shared/model/project-create-model';
import { ProjectService } from '../shared/service/project.service';
import { MinistryService } from 'app/module/core/service/office/ministry.service';
import { LevelDto } from '../shared/model/level-dto';
import { OfficeService } from 'app/module/core/service/office';
import { FiscalYearDTO } from 'app/module/fiscal-year/shared/model/fiscal-year-dto';


@Component({
    selector: 'app-project-create',
    templateUrl: './project-create.component.html'
})
export class ProjectCreateComponent implements OnInit {

    @ViewChild('createProjectForm')
    createProjectForm: any;

    project: ProjectCreateModel = new ProjectCreateModel();
    serverErrorMessages: string[] = [];
    buttonPressed = false;
    profiles = [];
    // levels: Array<LevelDto> = [];
    // level: LevelDto = new LevelDto();
    // fiscalYears: FiscalYearDTO[] = [];

    constructor(
        private location: Location,
        private projectService: ProjectService,
        private ministryService: MinistryService,
        // private officeService: OfficeService,
        private toastr: ToastrService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.addDonor();
    }


    addDonor() {
        this.project.donorList.push({
            donorId: null
        });
    }

    create(form: NgForm) {
        this.buttonPressed = true;
        this.project.enabled = "Y";
        console.log("Project Data::: ", this.project);
        this.projectService
            .create(this.project)
            .subscribe(response => {
                this.toastr.success(response.message);
                this.resetForm();
                this.buttonPressed = false;
            },
                errorResponse => {
                    this.buttonPressed = false;
                    if (errorResponse.error.message) {
                        this.toastr.error(errorResponse.error.message);
                    } else {
                        errorResponse.error.forEach(error => {
                            if (form.form.contains(error.fieldType)) {
                                form.controls[error.fieldType].setErrors({ server: true });
                                this.serverErrorMessages[error.fieldType] = error.message;
                            } else {
                                this.toastr.error(error.message);
                            }
                        });
                    }
                });
    }

    back() {
        this.location.back();
    }

    resetForm() {
        this.project = new ProjectCreateModel();
        this.createProjectForm.form.markAsPristine();
        this.createProjectForm.form.markAsUntouched();
    }

    removeDonor(index: number) {
        if (index === 0 && this.project.donorList.length === 1) {
            return;
        }
        this.project.donorList.splice(index, 1);
    }

}
