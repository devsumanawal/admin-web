import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { ProjectCreateComponent } from 'app/module/project/create/project-create.component';
import { CommonModule } from '@angular/common';
import { ProjectManageComponent } from 'app/module/project/manage/project-manage.component';
import { ProjectEditComponent } from './manage/edit/project-edit.component';
import { EditProjectResolverService } from './shared/resolvers/edit-project-resolver.service';
import { ViewProjectResolverService } from './shared/resolvers/view-project-resolver.service';
import { ManageProjectResolverService } from './shared/resolvers/manage-project-resolver.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'projectData'
    },
    children: [
      {
        path: '',
        component: ProjectCreateComponent,
        pathMatch: 'full',
        resolve: {
          userGroups: ViewProjectResolverService
        }
      },
      {
        path: AppRoutes.CREATE,
        component: ProjectCreateComponent,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: ProjectManageComponent,
            pathMatch: 'full',
            resolve: {
              userGroups: ManageProjectResolverService
            }
          },
          {
            path: AppRoutes.EDIT_ID,
            component: ProjectEditComponent,
            pathMatch: 'full',
            resolve: {
              userGroupDto: EditProjectResolverService,
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
