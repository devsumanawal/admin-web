import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { ProjectService } from '../service/project.service';

@Injectable()
export class ManageProjectResolverService implements Resolve<any> {
  constructor(
    private officeService: ProjectService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.officeService.getAllProject().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
