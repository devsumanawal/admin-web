import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { ProjectDTO } from '../model/project-dto';
import { ProjectCreateModel } from '../model/project-create-model';
import { ProjectEditModel } from '../model/project-edit-model';


@Injectable()
export class ProjectService {

  selectedProject: ProjectDTO;;

  constructor(private http: HttpClient) { }

  getAllProject() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.PROJECT_DATA);
  }

  searchAllProjects(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.PROJECT_DATA + ApiConstants.SEARCH, searchParam);
  }

  create(projectCreateModel: ProjectCreateModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.PROJECT_DATA, projectCreateModel);
  }

  edit(id, projectEditModel: ProjectEditModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.PROJECT_DATA + ApiConstants.MODIFY + '/' + id, projectEditModel);
  }

  getProjectById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.PROJECT_DATA + '/' + id);
  }

  setSelectedProject(selectedProject: ProjectDTO) {
    this.selectedProject = selectedProject;
  }

  getSelectedProject(): ProjectDTO {
    return this.selectedProject;
  }

  searchProjectForManage(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.PROJECT_DATA + ApiConstants.MANAGE + ApiConstants.SEARCH, searchParam);
  }
}
