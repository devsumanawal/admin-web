import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { ProjectService } from '../service/project.service';

@Injectable()
export class ProjectDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.officeService.getSelectedProject() == null) {
      this.router.navigate([AppRoutes.DEPARTMENT]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private officeService: ProjectService,
    public router: Router
  ) { }

}
