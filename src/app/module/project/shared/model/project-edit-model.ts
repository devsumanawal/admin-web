export class ProjectEditModel {
  id: number;
  name: string;
  unicodeName: string;
  code: string;
  enabled: string;
  ministryId: number;
  donorList: {
    donorId: number
  }[] = [];
}
