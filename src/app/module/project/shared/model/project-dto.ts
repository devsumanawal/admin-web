export class ProjectDTO {
    id: number;
    name: string;
    unicodeName: string;
    code: string;
    enabled: string;
    ministryDto: MinistryDto;
    donorDtoList: DonorDto[];

}

export class MinistryDto {
    id: number;
    name: string;
    unicodeName: string;
    code: string;
    status: string;
}

export class DonorDto {
    id: number;
    name: string;
    unicodeName: string;
    code: string;
    status: string;
}
