import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective, PaginationComponent } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { AppRoutes } from 'app/constants/app-routes';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { ProjectDTO } from '../shared/model/project-dto';
import { ProjectService } from '../shared/service/project.service';
import { OfficeService } from 'app/module/core/service/office';
import { LevelDto } from '../shared/model/level-dto';
import { FiscalYearDTO } from 'app/module/fiscal-year/shared/model/fiscal-year-dto';

@Component({
  selector: 'app-project-manage',
  templateUrl: './project-manage.component.html'
})
export class ProjectManageComponent implements OnInit {
  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
  projects: Array<ProjectDTO> = [];

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();


  constructor(
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private projectService: ProjectService,
    private route: ActivatedRoute,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.resetSearch();
    this.projectService.setSelectedProject(null);
  }

  

  resetSearch() {
    this.filterParam = [
      { key: 'name', value: '', label: 'Name', alias: 'Name' },
      { key: 'code', value: '', label: 'Code', alias: 'Code' },
    ];
    this.searchProjectOnUserInput();
  }

  searchProjectOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllProjectForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllProjectForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }

    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };

    this.projectService
      .searchAllProjects(this.searchParam)
      .subscribe(successResponse => {
        this.projects = successResponse.object;
        this.totalItems = successResponse.totalCount;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
        });

    this.searchButtonpressed = false;
  }

  clickViewDetailButton(projectDto: ProjectDTO) {
    this.projectService.setSelectedProject(projectDto);
    this.router.navigate([AppRoutes.PROJECT_DATA, AppRoutes.DETAILS]);
  }

  clickedEditProject(projectDto: ProjectDTO) {
    this.projectService.setSelectedProject(projectDto);
    this.router.navigate([AppRoutes.PROJECT_DATA, AppRoutes.MANAGE, AppRoutes.EDIT, projectDto.id]);
  }


  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  goBack() {
    this.location.back();
  }


  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('Project Data', 'Project Data Setup', 'Manage Project Data', childRoleName);
  }

  isActive(active: string) {
    return active === "Y";
  }
}
