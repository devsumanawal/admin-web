import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'app/constants/app-routes';
import { ProjectDTO, DonorDto } from '../../shared/model/project-dto';
import { ProjectEditModel } from '../../shared/model/project-edit-model';
import { ProjectService } from '../../shared/service/project.service';
import { MinistryService } from 'app/module/core/service/office/ministry.service';
import { LevelDto } from '../../shared/model/level-dto';
import { FiscalYearDTO } from 'app/module/fiscal-year/shared/model/fiscal-year-dto';
import { OfficeService } from 'app/module/core/service/office';
import { DonorDTO } from 'app/module/donor/shared/model/donor-dto';
import { of } from 'rxjs/observable/of';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html'
})

export class ProjectEditComponent implements OnInit {

  editProject: ProjectEditModel = new ProjectEditModel();
  projectId: number;


  @ViewChild('editProjectForm')
  editProjectForm: NgForm;

  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;
  levels: Array<LevelDto> = [];
  fiscalYears: FiscalYearDTO[] = [];

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private projectService: ProjectService,
    private ministryService: MinistryService,
    // private officeService: OfficeService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.OFFICE]);
      }

      this.firstAlreadyLoaded = true;
      this.projectId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditProject();
  }



  ngOnDestroy() {
    this.projectService.setSelectedProject(null);
  }

  setEditProject() {
    const projectDTO: ProjectDTO = this.projectService.getSelectedProject();
    this.editProject.name = projectDTO.name;
    this.editProject.unicodeName = projectDTO.unicodeName;
    this.editProject.code = projectDTO.code;
    this.editProject.enabled = projectDTO.enabled;
    this.editProject.ministryId = projectDTO.ministryDto.id;
    this.setSelectedDonors(projectDTO);
  }



  setSelectedDonors(projectDTO: ProjectDTO) {
    for (var donor of projectDTO.donorDtoList) {
      this.editProject.donorList.push({
        donorId: donor.id
      });
    }
    console.log('Donor List::: ', this.editProject.donorList);
  }

  submitEditProject() {
    this.buttonPressed = true;
    console.log("Project Data For Edit::: ", this.editProject);
    this.projectService
      .edit(this.projectId, this.editProject)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editProjectForm.form.contains(error.fieldType)) {
                this.editProjectForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}