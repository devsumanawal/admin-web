import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToastrService } from 'ngx-toastr';
import { MinistryCreateModel } from 'app/module/ministry/shared/model/ministry-create-model';
import { MinistryService } from '../shared/service/ministry.service';


@Component({
    selector: 'app-ministry-create',
    templateUrl: './ministry-create.component.html'
})
export class MinistryCreateComponent implements OnInit {

    @ViewChild('createMinistryForm')
    createMinistryForm: any;

    ministry: MinistryCreateModel = new MinistryCreateModel();
    serverErrorMessages: string[] = [];
    buttonPressed = false;
    profiles = [];

    constructor(
        private location: Location,
        private ministryService: MinistryService,
        private toastr: ToastrService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    create(form: NgForm) {
        this.buttonPressed = true;
        this.ministry.status="Y";
        this.ministryService
            .create(this.ministry)
            .subscribe(response => {
                this.toastr.success(response.message);
                this.resetForm();
                this.buttonPressed = false;
            },
                errorResponse => {
                    this.buttonPressed = false;
                    if (errorResponse.error.message) {
                        this.toastr.error(errorResponse.error.message);
                    } else {
                        errorResponse.error.forEach(error => {
                            if (form.form.contains(error.fieldType)) {
                                form.controls[error.fieldType].setErrors({ server: true });
                                this.serverErrorMessages[error.fieldType] = error.message;
                            } else {
                                this.toastr.error(error.message);
                            }
                        });
                    }
                });
    }


    back() {
        this.location.back();
    }

    resetForm() {
        this.ministry = new MinistryCreateModel();
        this.createMinistryForm.form.markAsPristine();
        this.createMinistryForm.form.markAsUntouched();
    }

}
