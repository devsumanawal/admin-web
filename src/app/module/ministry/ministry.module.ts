import { NgModule } from '@angular/core';


import { SharedModule } from 'app/module/shared/shared.module';
import { MinistryEditComponent } from './manage/edit/ministry-edit.component';
import { MinistryCreateComponent } from './create/ministry-create.component';
import { MinistryManageComponent } from './manage/ministry-manage.component';
import { MinistryService } from './shared/service/ministry.service';
import { ViewMinistryResolverService } from './shared/resolvers/view-ministry-resolver.service';
import { ManageMinistryResolverService } from './shared/resolvers/manage-ministry-resolver.service';
import { EditMinistryResolverService } from './shared/resolvers/edit-ministry-resolver.service';
import { MinistryRoutingModule } from './ministry-routing.module';
import { MinistryDetailsGuardService } from './shared/guard/ministry-details-guard.service';


@NgModule({
  imports: [
    SharedModule,
    MinistryRoutingModule
  ],
  declarations: [
    MinistryCreateComponent,
    MinistryEditComponent,
    MinistryManageComponent
  ],
  providers: [
    MinistryService,
    MinistryDetailsGuardService,
    ViewMinistryResolverService,
    ManageMinistryResolverService,
    EditMinistryResolverService
  ]
})
export class MinistryModule { }
