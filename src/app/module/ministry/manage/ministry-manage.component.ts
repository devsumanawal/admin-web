import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective, PaginationComponent } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { AppRoutes } from 'app/constants/app-routes';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { MinistryDTO } from '../shared/model/ministry-dto';
import { MinistryService } from '../shared/service/ministry.service';

@Component({
  selector: 'app-ministry-manage',
  templateUrl: './ministry-manage.component.html'
})
export class MinistryManageComponent implements OnInit {
  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
  ministrys: Array<MinistryDTO> = [];

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private ministryService: MinistryService,
    private route: ActivatedRoute,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.resetSearch();
    this.ministryService.setSelectedMinistry(null);
  }

  resetSearch() {
    this.filterParam = [
      { key: 'name', value: '', label: 'Name', alias: 'Name' },
      { key: 'code', value: '', label: 'Code', alias: 'Code' },
    ];
    this.searchMinistryOnUserInput();
  }

  searchMinistryOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllMinistryForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllMinistryForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }

    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };

    this.ministryService
      .searchAllMinistrys(this.searchParam)
      .subscribe(successResponse => {
        this.ministrys = successResponse.object;
        this.totalItems = successResponse.totalCount;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
        });
    this.searchButtonpressed = false;
  }

  clickViewDetailButton(ministryDto: MinistryDTO) {
    this.ministryService.setSelectedMinistry(ministryDto);
    this.router.navigate([AppRoutes.MINISTRY, AppRoutes.DETAILS]);
  }

  clickedEditMinistry(ministryDto: MinistryDTO) {
    this.ministryService.setSelectedMinistry(ministryDto);
    this.router.navigate([AppRoutes.MINISTRY, AppRoutes.MANAGE, AppRoutes.EDIT, ministryDto.id]);
  }


  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  goBack() {
    this.location.back();
  }


  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('Ministry', 'Ministry Setup', 'Manage Ministry', childRoleName);
  }
  
  isActive(active: string) {
    return active === "Y";
  }
}
