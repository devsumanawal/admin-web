import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'app/constants/app-routes';
import { MinistryDTO } from '../../shared/model/ministry-dto';
import { MinistryEditModel } from '../../shared/model/ministry-edit-model';
import { MinistryService } from '../../shared/service/ministry.service';

@Component({
  selector: 'app-ministry-edit',
  templateUrl: './ministry-edit.component.html'
})

export class MinistryEditComponent implements OnInit {

  editMinistry: MinistryEditModel = new MinistryEditModel();
  ministryId: number;


  @ViewChild('editMinistryForm')
  editMinistryForm: NgForm;

  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private ministryService: MinistryService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.MINISTRY]);
      }

      this.firstAlreadyLoaded = true;
      this.ministryId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditMinistry();
  }

  ngOnDestroy() {
    this.ministryService.setSelectedMinistry(null);
  }

  setEditMinistry() {
    const ministryDTO: MinistryDTO = this.ministryService.getSelectedMinistry();
    this.editMinistry.name = ministryDTO.name;
    this.editMinistry.unicodeName = ministryDTO.unicodeName;
    this.editMinistry.code = ministryDTO.code;
    this.editMinistry.status= ministryDTO.status;
  }

  submitEditMinistry() {
    this.buttonPressed = true;

    this.ministryService
      .edit(this.ministryId, this.editMinistry)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editMinistryForm.form.contains(error.fieldType)) {
                this.editMinistryForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}