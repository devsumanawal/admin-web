import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { MinistryService } from '../service/ministry.service';

@Injectable()
export class MinistryDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.officeService.getSelectedMinistry() == null) {
      this.router.navigate([AppRoutes.DEPARTMENT]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private officeService: MinistryService,
    public router: Router
  ) { }

}
