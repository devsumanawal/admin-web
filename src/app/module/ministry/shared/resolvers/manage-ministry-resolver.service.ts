import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { MinistryService } from '../service/ministry.service';

@Injectable()
export class ManageMinistryResolverService implements Resolve<any> {
  constructor(
    private officeService: MinistryService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.officeService.getAllMinistry().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
