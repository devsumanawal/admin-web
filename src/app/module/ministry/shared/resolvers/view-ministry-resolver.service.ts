import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { MinistryService } from '../service/ministry.service';

@Injectable()
export class ViewMinistryResolverService implements Resolve<Observable<any>> {
  constructor(
    private officeService: MinistryService,
    private toastr: ToastrService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.officeService.getAllMinistry().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
