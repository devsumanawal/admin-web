import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { MinistryDTO } from '../model/ministry-dto';
import { MinistryCreateModel } from '../model/ministry-create-model';
import { MinistryEditModel } from '../model/ministry-edit-model';


@Injectable()
export class MinistryService {

  selectedMinistry: MinistryDTO;;

  constructor(private http: HttpClient) { }

  getAllMinistry() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.MINISTRY);
  }

  searchAllMinistrys(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.MINISTRY + ApiConstants.SEARCH, searchParam);
  }

  create(ministryCreateModel: MinistryCreateModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.MINISTRY, ministryCreateModel);
  }

  edit(id, ministryEditModel: MinistryEditModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.MINISTRY + ApiConstants.MODIFY + '/' + id, ministryEditModel);
  }

  getMinistryById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.MINISTRY + '/' + id);
  }

  setSelectedMinistry(selectedMinistry: MinistryDTO) {
    this.selectedMinistry = selectedMinistry;
  }

  getSelectedMinistry(): MinistryDTO {
    return this.selectedMinistry;
  }
}
