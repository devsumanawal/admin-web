export class MinistryEditModel {
  id: number;
  name: string;
  unicodeName: string;
  code: string;
  status: string;
}
