export class MinistryDTO {
    id: number;
    name: string;
    code: string;
    status: string;
    unicodeName: string;
}