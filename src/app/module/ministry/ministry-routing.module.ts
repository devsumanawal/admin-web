import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { MinistryCreateComponent } from 'app/module/ministry/create/ministry-create.component';
import { CommonModule } from '@angular/common';
import { MinistryManageComponent } from 'app/module/ministry/manage/ministry-manage.component';
import { MinistryEditComponent } from './manage/edit/ministry-edit.component';
import { EditMinistryResolverService } from './shared/resolvers/edit-ministry-resolver.service';
import { ViewMinistryResolverService } from './shared/resolvers/view-ministry-resolver.service';
import { ManageMinistryResolverService } from './shared/resolvers/manage-ministry-resolver.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'ministry'
    },
    children: [
      {
        path: '',
        component: MinistryCreateComponent,
        pathMatch: 'full',
        resolve: {
          userGroups: ViewMinistryResolverService
        }
      },
      {
        path: AppRoutes.CREATE,
        component: MinistryCreateComponent,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: MinistryManageComponent,
            pathMatch: 'full',
            resolve: {
              userGroups: ManageMinistryResolverService
            }
          },
          {
            path: AppRoutes.EDIT_ID,
            component: MinistryEditComponent,
            pathMatch: 'full',
            resolve: {
              userGroupDto: EditMinistryResolverService,
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class MinistryRoutingModule { }
