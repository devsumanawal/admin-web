import { NgModule } from '@angular/core';

import { FiscalYearRoutingModule } from './fiscal-year-routing.module';
import { FiscalYearCreateComponent } from './create/fiscal-year-create.component';
import { ViewFiscalYearResolverService } from './shared/resolvers/view-fiscal-year-resolver.service';
import { ManageFiscalYearResolverService } from './shared/resolvers/manage-fiscal-year-resolver.service';
import { EditFiscalYearResolverService } from './shared/resolvers/edit-fiscal-year-resolver.service';
import { FiscalYearService } from 'app/module/fiscal-year/shared/service/fiscal-year.service';
import { SharedModule } from 'app/module/shared/shared.module';
import { FiscalYearManageComponent } from 'app/module/fiscal-year/manage/fiscal-year-manage.component';
import { ActivatedRouteSnapshot } from '@angular/router';
import { FiscalYearDetailsGuardService } from 'app/module/fiscal-year/shared/guard/fiscal-year-details-guard.service';
import { FiscalYearEditComponent } from './manage/edit/fiscal-year-edit.component';


@NgModule({
  imports: [
    SharedModule,
    FiscalYearRoutingModule,
  ],
  declarations: [
    FiscalYearCreateComponent,
    FiscalYearManageComponent,
    FiscalYearEditComponent
  ],
  providers: [
    FiscalYearService,
    FiscalYearDetailsGuardService,
    ViewFiscalYearResolverService,
    ManageFiscalYearResolverService,
    EditFiscalYearResolverService,
  ]
})
export class FiscalYearModule { }
