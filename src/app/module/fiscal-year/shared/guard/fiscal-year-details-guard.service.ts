import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { FiscalYearService } from '../service/fiscal-year.service';

@Injectable()
export class FiscalYearDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.fiscalYearService.getSelectedFiscalYear() == null) {
      this.router.navigate([AppRoutes.OFFICE_TYPE]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private fiscalYearService: FiscalYearService,
    public router: Router
  ) { }

}
