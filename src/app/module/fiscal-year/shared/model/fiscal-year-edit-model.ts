export class FiscalYearEditModel {

  name: string;
  startDate: string;
  nepaliStartDate: string;
  endDate: string;
  nepliEndDate: string;
  enabled: boolean;

}
