import { DateUtil } from 'app/module/core/util/date-util.ts';

export class FiscalYearCreateModel {

  name: string;
  startDate: string;
  nepaliStartDate: string;
  endDate: string;
  nepliEndDate: string;
  enabled: boolean;

}
