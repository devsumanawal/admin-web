import { ConvergentAdminLoginDto } from "app/module/admin/shared/model/convergent-admin-login-dto";


export class FiscalYearDTO {
    id: number;
    name: string;
    startDate: string;
    nepaliStartDate: string;
    endDate: string;
    nepliEndDate: string;
    enabled: boolean;
}