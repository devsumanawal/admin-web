import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { FiscalYearDTO } from '../model/fiscal-year-dto';
import { FiscalYearCreateModel } from '../model/fiscal-year-create-model';
import { FiscalYearEditModel } from '../model/fiscal-year-edit-model';


@Injectable()
export class FiscalYearService {

  selectedFiscalYear: FiscalYearDTO;

  constructor(private http: HttpClient) { }

  setSelectedFiscalYear(fiscalYear: FiscalYearDTO) {
    this.selectedFiscalYear = fiscalYear;
  }

  getSelectedFiscalYear(): FiscalYearDTO {
    return this.selectedFiscalYear;
  }

  getAllFiscalYears() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.FISCAL_YEAR);
  }

  searchFiscalYears(searchParam) {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.FISCAL_YEAR);
  }

  searchFiscalYearsForManage() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.FISCAL_YEAR);
  }

  getAllFiscalYearsForManage() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.FISCAL_YEAR);
  }

  getFiscalYearById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.FISCAL_YEAR + '/' + id);
  }

  create(fiscalYearCreateModel: FiscalYearCreateModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.FISCAL_YEAR, fiscalYearCreateModel);
  }

  edit(id: number, fiscalYearEditModel: FiscalYearEditModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.FISCAL_YEAR + ApiConstants.MODIFY + '/' + id, fiscalYearEditModel);
  }

}
