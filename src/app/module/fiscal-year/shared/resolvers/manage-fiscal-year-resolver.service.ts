import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { FiscalYearService } from '../service/fiscal-year.service';

@Injectable()
export class ManageFiscalYearResolverService implements Resolve<any> {
  constructor(
    private fiscalYearService: FiscalYearService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.fiscalYearService.getAllFiscalYearsForManage().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
