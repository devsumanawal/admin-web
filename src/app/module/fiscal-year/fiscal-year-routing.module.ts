import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { FiscalYearCreateComponent } from 'app/module/fiscal-year/create/fiscal-year-create.component';
import { CommonModule } from '@angular/common';
import { FiscalYearManageComponent } from 'app/module/fiscal-year/manage/fiscal-year-manage.component';
import { FiscalYearEditComponent } from './manage/edit/fiscal-year-edit.component';
import { EditFiscalYearResolverService } from './shared/resolvers/edit-fiscal-year-resolver.service';
import { FiscalYearDetailsGuardService } from './shared/guard/fiscal-year-details-guard.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'fiscalYear'
    },
    children: [
      {
        path: '',
        component: FiscalYearManageComponent,
        pathMatch: 'full',
        resolve: {
        }
      },
      {
        path: AppRoutes.CREATE,
        component: FiscalYearCreateComponent,
        pathMatch: 'full',
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: FiscalYearManageComponent,
            pathMatch: 'full',
          },
          {
            path: AppRoutes.EDIT_ID,
            component: FiscalYearEditComponent,
            pathMatch: 'full',
            resolve: {
              FiscalYearDto: EditFiscalYearResolverService,
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class FiscalYearRoutingModule { }
