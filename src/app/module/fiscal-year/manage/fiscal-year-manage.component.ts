import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective, PaginationComponent } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { AppRoutes } from 'app/constants/app-routes';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { FiscalYearDTO } from '../shared/model/fiscal-year-dto';
import { FiscalYearService } from '../shared/service/fiscal-year.service';

@Component({
  selector: 'app-fiscal-year-manage',
  templateUrl: './fiscal-year-manage.component.html',
  styleUrls: ['./fiscal-year-manage.component.scss']
})
export class FiscalYearManageComponent implements OnInit {
  @ViewChild(PaginationComponent)
  paginationComponent: PaginationComponent;

  fiscalYears: Array<FiscalYearDTO> = [];

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private fiscalYearService: FiscalYearService,
    private route: ActivatedRoute,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.fiscalYearService.setSelectedFiscalYear(null);
    this.getAllFiscalYearForSearch(null);
  }

  getAllFiscalYearForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }

    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };

    this.fiscalYearService
      .searchFiscalYears(this.searchParam)
      .subscribe(successResponse => {
        this.fiscalYears = successResponse;
        this.totalItems = successResponse.totalCount;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
        });
    this.searchButtonpressed = false;
  }

  clickViewDetailButton(FiscalYearDto: FiscalYearDTO) {
    this.fiscalYearService.setSelectedFiscalYear(FiscalYearDto);
    this.router.navigate([AppRoutes.FISCAL_YEAR, AppRoutes.DETAILS]);
  }

  clickedEditFiscalYear(FiscalYearDto: FiscalYearDTO) {
    this.fiscalYearService.setSelectedFiscalYear(FiscalYearDto);
    this.router.navigate([AppRoutes.FISCAL_YEAR, AppRoutes.MANAGE, AppRoutes.EDIT, FiscalYearDto.id]);
  }


  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  goBack() {
    this.location.back();
  }


  getChildRole(childRoleName: string) {
    return this.navigationService.getChildRoleOfChildRole('Fiscal Year', 'Fiscal Year Setup', 'Manage Fiscal Year', childRoleName);
  }

}
