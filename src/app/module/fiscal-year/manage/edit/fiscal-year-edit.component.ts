import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { FiscalYearService } from 'app/module/fiscal-year/shared/service/fiscal-year.service';
import { FiscalYearEditModel } from 'app/module/fiscal-year/shared/model/fiscal-year-edit-model';
import { AppRoutes } from 'app/constants/app-routes';
import { FiscalYearDTO } from '../../shared/model/fiscal-year-dto';

@Component({
  selector: 'app-fiscal-year-edit',
  templateUrl: './fiscal-year-edit.component.html',
  styleUrls: ['./fiscal-year-edit.component.scss']
})
export class FiscalYearEditComponent implements OnInit {

  editFiscalYear: FiscalYearEditModel = new FiscalYearEditModel();
  fiscalYearId: number;
  @ViewChild('editFiscalYearForm')
  editFiscalYearForm: NgForm;
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private fiscalYearService: FiscalYearService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.OFFICE_TYPE]);
      }

      this.firstAlreadyLoaded = true;
      this.fiscalYearId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditFiscalYear();
  }

  ngOnDestroy() {
    this.fiscalYearService.setSelectedFiscalYear(null);
  }

  setEditFiscalYear() {
    const fiscalYearDto: FiscalYearDTO = this.fiscalYearService.getSelectedFiscalYear();
    this.editFiscalYear.name = fiscalYearDto.name;
    this.editFiscalYear.startDate = fiscalYearDto.startDate;
    this.editFiscalYear.nepaliStartDate = fiscalYearDto.nepaliStartDate;
    this.editFiscalYear.endDate = fiscalYearDto.endDate;
    this.editFiscalYear.nepliEndDate = fiscalYearDto.nepliEndDate;
    // this.editFiscalYear.enabled = fiscalYearDto.enabled;
  }

  submitEditFiscalYear() {

    this.buttonPressed = true;

    this.fiscalYearService
      .edit(this.fiscalYearId, this.editFiscalYear)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editFiscalYearForm.form.contains(error.fieldType)) {
                this.editFiscalYearForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}
