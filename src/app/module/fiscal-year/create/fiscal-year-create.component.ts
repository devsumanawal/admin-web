import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';

import { ToastrService } from 'ngx-toastr';
import { FiscalYearCreateModel } from 'app/module/fiscal-year/shared/model/fiscal-year-create-model';
import { FiscalYearService } from '../shared/service/fiscal-year.service';


@Component({
  selector: 'app-fiscal-year-create',
  templateUrl: './fiscal-year-create.component.html'
})
export class FiscalYearCreateComponent implements OnInit {

  @ViewChild('createFiscalYearForm')
  createFiscalYearForm: any;

  fiscalYear: FiscalYearCreateModel = new FiscalYearCreateModel();
  serverErrorMessages: string[] = [];
  buttonPressed = false;

  constructor(
    private location: Location,
    private FiscalYearService: FiscalYearService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
  }

  create(form: NgForm) {
    console.log('To Persist::: ', this.fiscalYear);
    this.buttonPressed = true;
    this.FiscalYearService
      .create(this.fiscalYear)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.resetForm();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (form.form.contains(error.fieldType)) {
                form.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }


  back() {
    this.location.back();
  }

  resetForm() {
    this.fiscalYear = new FiscalYearCreateModel();
    this.createFiscalYearForm.form.markAsPristine();
    this.createFiscalYearForm.form.markAsUntouched();
  }


}
