import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { PaginationComponent } from 'ngx-bootstrap';
import { AppRoutes } from 'app/constants/app-routes';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { OfficeDTO } from '../shared/model/office-dto';
import { OfficeService } from '../shared/service/office.service';
import { OfficeTypeService } from 'app/module/core/service/office-type/officeType.service';

@Component({
  selector: 'app-office-manage',
  templateUrl: './office-manage.component.html',
  styleUrls: ['./office-manage.component.scss']
})

export class OfficeManageComponent implements OnInit {

  officeForManage: Array<OfficeDTO> = [];
  selectedOffice: OfficeDTO = new OfficeDTO();

  @ViewChild(PaginationComponent)
  paginationComponent: PaginationComponent;

  cardViewEnabled: Boolean = true;

  filterParam: any;

  officeTypes: any;
  provinces: any;

  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;

  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private location: Location,
    private officeService: OfficeService,
    private toastr: ToastrService,
    private router: Router,
    public navigationService: NavigationService,
    public officeTypeService: OfficeTypeService

  ) {
  }

  ngOnInit() {
    this.resetSearch();
    this.setOfficeDto(null);
    this.officeService.setSelectedForOfficeDetail(null);
    this.cardViewEnabled = localStorage.getItem(SessionStorageConstants.CardViewEnabled) === 'true';
  }
  resetSearch() {
    this.filterParam = [
      { key: 'name', value: '', label: 'Name', alias: 'Name' },
    ];
    this.searchOfficeOnUserInput();
  }

  searchOfficeOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllOfficeForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllOfficeForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }

    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };
    this.officeService
      .searchAllOffices(this.searchParam)
      .subscribe(successResponse => {
        this.officeForManage = successResponse.object;
        this.searchButtonpressed = false;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
          this.searchButtonpressed = false;
        });
  }

  goBack() {
    this.router.navigate(['office']);
  }


  clickViewDetailButton(officeDTO: OfficeDTO) {
    this.setOfficeDto(officeDTO);
    this.router.navigate([AppRoutes.OFFICE, AppRoutes.DETAILS]);
  }

  clickEditOffice(selected: OfficeDTO) {
    this.officeService.setSelectedForOfficeDetail(selected);
    this.router.navigate([AppRoutes.OFFICE, AppRoutes.MANAGE, AppRoutes.EDIT, selected.id]);
  }


  setOfficeDto(officeDTO: OfficeDTO) {
    this.officeService.setSelectedForOfficeDetail(officeDTO);
  }

  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  toggleCardViewEnabled() {
    this.cardViewEnabled = !this.cardViewEnabled;
    localStorage.setItem(SessionStorageConstants.CardViewEnabled, (this.cardViewEnabled ? 'true' : 'false'));
  }


  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('Office', 'Office Setup', 'Manage Office', childRoleName);
  }

  isActive(active: string) {
    return active === "Y";
  }

}