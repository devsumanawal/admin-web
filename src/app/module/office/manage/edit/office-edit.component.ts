import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'app/constants/app-routes';
import { OfficeDTO } from '../../shared/model/office-dto';
import { OfficeEditModel } from '../../shared/model/office-edit-model';
import { OfficeService } from '../../shared/service/office.service';
import { OfficeTypeService } from 'app/module/core/service/office-type/officeType.service';
import { DepartmentService } from 'app/module/core/service/office/department.service';

@Component({
  selector: 'app-office-edit',
  templateUrl: './office-edit.component.html',
  styleUrls: ['./office-edit.component.scss']
})

export class OfficeEditComponent implements OnInit {

  editOffice: OfficeEditModel = new OfficeEditModel();
  officeId: number;


  @ViewChild('editOfficeForm')
  editOfficeForm: NgForm;

  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private officeService: OfficeService,
    private officeTypeService: OfficeTypeService,
    private departmentService: DepartmentService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.OFFICE]);
      }

      this.firstAlreadyLoaded = true;
      this.officeId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.setEditOffice();
  }

  ngOnDestroy() {
    this.officeService.setSelectedForOfficeDetail(null);
  }

  setEditOffice() {
    const officeDTO: OfficeDTO = this.officeService.getSelectedForOffice();
    this.editOffice.name = officeDTO.name;
    this.editOffice.unicodeName = officeDTO.unicodeName;
    this.editOffice.officeTypeId = officeDTO.officeTypeDto.id;
    this.editOffice.provinceId = officeDTO.provinceDto.id;
    this.editOffice.departmentId = officeDTO.departmentDto.id;
    this.editOffice.active = officeDTO.active;
  }

  submitEditOffice() {
    this.buttonPressed = true;

    this.officeService
      .edit(this.officeId, this.editOffice)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editOfficeForm.form.contains(error.fieldType)) {
                this.editOfficeForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}