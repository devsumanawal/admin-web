import { NgModule } from '@angular/core';


import { SharedModule } from 'app/module/shared/shared.module';
import { ActivatedRouteSnapshot } from '@angular/router';
import { EditOfficeResolverService } from './shared/resolvers/edit-office-resolver.service';
import { ManageOfficeResolverService } from './shared/resolvers/manage-office-resolver.service';
import { OfficeService } from './shared/service/office.service';
import { OfficeDetailsGuardService } from './shared/guard/office-details-guard.service';
import { ViewOfficeResolverService } from './shared/resolvers/view-office-resolver.service';
import { OfficeComponent } from './office.component';
import { OfficeCreateComponent } from './create/office-create.component';
import { OfficeEditComponent } from './manage/edit/office-edit.component';
import { OfficeManageComponent } from './manage/office-manage.component';
import { OfficeDetailComponent } from './details/office-details.component';
import { OfficeRoutingModule } from './office-routing.module';


@NgModule({
  imports: [
    SharedModule,
    OfficeRoutingModule
  ],
  declarations: [
    OfficeComponent,
    OfficeCreateComponent,
    OfficeEditComponent,
    OfficeManageComponent,
    OfficeDetailComponent
  ],
  providers: [
    OfficeService,
    OfficeDetailsGuardService,
    ViewOfficeResolverService,
    ManageOfficeResolverService,
    EditOfficeResolverService
    // UserGroupProfileForCreateResolverService
  ]
})
export class OfficeModule { }
