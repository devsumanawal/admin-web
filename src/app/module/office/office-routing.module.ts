import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { OfficeCreateComponent } from 'app/module/office/create/office-create.component';
import { CommonModule } from '@angular/common';
import { OfficeManageComponent } from 'app/module/office/manage/office-manage.component';
import { OfficeDetailComponent } from './details/office-details.component';
import { OfficeEditComponent } from './manage/edit/office-edit.component';
import { EditOfficeResolverService } from './shared/resolvers/edit-office-resolver.service';
import { OfficeDetailsGuardService } from './shared/guard/office-details-guard.service';
import { ViewOfficeResolverService } from './shared/resolvers/view-office-resolver.service';
import { ManageOfficeResolverService } from './shared/resolvers/manage-office-resolver.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'office'
    },
    children: [
      {
        path: '',
        component: OfficeCreateComponent,
        pathMatch: 'full',
        resolve: {
         userGroups: ViewOfficeResolverService
        }
      },
      {
        path: AppRoutes.CREATE,
        component: OfficeCreateComponent,
        pathMatch: 'full',
        resolve: {
          // userGroups: UserGroupProfileForCreateResolverService
        }
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: OfficeManageComponent,
            pathMatch: 'full',
            resolve: {
              userGroups: ManageOfficeResolverService
            }
          },
          {
            path: AppRoutes.EDIT_ID,
            component: OfficeEditComponent,
            pathMatch: 'full',
            resolve: {
              userGroupDto: EditOfficeResolverService,
              // profiles: AdminProfileForCreateResolverService
            }
          }
        ]
      },
      {
        path: AppRoutes.DETAILS,
        component: OfficeDetailComponent,
        pathMatch: 'full',
        canActivate: [
          OfficeDetailsGuardService
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class OfficeRoutingModule { }
