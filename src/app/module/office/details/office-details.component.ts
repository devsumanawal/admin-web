import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { OfficeService } from '../shared/service/office.service';
import { OfficeDTO } from '../shared/model/office-dto';


@Component({
  selector: 'app-office-details',
  templateUrl: './office-details.component.html',
  styleUrls: ['./office-details.component.scss']
})
export class OfficeDetailComponent implements OnInit {

  office: OfficeDTO = new OfficeDTO();

  constructor(
    officeService: OfficeService,
    private location: Location) {
    this.office = officeService.getSelectedForOffice();
  }

  ngOnInit() {
  }

  goBack() {
    this.location.back();
  }

  status(active: boolean) {
    if (active) {
      return 'Active';
    } else {
      return 'Blocked';
    }
  }
}
