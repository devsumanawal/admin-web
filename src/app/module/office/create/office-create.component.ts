import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToastrService } from 'ngx-toastr';
import { OfficeCreateModel } from 'app/module/office/shared/model/office-create-model';
import { OfficeTypeService } from 'app/module/core/service/office-type/officeType.service';
import { OfficeService } from '../shared/service/office.service';
import { DepartmentService } from 'app/module/core/service/office/department.service';


@Component({
  selector: 'app-office-create',
  templateUrl: './office-create.component.html'
})
export class OfficeCreateComponent implements OnInit {

  @ViewChild('createOfficeForm')
  createOfficeForm: any;

  office: OfficeCreateModel = new OfficeCreateModel();
  adminProfileForCreate: Array<any> = [];
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  profiles = [];

  constructor(
    private location: Location,
    private officeService: OfficeService,
    private officeTypeService: OfficeTypeService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private departmentService: DepartmentService
  ) {
  }

  ngOnInit() {

    console.log('Form Value: ', this.createOfficeForm);
  }

  create(form: NgForm) {
    this.buttonPressed = true;
    this.officeService
      .create(this.office)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.resetForm();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (form.form.contains(error.fieldType)) {
                form.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }


  back() {
    this.location.back();
  }

  resetForm() {
    this.office = new OfficeCreateModel();
    this.createOfficeForm.form.markAsPristine();
    this.createOfficeForm.form.markAsUntouched();
  }

}
