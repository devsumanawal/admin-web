import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'app/constants/app-routes';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { SearchParam } from 'app/module/core/models/search-param';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { NgForm } from '@angular/forms';
import { PaginationComponent } from 'ngx-bootstrap';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { OfficeDTO } from './shared/model/office-dto';
import { OfficeService } from './shared/service/office.service';

@Component({
  selector: 'app-office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.scss']
})

export class OfficeComponent implements OnInit {

  @ViewChild(PaginationComponent)
  paginationComponent: PaginationComponent;

  offices: Array<OfficeDTO> = [];

  cardViewEnabled: Boolean = true;

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;

  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private officeService: OfficeService,
    private toastr: ToastrService,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.resetSearch();
    this.setOfficeDto(null);
    this.cardViewEnabled = localStorage.getItem(SessionStorageConstants.CardViewEnabled) === 'true';
  }

  searchOfficeOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllOfficesForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllOfficesForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }
    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };
    this.officeService
      .searchAllOffices(this.searchParam)
      .subscribe(successResponse => {
        this.offices = successResponse.object;
        this.totalItems = successResponse.totalCount;
        this.searchButtonpressed = false;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
          this.searchButtonpressed = false;
        });
  }

  resetSearch() {
    this.filterParam = [

      { key: 'office.name', value: '' },
      { key: 'office.unicodeName', value: '' },
      { key: 'officeType.id', value: '', condition: '=' },
      { key: 'province.id', value: '', condition: '=' }
    ];
    this.searchOfficeOnUserInput();
  }

  setOfficeDto(officeDTO: OfficeDTO) {
    this.officeService.setSelectedForOfficeDetail(officeDTO);
  }

  clickViewDetailButton(officeDTO: OfficeDTO) {
    this.setOfficeDto(officeDTO);
    this.router.navigate([AppRoutes.OFFICE, AppRoutes.DETAILS]);
  }

  toggleCardViewEnabled() {
    this.cardViewEnabled = !this.cardViewEnabled;
    localStorage.setItem(SessionStorageConstants.CardViewEnabled, (this.cardViewEnabled ? 'true' : 'false'));
  }

  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRole('Office Setup', 'Setup Office', childRoleName);
  }
}

