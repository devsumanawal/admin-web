import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { OfficeService } from '../service/office.service';

@Injectable()
export class ManageOfficeResolverService implements Resolve<any> {
  constructor(
    private officeService: OfficeService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.officeService.getAllOfficeForManage().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
