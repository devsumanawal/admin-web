
export class OfficeCreateModel {

  name: string;
  unicodeName: string;
  officeTypeId: any;
  provinceId: any;
  ministryId: any;
  departmentId: any;
  active: boolean;
}
