import { AdminProfileDto } from "app/module/admin-profile/shared/model/admin-profile-dto-model";
import { OfficeTypeDTO } from "app/module/office-type/shared/model/office-type-dto";
import { ProvinceDTO } from "./province-dto";
import { MinistryDTO } from "app/module/ministry/shared/model/ministry-dto";
import { DepartmentDTO } from "app/module/department/shared/model/department-dto";

export class OfficeDTO {
    id: number;
    name: string;
    officeCode: string;
    unicodeName: string;
    officeTypeId: any;
    provinceId: any;
    officeTypeDto: OfficeTypeDTO;
    provinceDto: ProvinceDTO;
    departmentDto: DepartmentDTO;
    active: boolean;
}