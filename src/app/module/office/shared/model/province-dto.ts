
export class ProvinceDTO {
    id: number;
    name: string;
    unicodeName: string;
}