import { ProvinceDTO } from "./province-dto";
import { OfficeTypeDTO } from "app/module/office-type/shared/model/office-type-dto";
import { DepartmentDTO } from "app/module/department/shared/model/department-dto";

export class OfficeEditModel {
  id: number;
  name: string;
  unicodeName: string;
  officeTypeId: any;
  provinceId: any;
  departmentId: any;
  officeTypeDto: OfficeTypeDTO;
  provinceDto: ProvinceDTO;
  departmentDto: DepartmentDTO;
  active: boolean;
}
