import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { OfficeDTO } from '../model/office-dto';
import { OfficeCreateModel } from '../model/office-create-model';
import { OfficeTypeEditModel } from 'app/module/office-type/shared/model/office-type-edit-model';


@Injectable()
export class OfficeService {

  selectedOffice: OfficeDTO;;

  constructor(private http: HttpClient) { }

  getAllOffice() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.OFFICE);
  }

  searchAllOffices(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.OFFICE + ApiConstants.SEARCH, searchParam);
  }



  create(officeCreateModel: OfficeCreateModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.OFFICE, officeCreateModel);
  }

  edit(id, officeTypeEditModel: OfficeTypeEditModel) {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.OFFICE + ApiConstants.MODIFY + '/' + id, officeTypeEditModel);
  }

  getOfficeById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.OFFICE + '/' + id);
  }

  setSelectedForOfficeDetail(selectedOffice: OfficeDTO) {
    this.selectedOffice = selectedOffice;
  }

  getSelectedForOffice(): OfficeDTO {
    return this.selectedOffice;
  }

  searchOfficeForManage(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.OFFICE + ApiConstants.MANAGE + ApiConstants.SEARCH, searchParam);
  }


  getAllOfficeForManage(): any {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.OFFICE + ApiConstants.MANAGE);
  }
}
