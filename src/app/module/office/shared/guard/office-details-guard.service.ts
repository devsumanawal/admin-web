import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { OfficeService } from '../service/office.service';

@Injectable()
export class OfficeDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.officeService.getSelectedForOffice() == null) {
      this.router.navigate([AppRoutes.OFFICE]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private officeService: OfficeService,
    public router: Router
  ) { }

}
