import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordComponent } from './change-password.component';
import { ChangePasswordService } from 'app/module/change-password/shared/change-password.service';
import { SharedModule } from 'app/module/shared/shared.module';
import { PasswordPolicyModule } from 'app/module/password-policy/password-policy.module';
import { IsFirstLoginGuard } from 'app/module/core/guard/is-first-login.guard';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PasswordPolicyModule,
  ],
  declarations: [ChangePasswordComponent],
  providers: [ChangePasswordService, IsFirstLoginGuard],
  exports: [ChangePasswordComponent]
})
export class ChangePasswordModule { }
