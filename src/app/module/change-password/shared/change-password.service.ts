import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from 'app/constants/api-constants';
import { ChangePasswordModel } from 'app/module/change-password/shared/change-password-model';
import { Observable } from 'rxjs/Observable';
import { BackendResponse } from 'app/module/core/models/backend-response';

@Injectable()
export class ChangePasswordService {

  constructor(private http: HttpClient) { }

  changePassword(changePasswordModel: ChangePasswordModel): Observable<BackendResponse> {
    return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.CHANGE_PASSWORD, changePasswordModel);
  }
}
