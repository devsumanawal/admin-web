import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ChangePasswordService } from 'app/module/change-password/shared/change-password.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Validators } from '@angular/forms';
import { AbstractControl } from '@angular/forms/src/model';
import { AuthorizationService } from 'app/module/core/service/authorization.service';
import { ChangePasswordModel } from 'app/module/change-password/shared/change-password-model';
import { PasswordPolicyModel } from 'app/module/password-policy/shared/password-policy-model';
import { PasswordPoliciesValidationService } from 'app/module/core/service/validation/password-policies-validation.service';
import { PasswordPolicyComponent } from 'app/module/password-policy/password-policy.component';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  @ViewChild(PasswordPolicyComponent) passwordPolicyComponent: PasswordPolicyComponent;

  changePassword: ChangePasswordModel = new ChangePasswordModel();
  passwordPolicies: Array<PasswordPolicyModel> = [];
  validationResult: Array<string> = [];
  buttonPressed = false;
  @ViewChild('changePasswordForm') changePasswordForm: any;

  constructor(
    private changePasswordService: ChangePasswordService,
    private toastr: ToastrService,
    private router: Router,
    private authService: AuthorizationService,
    private route: ActivatedRoute,
    private validationService: PasswordPoliciesValidationService,
  ) {
  }

  ngOnInit() {
  }

  loadChangePasswordComponent() {
    this.changePasswordForm.form.reset();
    this.changePassword = new ChangePasswordModel();
  }

  submit() {
    this.buttonPressed = true;
    this.changePasswordService
      .changePassword(this.changePassword)
      .subscribe(response => {
        this.buttonPressed = false;
        this.toastr.success(response.message);
        this.authService.removeToken();
        this.router.navigate([AppRoutes.LOGIN]);
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          }

          errorResponse.error.forEach(error => {
            this.toastr.error(error.message);
          });

        });
  }

  validate(password: string, index: number) {
    if (password && this.passwordPolicies) {
      this.validationResult[index] =
        this.validationService.validate(password, this.passwordPolicies);
    }
  }


}
