import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'app/module/admin/shared/service/admin.service';
import { ConvergentAdminLoginDto } from 'app/module/admin/shared/model/convergent-admin-login-dto';
import { AdminEditModel } from 'app/module/admin/shared/model/admin-edit-model';
import { AppRoutes } from 'app/constants/app-routes';
import { OfficeService, Office } from 'app/module/core/service/office';
import { AdminCreateModel } from '../../shared/model/admin-create-model';
import { UserGroupService } from 'app/module/user-group/shared/service/user-group.service';
import { UserGroupDTO } from 'app/module/user-group/shared/model/user-group-dto';

@Component({
    selector: 'app-admin-edit',
    templateUrl: './admin-edit.component.html',
})
export class AdminEditComponent implements OnInit {
    adminId: number;
    admin: AdminEditModel = new AdminEditModel();
    adminProfiles: Array<any> = [];
    serverErrorMessages: string[] = [];
    buttonPressed = false;

    offices: Office[] = [];
    userGroups: UserGroupDTO[] = [];

    constructor(
        private location: Location,
        private route: ActivatedRoute,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private toastr: ToastrService,
        private adminService: AdminService,
        public officeService: OfficeService,
        public userGroupService: UserGroupService
    ) {
    }

    ngOnInit() {
        this.officeService.offices.subscribe((offices) => this.offices = offices);
        this.userGroupService.getAllUserGroup().subscribe((userGroups) => this.userGroups = userGroups);
        this.route.params.subscribe(params => {
            this.adminId = Number(params.id);
            this.adminService.getAdminById(this.adminId).subscribe((data) => {
                const { officeAndUserGroups, ...admin } = data[0];
                this.admin = admin;
                this.admin.officeUserGroupList = officeAndUserGroups.map((list) => {
                    return {
                        officeId: list.office.id.toString(),
                        userGroupId: list.userGroup.id.toString() || ''
                    };
                });
            });
        });
    }

    addOffice() {
        const officeUserGroupList = [
            ...this.admin.officeUserGroupList,
            {
                officeId: '',
                userGroupId: ''
            }
        ];
        this.admin = {
            ... this.admin,
            officeUserGroupList
        }
        console.log(this.admin.officeUserGroupList)
    }

    removeOffice(index: number) {
        if (index === 0 && this.admin.officeUserGroupList.length === 1) {
            return;
        }
        this.admin.officeUserGroupList.splice(index, 1);
    }

    edit(form: NgForm) {
        this.buttonPressed = true;
        this.adminService
            .edit(this.admin, this.adminId)
            .subscribe(response => {
                this.toastr.success(response.message);
                this.buttonPressed = false;
                this.back();
            },
                errorResponse => {
                    this.buttonPressed = false;
                    if (errorResponse.error.message) {
                        this.toastr.error(errorResponse.error.message);
                    } else {
                        errorResponse.error.forEach(error => {
                            if (form.form.contains(error.fieldType)) {
                                form.controls[error.fieldType].setErrors({ server: true });
                                this.serverErrorMessages[error.fieldType] = error.message;
                            } else {
                                this.toastr.error(error.message);
                            }
                        });
                    }
                });
    }

    back() {
        this.router.navigate([AppRoutes.ADMIN, AppRoutes.MANAGE]);
    }

    cancelForm() {
        this.router.navigate([AppRoutes.ADMIN, AppRoutes.MANAGE]);
    }

    userType(isSuper: string) {
        if (isSuper === 'Y') {
            return "Super User";
        } else if (isSuper === 'N') {
            return "Normal User";
        } else if (isSuper === 'S') {
            return "Master User";
        } else {
            return "N/A";
        }
    }
}
