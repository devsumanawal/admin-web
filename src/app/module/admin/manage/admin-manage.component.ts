import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { PaginationComponent } from 'ngx-bootstrap';
import { ConvergentAdminLoginDto } from 'app/module/admin/shared/model/convergent-admin-login-dto';
import { AdminService } from 'app/module/admin/shared/service/admin.service';
import { AppRoutes } from 'app/constants/app-routes';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';

@Component({
    selector: 'app-admin-manage',
    templateUrl: './admin-manage.component.html',
})
export class AdminManageComponent implements OnInit {
    adminsForManage: Array<ConvergentAdminLoginDto> = new Array<ConvergentAdminLoginDto>();
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
    selectedAdmin: ConvergentAdminLoginDto = new ConvergentAdminLoginDto();
    cardViewEnabled = false;

    filterParam: any;
    adminProfiles: any;
    searchParam = new SearchParam();
    pageEvent: PageChangedEvent;
    page = PaginationConstant.page;
    size = PaginationConstant.size;
    totalItems: number;
    searchButtonpressed = false;

    statusListForSearch = StatusDescriptionConstants.getStatusForFilter();
    admins: any[] = [];

    constructor(
        private location: Location,
        private adminService: AdminService,
        private toastr: ToastrService,
        private router: Router,
        private adminProfileService: AdminProfileService,
        public navigationService: NavigationService,
    ) {
    }

    ngOnInit() {
        this.resetSearch();
        this.adminService.setSelectedForAdminDetail(null);
        // this.setConvergentAdminLoginDto(null);
        // this.getAdminProfiles();
        // this.cardViewEnabled = localStorage.getItem(SessionStorageConstants.CardViewEnabled) === 'true';
        // this.adminService.getAllAdmins().subscribe((admins) => {
        //     this.admins = admins;
        // });
    }

    resetSearch() {
        this.filterParam = [
            { key: 'username', value: '', label: 'UserName', alias: 'UserName' },
            { key: 'name', value: '', label: 'Name', alias: 'Name' }
        ];
        this.searchAdminOnUserInput();
    }

    searchAdminOnUserInput() {
        if (this.paginationComponent.page === 1) {
            this.getAllAdminsForSearch(null);
        } else {
            this.paginationComponent.selectPage(1);
        }
    }

    getAllAdminsForSearch(event?: PageChangedEvent) {
        this.searchButtonpressed = true;
        this.page = 1;
        if (event) {
            this.pageEvent = event;
            this.page = event.page;
            this.size = event.itemsPerPage;
        }
        this.searchParam = {
            search: this.filterParam,
            page: this.page,
            size: this.size,
        };
        this.adminService
            .searchAdminsForManage(this.searchParam)
            .subscribe(successResponse => {
                this.admins = successResponse.object;
                this.totalItems = successResponse.totalCount;
                this.searchButtonpressed = false;
            },
                errorResponse => {
                    this.toastr.error(errorResponse.error.message);
                    this.searchButtonpressed = false;
                });
    }

    goBack() {
        this.router.navigate(['admin']);
    }

    clickViewDetailButton(convergentAdminLoginDto: ConvergentAdminLoginDto) {
        this.adminService.setSelectedForAdminDetail(convergentAdminLoginDto);
        this.router.navigate([AppRoutes.ADMIN, AppRoutes.DETAILS]);
    }

    setConvergentAdminLoginDto(convergentAdminLoginDto: ConvergentAdminLoginDto) {
        this.adminService.setSelectedForAdminDetail(convergentAdminLoginDto);
    }

    deleteAdmin(event) {
        this.adminService.deleteAdmin(event).subscribe(
            (success) => {
                this.getAllAdminsForSearch(this.pageEvent);
                this.toastr.success(success.message)
            },
            (errorResponse) => this.handleErrorResponse(errorResponse)
        )
    }

    blockAdmin(event) {
        this.adminService.blockAdmin(event).subscribe(
            (success) => {
                this.getAllAdminsForSearch(this.pageEvent);
                this.toastr.success(success.message)
            },
            (errorResponse) => this.handleErrorResponse(errorResponse)
        )
    }

    unBlockAdmin(event) {
        this.adminService.unBlockAdmin(event).subscribe(
            (success) => {
                this.getAllAdminsForSearch(this.pageEvent);
                this.toastr.success(success.message)
            },
            (errorResponse) => this.handleErrorResponse(errorResponse)
        )
    }

    resetAdminPassword(event) {
        this.adminService.resetAdminPassword(event).subscribe(
            (success) => {
                this.getAllAdminsForSearch(this.pageEvent);
                this.toastr.success(success.message)
            },
            (errorResponse) => this.handleErrorResponse(errorResponse)
        )
    }

    resetAdminTOTP(event) {
        this.adminService.resetAdminTOTP(event).subscribe(
            (success) => {
                this.getAllAdminsForSearch(this.pageEvent);
                this.toastr.success(success.message)
            },
            (errorResponse) => this.handleErrorResponse(errorResponse)
        )
    }

    private handleErrorResponse(errorResponse) {
        if (errorResponse.error.message) {
            this.toastr.error((errorResponse.error.message));
        } else {
            errorResponse.error.forEach(error => {
                this.toastr.error((error.message));
            });
        }
    }

    toggleCardViewEnabled() {
        this.cardViewEnabled = !this.cardViewEnabled;
        localStorage.setItem(SessionStorageConstants.CardViewEnabled, (this.cardViewEnabled ? 'true' : 'false'));
    }

    getAdminProfiles() {
        this.adminProfileService.getAllAdminProfiles()
            .subscribe((successResponse) => {
                this.adminProfiles = successResponse;
            }, errorResponse => {
                this.toastr.error(errorResponse.error.message);
            }
            );
    }

    getChildRole(childRoleName: string): NavigationRoleResponseModel {
        return this.navigationService.getChildRoleOfChildRole('User Setup', 'Setup Admin', 'Manage Admin', childRoleName);
    }


    userType(isSuper: string) {
        if (isSuper == 'Y') {
            return "Super User";
        } else if (isSuper == 'N') {
            return "Normal User";
        } else {
            return "N/A";
        }
    }

    isActive(active: string) {
        return active === "Y";
    }

}
