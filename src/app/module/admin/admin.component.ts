import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { SessionStorageConstants } from 'app/constants/session-storage-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { ConvergentAdminLoginDto } from 'app/module/admin/shared/model/convergent-admin-login-dto';
import { AdminService } from 'app/module/admin/shared/service/admin.service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';
import { SearchParam } from 'app/module/core/models/search-param';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { PaginationComponent } from 'ngx-bootstrap';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;

    roleNames: Array<String> = [];
    admins: Array<ConvergentAdminLoginDto> = new Array<ConvergentAdminLoginDto>();
    cardViewEnabled: Boolean = true;

    filterParam: any;
    adminProfiles: any;
    searchParam = new SearchParam();
    pageEvent: PageChangedEvent;
    page = PaginationConstant.page;
    size = PaginationConstant.size;
    totalItems: number;
    searchButtonpressed = false;

    statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private adminService: AdminService,
        private adminProfileService: AdminProfileService,
        private toastr: ToastrService,
        public navigationService: NavigationService,
    ) { }

    ngOnInit() {
        this.resetSearch();
        this.setConvergentAdminLoginDto(null);
        this.getAdminProfiles();
        this.cardViewEnabled = localStorage.getItem(SessionStorageConstants.CardViewEnabled) === 'true';
    }

    searchAdminOnUserInput() {
        if (this.paginationComponent.page === 1) {
            this.getAllAdminsForSearch(null);
        } else {
            this.paginationComponent.selectPage(1);
        }
    }

    getAllAdminsForSearch(event?: PageChangedEvent) {
        this.searchButtonpressed = true;
        this.page = 1;
        if (event) {
            this.pageEvent = event;
            this.page = event.page;
            this.size = event.itemsPerPage;
        }
        
        this.adminService
            .searchAllAdmins(this.searchParam)
            .subscribe(successResponse => {
                this.admins = successResponse.object;
                this.totalItems = successResponse.totalCount;
                this.searchButtonpressed = false;
            },
                errorResponse => {
                    this.toastr.error(errorResponse.error.message);
                    this.searchButtonpressed = false;
                });
    }

    resetSearch() {
        this.filterParam = [
            { key: 'adminProfile.id', value: '', condition: '=' },
            { key: 'status.name', value: '', condition: '=' },
            { key: 'convergentAdmin.name', value: '' },
            { key: 'username', value: '' }
        ];
        this.searchAdminOnUserInput();
    }

    setConvergentAdminLoginDto(convergentAdminLoginDto: ConvergentAdminLoginDto) {
        this.adminService.setSelectedForAdminDetail(convergentAdminLoginDto);
    }

    clickViewDetailButton(convergentAdminLoginDto: ConvergentAdminLoginDto) {
        this.setConvergentAdminLoginDto(convergentAdminLoginDto);
        this.router.navigate([AppRoutes.ADMIN, AppRoutes.DETAILS]);
    }

    toggleCardViewEnabled() {
        this.cardViewEnabled = !this.cardViewEnabled;
        localStorage.setItem(SessionStorageConstants.CardViewEnabled, (this.cardViewEnabled ? 'true' : 'false'));
    }

    getAdminProfiles() {
        this.adminProfileService.getAllAdminProfiles()
            .subscribe((successResponse) => {
                this.adminProfiles = successResponse;
            }, errorResponse => {
                this.toastr.error(errorResponse.error.message);
            }
            );
    }

    isActive(statusDescription: string) {
        return StatusDescriptionConstants.isActive(statusDescription);
    }

    getChildRole(childRoleName: string): NavigationRoleResponseModel {
        return this.navigationService.getChildRole('User Setup', 'Setup Admin', childRoleName);
    }
}

