import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { AdminComponent } from 'app/module/admin/admin.component';
import { AdminCreateComponent } from 'app/module/admin/create/admin-create.component';
import { AdminDetailsComponent } from 'app/module/admin/details/admin-details.component';
import { AdminManageComponent } from 'app/module/admin/manage/admin-manage.component';
import { AdminEditComponent } from 'app/module/admin/manage/edit/admin-edit.component';
import { AdminDetailsGuardService } from 'app/module/admin/shared/guard/admin-details-guard.service';
import { AdminProfileForCreateResolverService } from 'app/module/admin/shared/resolvers/admin-profile-for-create-resolver.service';
import { EditAdminResolverService } from 'app/module/admin/shared/resolvers/edit-admin-resolver.service';


const routeConfig = [
    {
        path: '',
        data: {
            title: 'Admin'
        },
        children: [
            {
                path: '',
                component: AdminManageComponent,
                pathMatch: 'full',
                resolve: {
                    // admins: ViewAdminResolverService
                }
            },
            {
                path: AppRoutes.CREATE,
                component: AdminCreateComponent,
                pathMatch: 'full',
                resolve: {
                    // profiles: AdminProfileForCreateResolverService
                }
            },
            {
                path: AppRoutes.MANAGE,
                children: [
                    {
                        path: '',
                        component: AdminManageComponent,
                        pathMatch: 'full',
                        resolve: {
                            // admins: ManageAdminResolverService
                        }
                    },
                    {
                        path: AppRoutes.EDIT_ID,
                        component: AdminEditComponent,
                        pathMatch: 'full',
                        resolve: {
                            // adminLoginDto: EditAdminResolverService,
                            // profiles: AdminProfileForCreateResolverService
                        }
                    }
                ]
            },
            {
                path: AppRoutes.DETAILS,
                component: AdminDetailsComponent,
                pathMatch: 'full',
                canActivate: [
                    AdminDetailsGuardService
                ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routeConfig)],
    declarations: [],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
