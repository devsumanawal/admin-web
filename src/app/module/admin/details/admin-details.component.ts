import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ConvergentAdminLoginDto } from 'app/module/admin/shared/model/convergent-admin-login-dto';
import { AdminService } from 'app/module/admin/shared/service/admin.service';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';


@Component({
  selector: 'app-admin-details',
  templateUrl: './admin-details.component.html',
  styleUrls: ['./admin-details.component.scss']
})
export class AdminDetailsComponent implements OnInit {
  admin: ConvergentAdminLoginDto = new ConvergentAdminLoginDto();

  constructor(adminService: AdminService,
    private location: Location) {
    this.admin = adminService.getSelectedForAdminDetail();
  }

  ngOnInit() {
  }

  goBack() {
    this.location.back();
  }

  isActive(statusDescription: string) {
    return StatusDescriptionConstants.isActive(statusDescription);
  }

  userType(isSuper: string) {
    if (isSuper == 'Y') {
      return "Super User";
    } else if (isSuper == 'N') {
      return "Normal User";
    } else if (isSuper == 'S') {
      return "Master User";
    } else {
      return "N/A";
    }
  }

}
