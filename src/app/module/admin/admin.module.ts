import { NgModule } from '@angular/core';

import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminCreateComponent } from './create/admin-create.component';
import { ViewAdminResolverService } from './shared/resolvers/view-admin-resolver.service';
import { ManageAdminResolverService } from './shared/resolvers/manage-admin-resolver.service';
import { EditAdminResolverService } from './shared/resolvers/edit-admin-resolver.service';
import { AdminProfileForCreateResolverService } from './shared/resolvers/admin-profile-for-create-resolver.service';
import { AdminDetailsComponent } from './details/admin-details.component';
import { AdminService } from 'app/module/admin/shared/service/admin.service';
import { SharedModule } from 'app/module/shared/shared.module';
import { AdminProfileModule } from 'app/module/admin-profile/admin-profile.module';
import { AdminManageComponent } from 'app/module/admin/manage/admin-manage.component';
import { AdminEditComponent } from 'app/module/admin/manage/edit/admin-edit.component';
import { ActivatedRouteSnapshot } from '@angular/router';
import { AdminDetailsGuardService } from 'app/module/admin/shared/guard/admin-details-guard.service';


@NgModule({
  imports: [
    SharedModule,
    AdminRoutingModule,
    AdminProfileModule
  ],
  declarations: [
    AdminComponent,
    AdminCreateComponent,
    AdminManageComponent,
    AdminEditComponent,
    AdminDetailsComponent
  ],
  providers: [
    AdminService,
    AdminDetailsGuardService,
    ViewAdminResolverService,
    ManageAdminResolverService,
    EditAdminResolverService,
    AdminProfileForCreateResolverService,
  ]
})
export class AdminModule { }
