import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms/src/directives/ng_form';

import { ToastrService } from 'ngx-toastr';
import { AdminCreateModel } from 'app/module/admin/shared/model/admin-create-model';
import { AdminService } from 'app/module/admin/shared/service/admin.service';
import { DateUtil } from 'app/module/core/util/date-util.ts';
import { OfficeService, Office } from 'app/module/core/service/office';
import { UserGroupService } from 'app/module/user-group/shared/service/user-group.service';
import { UserGroupDTO } from 'app/module/user-group/shared/model/user-group-dto';


@Component({
    selector: 'app-admin-create',
    templateUrl: './admin-create.component.html'
})
export class AdminCreateComponent implements OnInit {
    @ViewChild('createAdminForm')
    createAdminForm: any;

    admin: AdminCreateModel = new AdminCreateModel();
    adminUsername: string;
    adminProfileForCreate: Array<any> = [];
    serverErrorMessages: string[] = [];
    buttonPressed = false;
    maxDobDate: Date = DateUtil.getTodayDate();
    offices: Office[] = [];
    userGroups: UserGroupDTO[] = [];

    serviceGroups = [
        { name: "Admin", value: "ADMIN" },
        { name: "Officer", value: "OFFICER" }
    ]

    userTypes = [
        { name: "Master User", value: "S" },
        { name: "Super User", value: "Y" },
        { name: "Normal User", value: "N" }
    ]

    constructor(
        private location: Location,
        private adminService: AdminService,
        private toastr: ToastrService,
        private route: ActivatedRoute,
        public officeService: OfficeService,
        public userGroupService: UserGroupService
    ) { }

    ngOnInit() {
        this.addOffice();
        this.officeService.offices.subscribe((offices) => this.offices = offices);
        this.userGroupService.getAllUserGroup().subscribe((userGroups) => this.userGroups = userGroups);
    }

    addOffice() {
        this.admin.officeUserGroupList.push({
            officeId: null,
            userGroupId: null
        });
    }

    removeOffice(index: number) {
        if (index === 0 && this.admin.officeUserGroupList.length === 1) {
            return;
        }
        this.admin.officeUserGroupList.splice(index, 1);
    }

    create(form: NgForm) {
        this.buttonPressed = true;
        console.log("User Data::: ", this.admin);
        this.adminService
            .create(this.admin)
            .subscribe(response => {
                this.toastr.success(response.message);
                this.resetForm();
                this.buttonPressed = false;
            },
                errorResponse => {
                    this.buttonPressed = false;
                    if (errorResponse.error.message) {
                        this.toastr.error(errorResponse.error.message);
                    } else {
                        errorResponse.error.forEach(error => {
                            if (form.form.contains(error.fieldType)) {
                                form.controls[error.fieldType].setErrors({ server: true });
                                this.serverErrorMessages[error.fieldType] = error.message;
                            } else {
                                this.toastr.error(error.message);
                            }
                        });
                    }
                });
    }

    back() {
        this.location.back();
    }

    resetForm() {
        this.admin = new AdminCreateModel();
        this.admin.officeUserGroupList = [
            {
                officeId: null,
                userGroupId: null
            }
        ]
        this.createAdminForm.form.markAsPristine();
        this.createAdminForm.form.markAsUntouched();
    }


}
