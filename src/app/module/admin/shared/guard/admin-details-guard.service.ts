import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AdminService } from 'app/module/admin/shared/service/admin.service';
import { AppRoutes } from 'app/constants/app-routes';

@Injectable()
export class AdminDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.adminService.getSelectedForAdminDetail() == null) {
      this.router.navigate([AppRoutes.ADMIN]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private adminService: AdminService,
    public router: Router
  ) { }

}
