import { Status } from 'app/module/core/models/status';
import { ConvergentAdminDto } from 'app/module/admin/shared/model/convergent-admin-dto-model';
import { AdminProfileDto } from 'app/module/admin-profile/shared/model/admin-profile-dto-model';

export class ConvergentAdminLoginDto {

  adminProfileDto: AdminProfileDto;
  convergentAdminDto: ConvergentAdminDto;
  attemptCount: number;
  id: number;
  isLoggedIn: boolean;
  lastLoggedInDate: Date;
  lastPasswordChangeDate: Date;
  statusDto: Status;
  username: string;
  serviceGroup: string;
  isSuper: string;
  enableTwoFactorAuthentication: boolean;
}
