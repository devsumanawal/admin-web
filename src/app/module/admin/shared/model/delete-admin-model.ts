export class AdminDeleteModel {
  id: number;
  remarks: string;
  constructor(id: number, remarks: string) {
    this.id = id;
    this.remarks = remarks;

  }
}
