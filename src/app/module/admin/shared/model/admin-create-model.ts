export class AdminCreateModel {
    id?: number;
    name: string;
    username: string;
    password: string;
    email: string;
    contactNumber: string;
    address: string;
    serviceGroup: string;
    isSuper: string;
    officeUserGroupList: {
        officeId: number,
        userGroupId: number
    }[] = [];
}