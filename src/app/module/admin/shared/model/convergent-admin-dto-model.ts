import { Status } from 'app/module/core/models/status';
import { ConvergentAdminLoginDto } from 'app/module/admin/shared/model/convergent-admin-login-dto';

export class ConvergentAdminDto {

  id: number;
  profilePhotoUrl: string;
  name: string;
  email: string;
  address: string;
  contact: string;
  createdDate: Date;
  modifiedDate: Date;
  createdBy: ConvergentAdminLoginDto;
  modifiedBy: ConvergentAdminLoginDto;
  remarks: string;
  statusDto: Status;
  dob: Date;
  gender: string;
}
