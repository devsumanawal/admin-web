export class AdminEditModel {
    name: string;
    username: string;
    password: string;
    email: string;
    contactNumber: string;
    address: string;
    serviceGroup: string;
    isSuper: string;
    status: string;
    officeUserGroupList: {
        officeId: string;
        userGroupId: string;
    }[];
}
