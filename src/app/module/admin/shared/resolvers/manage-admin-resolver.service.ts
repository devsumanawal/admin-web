import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { AdminService } from 'app/module/admin/shared/service/admin.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ManageAdminResolverService implements Resolve<any> {
  constructor(
    private adminService: AdminService,
    private toastr: ToastrService
  ) { }

  resolve() {
    return this.adminService.getAllAdminsForManage().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
