import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiConstants } from 'app/constants/api-constants';
import { AdminCreateModel } from 'app/module/admin/shared/model/admin-create-model';
import { AdminEditModel } from 'app/module/admin/shared/model/admin-edit-model';
import { ConvergentAdminLoginDto } from 'app/module/admin/shared/model/convergent-admin-login-dto';
import { BackendResponse } from 'app/module/core/models/backend-response';


@Injectable()
export class AdminService {

    selectedForAdminDetail: ConvergentAdminLoginDto;

    constructor(private http: HttpClient) { }

    getAllAdmins() {
        return this.http.get<any[]>(ApiConstants.ENDPOINT + ApiConstants.ADMIN);
    }

    searchAllAdmins(searchParam) {
        return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.SEARCH, searchParam);
    }

    searchAdminsForManage(searchParam) {
        return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.SEARCH, searchParam);
    }

    getAllAdminsForManage() {
        return this.http.get(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.MANAGE);
    }

    create(adminCreateModel: AdminCreateModel) {
        return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.ADMIN, adminCreateModel);
    }

    edit(adminEditModel: AdminEditModel, id) {
        return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.MODIFY + '/' + id, adminEditModel);
    }

    deleteAdmin(deleteAdminModel) {;
        return this.http.put<BackendResponse>
            (ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.MANAGE + ApiConstants.DELETE, deleteAdminModel);
    }

    blockAdmin(blockAdminModel) {
        return this.http.put<BackendResponse>
            (ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.MANAGE + ApiConstants.BLOCK, blockAdminModel);
    }

    unBlockAdmin(unBlockAdminModel) {
        return this.http.put<BackendResponse>
            (ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.MANAGE + ApiConstants.UNBLOCK, unBlockAdminModel);
    }

    resetAdminPassword(passwordResetModal) {
        return this.http.put<BackendResponse>
            (ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.MANAGE + ApiConstants.RESET_PASSWORD, passwordResetModal);
    }

    resetAdminTOTP(manageAdminModal) {
        return this.http.put<BackendResponse>
            (ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.MANAGE + ApiConstants.RESET_TOTP, manageAdminModal);
    }

    getAdminById(id) {
        return this.http.get<AdminCreateModel>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + '/' + id);
    }

    setSelectedForAdminDetail(selectedForAdminDetail: ConvergentAdminLoginDto) {
        this.selectedForAdminDetail = selectedForAdminDetail;
    }

    getSelectedForAdminDetail(): ConvergentAdminLoginDto {
        return this.selectedForAdminDetail;
    }
}
