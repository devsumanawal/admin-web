import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminDetailsComponent } from './admin-details.component';

const adminDetailsRoutes: Routes = [
  {
    path: '', component: AdminDetailsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminDetailsRoutes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AdminDetailRoutingModule { }
