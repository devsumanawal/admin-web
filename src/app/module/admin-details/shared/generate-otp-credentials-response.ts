export class GenerateOtpCredentialsResponse {
    qrFormatedValue: string;
    secretKey: string;
    status: string;
    statusCode: string;
    success: boolean;
}