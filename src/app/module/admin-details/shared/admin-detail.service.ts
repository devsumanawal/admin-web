import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from '../../../constants/api-constants';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';
import { AdminDetails } from 'app/module/admin-details/shared/admin-details';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { ToastrService } from 'ngx-toastr';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { GenerateOtpCredentialsResponse } from './generate-otp-credentials-response';
import { EnableOtpRequest } from './enable-otp-request';

@Injectable()
export class AdminDetailService {

    adminDetails: AdminDetails = new AdminDetails();
    adminDetailsRxSubject: Subject<AdminDetails> = new BehaviorSubject<AdminDetails>(this.adminDetails);

    constructor(
        private http: HttpClient,
        private toastr: ToastrService
    ) {
    }

    syncAdminDetails() {
        this.adminDetailsRxSubject.next(
            this.adminDetails
        );
    }

    getAdminDetailsRxSubject() {
        return this.adminDetailsRxSubject.asObservable();
    }

    getDetailAndSyncAdminDetail() {
        this.http.get<AdminDetails>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.DETAIL)
            .subscribe(
                (success) => {
                    if (success !== null) {
                        this.adminDetails = success
                        this.syncAdminDetails();
                    }
                }, (error) => {
                    if (error.error.message === null) {
                        this.toastr.error(error.error.message)
                    }
                });
    }

    getPicture(): Observable<BackendResponse> {
        return this.http.get<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.ADMIN_PHOTO);
    }

    generateKeyForTwoFactorAuth(): Observable<GenerateOtpCredentialsResponse> {
        return this.http.post<GenerateOtpCredentialsResponse>(
            ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.ADMIN_AUTHENTICATION + ApiConstants.GENERATE_OTP,
            null
        );
    }

    enableTwoFactorAuth(enableOtpRequest: EnableOtpRequest): Observable<BackendResponse> {
        return this.http.post<BackendResponse>(
            ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.ADMIN_AUTHENTICATION + ApiConstants.ENABLE_OTP,
            enableOtpRequest
        );
    }

    disableTwoFactorAuth(enableOtpRequest: EnableOtpRequest): Observable<BackendResponse> {
        return this.http.post<BackendResponse>(
            ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.ADMIN_AUTHENTICATION + ApiConstants.DISABLE_OTP,
            enableOtpRequest
        );
    }

    uploadPicture(uploadImage: File) {
        const input = new FormData();
        input.append('photo', uploadImage);
        return this.http.put<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.ADMIN_PHOTO, input);
    }

    updateAdmin(updateForm) {
        return this.http.post<BackendResponse>(ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.ADMIN_DETAIL +
            ApiConstants.EDIT_ADMIN, updateForm);
    }
}
