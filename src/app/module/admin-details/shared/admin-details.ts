export class AdminDetails {
  name: string;
  userName: string;
  address: string;
  mobileNumber: string;
  email: string;
  merchantCode: string;
  imageURL: string;
  isSuper: string;
  enableTwoFactorAuthentication: boolean;

  // constructor(adminDetails?: AdminDetails) {
  //   if (adminDetails) {
  //     this.name = adminDetails.name;
  //     this.userName = adminDetails.userName;
  //     this.address = adminDetails.address;
  //     this.mobileNumber = adminDetails.mobileNumber;
  //     this.email = adminDetails.email;
  //     this.merchantCode = adminDetails.merchantCode;
  //     this.imageURL = adminDetails.imageURL;
  //   }
  // }

}
