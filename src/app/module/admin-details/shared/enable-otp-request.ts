export class EnableOtpRequest {
  secretKey: string;
  otpCode: number;
  qrFormatedValue: string;
}
