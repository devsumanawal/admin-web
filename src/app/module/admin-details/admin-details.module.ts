import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AdminDetailRoutingModule } from 'app/module/admin-details/admin-detail-routing.module';
import { AdminDetailsComponent } from 'app/module/admin-details/admin-details.component';
import { ChangePasswordModule } from 'app/module/change-password/change-password.module';
import { ActivityLogModule } from 'app/module/activity-log/activity-log.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminDetailRoutingModule,
    ChangePasswordModule,
    ActivityLogModule,
  ],
  declarations: [
    AdminDetailsComponent
  ]
})
export class AdminDetailsModule { }
