import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AdminDetails } from 'app/module/admin-details/shared/admin-details';
import { AdminDetailService } from 'app/module/admin-details/shared/admin-detail.service';
import { ChangePasswordComponent } from 'app/module/change-password/change-password.component';
import { Subscription } from 'rxjs/Subscription';
import { ActivityLogComponent } from 'app/module/activity-log/activity-log.component';
import { AppConstant } from 'app/constants/app-constant';
import { EnableOtpRequest } from './shared/enable-otp-request';

@Component({
  selector: 'app-admin-details',
  templateUrl: './admin-details.component.html',
  styleUrls: ['./admin-details.component.scss']
})
export class AdminDetailsComponent implements OnInit, OnDestroy {

  adminDetail: AdminDetails = new AdminDetails();
  enableOtpRequest: EnableOtpRequest = null;
  serverErrorMessages: string[] = [];
  isEdit = false;
  buttonPressed = false;
  adminDetailSubscription: Subscription;
  enableDisableTwoFactorAuthReady = false;

  @ViewChild('fileInput') fileInput;
  @ViewChild('changePasswordBlock') private changePasswordBlock: ChangePasswordComponent;
  @ViewChild('activityLogBlock') private activityLogBlock: ActivityLogComponent;

  constructor(private detailService: AdminDetailService,
    private adminDetailService: AdminDetailService,
    private toastr: ToastrService) {
  };

  ngOnInit() {
    this.loadAdminDetailsComponent();
    this.adminDetailSubscription = this.adminDetailService.getAdminDetailsRxSubject()
      .subscribe(adminDetail => {
        this.adminDetail = adminDetail;
      });
    this.adminDetailService.getDetailAndSyncAdminDetail();
  }

  ngOnDestroy() {
    this.adminDetailSubscription.unsubscribe();
  }

  adminDetailTabSelected(event) {
    if (!this.checkTabWasSelected(event)) {
      return;
    }
    this.loadAdminDetailsComponent();
    this.adminDetailService.getDetailAndSyncAdminDetail();
  }

  loadAdminDetailsComponent() {
    this.adminDetail = new AdminDetails();
    this.enableOtpRequest = new EnableOtpRequest();
    this.enableDisableTwoFactorAuthReady = false;
    this.serverErrorMessages = [];
    this.isEdit = false;
    this.buttonPressed = false;
  }

  changePasswordTabSelected(event) {
    if (!this.checkTabWasSelected(event)) {
      return;
    }
    this.changePasswordBlock.loadChangePasswordComponent();
  }

  activityLogTabSelected(event) {
    if (!this.checkTabWasSelected(event)) {
      return;
    }

    this.activityLogBlock.loadActivityLogComponent();
  }

  checkTabWasSelected(event): boolean {
    if (!event.tabset) {
      return false;
    } else {
      return true;
    }
  }

  uploadPicture() {
    const fi = this.fileInput.nativeElement;

    if (fi.files && fi.files[0]) {
      const fileToUpload = fi.files[0];
      const file = fileToUpload;

      if (file.size > AppConstant.MAX_FILE_SIZE_LIMIT_IN_BYTE) {
        this.toastr.error('Max File Upload Size is ' + AppConstant.MAX_FILE_SIZE_LIMIT_IN_MEGABYTE + ' MB');
        this.fileInput.nativeElement.value = '';
        return;
      }

      this.detailService
        .uploadPicture(file)
        .subscribe(res => {
          this.handleSuccess(res.message)
          this.adminDetailService.getDetailAndSyncAdminDetail();
        },
          error => this.handleError(error.error.message),
          () => this.fileInput.nativeElement.value = ''
        );
    }
  }

  updateAdmin(form: NgForm) {
    this.buttonPressed = true;
    this.detailService
      .updateAdmin(this.adminDetail)
      .subscribe((successResponse) => {
        this.buttonPressed = false;
        this.handleSuccess(successResponse.message);
        this.adminDetailService.getDetailAndSyncAdminDetail();
        this.isEdit = false;
      },
        (errorResponse) => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.handleError(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              form.controls[error.fieldType].setErrors({ server: true });
              this.serverErrorMessages[error.fieldType] = error.message;
            });
          }
        });
  }

  edit() {
    this.isEdit = true;
  }

  cancel() {
    this.isEdit = false;
    this.adminDetailService.getDetailAndSyncAdminDetail();
  }

  generateKeyForTwoFactorAuth() {
    this.enableOtpRequest = null;
    this.buttonPressed = true;
    this.adminDetailService.generateKeyForTwoFactorAuth()
      .subscribe((successResponse) => {
        this.buttonPressed = false;
        this.enableOtpRequest = new EnableOtpRequest();
        this.enableOtpRequest.qrFormatedValue = successResponse.qrFormatedValue;
        this.enableOtpRequest.secretKey = successResponse.secretKey;
        this.isEdit = false;
        this.enableDisableTwoFactorAuthReady = true;
      },
        (errorResponse) => {
          this.buttonPressed = false;
          this.enableOtpRequest = null;
          if (errorResponse.error.message) {
            this.handleError(errorResponse.error.message);
          }
        });
  }

  cancelGenerateKeyForTwoFactorAuth() {
    this.loadAdminDetailsComponent();
    this.adminDetailService.getDetailAndSyncAdminDetail();
  }

  disableTwoFactorAuthClicked() {
    this.enableOtpRequest = new EnableOtpRequest();
    this.enableDisableTwoFactorAuthReady = true;
  }

  enableTwoFactorAuth() {
    this.buttonPressed = true;
    this.adminDetailService.enableTwoFactorAuth(this.enableOtpRequest)
      .subscribe((successResponse) => {
        this.enableOtpRequest = new EnableOtpRequest();
        this.adminDetail = new AdminDetails();
        this.adminDetailService.getDetailAndSyncAdminDetail();
        this.enableDisableTwoFactorAuthReady = false;
        this.buttonPressed = false;
        this.handleSuccess(successResponse.message);
        this.isEdit = false;
      },
        (errorResponse) => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.handleError(errorResponse.error.message);
          }
        });
  }

  disableTwoFactorAuth() {
    this.buttonPressed = true;
    this.adminDetailService.disableTwoFactorAuth(this.enableOtpRequest)
      .subscribe((successResponse) => {
        this.enableOtpRequest = new EnableOtpRequest();
        this.adminDetail = new AdminDetails();
        this.adminDetailService.getDetailAndSyncAdminDetail();
        this.enableDisableTwoFactorAuthReady = false;
        this.buttonPressed = false;
        this.handleSuccess(successResponse.message);
        this.isEdit = false;
      },
        (errorResponse) => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.handleError(errorResponse.error.message);
          }
        });
  }

  private handleSuccess(success) {
    this.toastr.success(success)
  }

  private handleError(error) {
    this.toastr.error(error)
  }
}
