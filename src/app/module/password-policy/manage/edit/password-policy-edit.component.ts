import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { PasswordPolicyModel } from 'app/module/password-policy/shared/password-policy-model';
import { ModifyPasswordPolicyModel } from '../../shared/modify-password-policy-model';
import { PasswordPolicyService } from 'app/module/password-policy/shared/password-policy.service';

@Component({
  selector: 'app-password-policy-edit',
  templateUrl: './password-policy-edit.component.html',
  styleUrls: ['./password-policy-edit.component.scss']
})
export class PasswordPolicyEditComponent implements OnInit {
  passwordPolicyId: number;
  modifyPasswordPolicyModel: ModifyPasswordPolicyModel = new ModifyPasswordPolicyModel();
  serverErrorMessages: string[] = [];
  buttonPressed = false;

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute,
    private passwordPolicyService: PasswordPolicyService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.getPasswordPolicyForEdit();
    this.route.params.subscribe(params => {
      this.passwordPolicyId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    });
  }

  getPasswordPolicyForEdit() {
    this.mapPasswordPolicyModelToModify(this.route.snapshot.data.passwordPolicyModel);
  }

  mapPasswordPolicyModelToModify(passwordPolicyModel: PasswordPolicyModel) {
    this.modifyPasswordPolicyModel.label = passwordPolicyModel.label;
    this.modifyPasswordPolicyModel.passwordPolicyDesc = passwordPolicyModel.passwordPolicyDesc;
    this.modifyPasswordPolicyModel.passwordPolicyValue = +passwordPolicyModel.passwordPolicyValue;
    this.modifyPasswordPolicyModel.passwordRegularExpressionLengthType
      = passwordPolicyModel.passwordRegularExpressionLengthType;
    this.modifyPasswordPolicyModel.regularExpression = passwordPolicyModel.regularExpression;
  }


  edit(form: NgForm) {
    this.buttonPressed = true;
    this.passwordPolicyService
      .edit(this.modifyPasswordPolicyModel, this.passwordPolicyId)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.buttonPressed = false;
        this.location.back();
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (form.form.contains(error.fieldType)) {
                form.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  back() {
    this.location.back();
  }
}
