import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { PasswordPolicyModel } from 'app/module/password-policy/shared/password-policy-model';
import { PasswordPolicyService } from 'app/module/password-policy/shared/password-policy.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-password-policy-manage',
  templateUrl: './password-policy-manage.component.html',
  styleUrls: ['./password-policy-manage.component.scss']
})
export class PasswordPolicyManageComponent implements OnInit {

  passwordPolicies: Array<PasswordPolicyModel> = [];

  constructor(
    private passwordPolicyService: PasswordPolicyService,
    private toastr: ToastrService,
    private location: Location
  ) { }

  ngOnInit() {
    this.passwordPolicyService.getAllPasswordPolicies()
      .subscribe(passwordPolicies => {
        this.setPasswordPolicies(passwordPolicies);
      },
      error => {
        this.toastr.error(error.error.message);
      })
  }

  setPasswordPolicies(passwordPolicies: Array<PasswordPolicyModel>) {
    this.passwordPolicies = passwordPolicies;
  }

  goBack() {
    this.location.back();
  }
}
