import { Component, OnInit } from '@angular/core';

import { PasswordPolicyModel } from './shared/password-policy-model';
import { PasswordPolicyService } from './shared/password-policy.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-password-policy',
  templateUrl: './password-policy.component.html',
  styleUrls: ['./password-policy.component.scss']
})
export class PasswordPolicyComponent implements OnInit {

  passwordPolicies: Array<PasswordPolicyModel> = [];

  constructor(private passwordPolicyService: PasswordPolicyService) { }

  ngOnInit() {
  }

  setPasswordPolicies(passwordPolicies: Array<PasswordPolicyModel>) {
    this.passwordPolicies = passwordPolicies;
  }
}
