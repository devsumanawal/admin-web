import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'passwordPolicyToText'
})
export class PasswordPolicyToTextPipe implements PipeTransform {
  transform(passwordPolicy: any, args?: any): string {
    let responseText = '';
    if (+passwordPolicy.passwordPolicyValue === 0) {
      return responseText;
    }
    if (passwordPolicy.passwordRegularExpressionLengthType === 'MAX') {
      responseText =
        'At Most ' +
        passwordPolicy.passwordPolicyValue +
        ' ' +
        passwordPolicy.label;
    } else if (passwordPolicy.passwordRegularExpressionLengthType === 'MIN') {
      responseText =
        'At Least ' +
        passwordPolicy.passwordPolicyValue +
        ' ' +
        passwordPolicy.label;
    } else if (passwordPolicy.passwordRegularExpressionLengthType === 'FIX') {
      responseText =
        passwordPolicy.label + ' must be ' + passwordPolicy.passwordPolicyValue;
    } else if (passwordPolicy.passwordRegularExpressionLengthType === null) {
      responseText =
        passwordPolicy.label + ' is ' + passwordPolicy.passwordPolicyValue;
    }
    return responseText;
  }
}
