import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PasswordPolicyManageComponent } from 'app/module/password-policy/manage/password-policy-manage.component';
import { AppRoutes } from 'app/constants/app-routes';
import { EditPasswordPolicyResolverService } from 'app/module/password-policy/shared/resolvers/edit-password-policy-resolver-service';
import { PasswordPolicyEditComponent } from 'app/module/password-policy/manage/edit/password-policy-edit.component';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'Password Policy'
    },
    children: [
      {
        path: '',
        component: PasswordPolicyManageComponent,
        pathMatch: 'full'
      },
      {
        path: AppRoutes.EDIT_ID,
        component: PasswordPolicyEditComponent,
        pathMatch: 'full',
        resolve:
          {
            passwordPolicyModel: EditPasswordPolicyResolverService,
          }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class PasswordPolicyRoutingModule { }
