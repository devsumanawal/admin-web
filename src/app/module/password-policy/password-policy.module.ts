import { NgModule } from '@angular/core';

import { PasswordPolicyComponent } from './password-policy.component';
import { PasswordPolicyManageComponent } from 'app/module/password-policy/manage/password-policy-manage.component';
import { PasswordPolicyService } from './shared/password-policy.service';
import { PasswordPolicyToTextPipe } from './pipes/password-policy-to-text.pipe';
import { SharedModule } from 'app/module/shared/shared.module';
import { CommonModule } from '@angular/common';
import { PasswordPolicyRoutingModule } from 'app/module/password-policy/password-policy-routing.module';
import { PasswordPolicyEditComponent } from 'app/module/password-policy/manage/edit/password-policy-edit.component';
import { EditPasswordPolicyResolverService } from 'app/module/password-policy/shared/resolvers/edit-password-policy-resolver-service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PasswordPolicyRoutingModule
  ],
  declarations: [
    PasswordPolicyComponent,
    PasswordPolicyManageComponent,
    PasswordPolicyEditComponent,
    PasswordPolicyToTextPipe
  ],
  providers: [
    PasswordPolicyService,
    PasswordPolicyToTextPipe,
    EditPasswordPolicyResolverService
  ],
  exports: [
    PasswordPolicyComponent,
    PasswordPolicyToTextPipe
  ]
})
export class PasswordPolicyModule { }
