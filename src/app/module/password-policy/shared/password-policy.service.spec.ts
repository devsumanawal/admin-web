import { TestBed, inject } from '@angular/core/testing';

import { PasswordPolicyService } from './password-policy.service';

describe('PasswordPolicyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PasswordPolicyService]
    });
  });

  it('should be created', inject([PasswordPolicyService], (service: PasswordPolicyService) => {
    expect(service).toBeTruthy();
  }));
});
