export class PasswordPolicyModel {
  id: number;
  label: string;
  passwordPolicyDesc: string;
  passwordPolicyValue: number;
  passwordRegularExpressionLengthType: string;
  regularExpressionToRegex?: RegExp;
  regularExpression: string;
}
