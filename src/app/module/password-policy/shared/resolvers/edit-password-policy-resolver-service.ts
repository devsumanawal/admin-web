import { Injectable } from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, Router} from '@angular/router';
import { PasswordPolicyService } from 'app/module/password-policy/shared/password-policy.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import {AppRoutes} from '../../../../constants/app-routes';

@Injectable()
export class EditPasswordPolicyResolverService implements Resolve<Observable<any>> {
  constructor(
    private passwordPolicyService: PasswordPolicyService,
    private toastr: ToastrService,
    private ngroute: Router,
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.passwordPolicyService.getPasswordPolicyById(route.paramMap.get('id')).catch((error) => {
      this.ngroute.navigate([AppRoutes.MANAGE_PASSWORD_POLICY]);
      return Observable.empty();
    });
  }
}
