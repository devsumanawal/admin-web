export class ModifyPasswordPolicyModel {
  label: string;
  passwordPolicyDesc: string;
  regularExpression: string;
  passwordPolicyValue: number;
  passwordRegularExpressionLengthType: string;
}
