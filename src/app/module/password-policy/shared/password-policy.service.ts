import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ApiConstants } from 'app/constants/api-constants';
import { PasswordPolicyModel } from './password-policy-model';
import { Observable } from 'rxjs/Observable';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { ModifyPasswordPolicyModel } from 'app/module/password-policy/shared/modify-password-policy-model';

@Injectable()
export class PasswordPolicyService {
  constructor(private http: HttpClient) { }

  getAllPasswordPolicies(): Observable<PasswordPolicyModel[]> {
    return this.http.get<PasswordPolicyModel[]>(
      ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.PASSWORD_POLICY
    );
  }

  getPasswordPolicyById(passwordPolicyId) {
    return this.http.get<PasswordPolicyModel>(
      ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.PASSWORD_POLICY + '/' + passwordPolicyId
    );
  }

  edit(modifyPasswordPolicyModel: ModifyPasswordPolicyModel, passwordPolicyId) {
    return this.http.put<BackendResponse>(
      ApiConstants.ENDPOINT + ApiConstants.ADMIN + ApiConstants.PASSWORD_POLICY + '/' + passwordPolicyId, modifyPasswordPolicyModel
    );
  }

}
