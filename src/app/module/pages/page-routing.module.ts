import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { P404Component } from 'app/module/pages/p404.component';
import { P500Component } from 'app/module/pages/p500.component';
import { P403Component } from 'app/module/pages/p403.component';

const routeConfig = [
  {
    path: '',
    children: [
      {
        path: AppRoutes.notFound,
        component: P404Component
      },
      {
        path: AppRoutes.serverError,
        component: P500Component
      },
      {
        path: AppRoutes.forbidden,
        component: P403Component
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class PageRoutingModule { }
