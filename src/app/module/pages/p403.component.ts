import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-p403',
  template: `
    <p>
    Forbidden Error 403
    </p>
  `,
  styles: []
})
export class P403Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
