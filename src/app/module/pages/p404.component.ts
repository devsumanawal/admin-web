import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-p404',
  template: `
    <p>
    Not Found 404
    </p>
    <style>
      .page-not-found{
        width: 100px;
        height: 200px;
        background: red;
      }
    </style>
    <div class="page-not-found"></div>
  `,
  styles: []
})
export class P404Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
