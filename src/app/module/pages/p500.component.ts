import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-p500',
  template: `
    <div class="page-not-found text-center">
      <div class="page-not-found-content">
        <div class="brand text-center">
          <img src="../assets/img/brand-light.svg" onerror="this.onerror=null; this.src='../assets/img/brand-light.png'">
        </div>
        <h1>404</h1>
        <h6>Page Not Found!</h6>
        <p>Looks like you are lost!</p>
        <button (click)="goBack()" class="btn">Go Back</button>
      </div>
      <div class="background-scroll"></div>
    </div>
  `,
  styles: []
})
export class P500Component implements OnInit {

  constructor(
    private location: Location,
  ) { }

  ngOnInit() {
  }
  goBack() {
    this.location.back();
  }

}
