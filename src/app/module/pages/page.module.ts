import { NgModule } from '@angular/core';

import { SharedModule } from 'app/module/shared/shared.module';
import { PageRoutingModule } from './/page-routing.module';
import { P404Component } from './p404.component';
import { P500Component } from './p500.component';
import { P403Component } from './p403.component';

@NgModule({
  imports: [
    SharedModule,
    PageRoutingModule
  ],
  declarations: [P404Component, P500Component, P403Component]
})
export class PageModule { }
