import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { CreateAdminProfile } from 'app/module/admin-profile/shared/model/create-admin-profile';
import { TreeviewItem } from 'ngx-treeview';
import { TreeViewUtilityService } from 'app/module/core/util/tree-view/tree-view-utility.service';
import { AdminProfileDto } from 'app/module/admin-profile/shared/model/admin-profile-dto-model';
import { ProfileRoleViewModel } from 'app/module/core/models/profile-role-view-model';
import { CreateProfileFeatures } from 'app/module/admin-profile/shared/model/create-profile-features';
import {AppRoutes} from '../../../../constants/app-routes';

@Component({
  selector: 'app-admin-profile-edit',
  templateUrl: './admin-profile-edit.component.html',
  styleUrls: ['./admin-profile-edit.component.scss']
})
export class AdminProfileEditComponent implements OnInit, OnDestroy {

  previouslySelectedRoleExceptViewRole = new Map<number, TreeviewItem[]>();

  treeViewRoles: TreeviewItem[] = [];
  selectedRoles: number[] = [];
  isAnyRoleChecked: boolean;
  availableRoles: any[] = [];
  availableRolesIdList: number[] = [];

  editAdminProfile: CreateAdminProfile = new CreateAdminProfile();
  adminProfileId: number;
  @ViewChild('editAdminProfileForm') editAdminProfileForm: NgForm;
  serverErrorMessages: string[] = [];
  buttonPressed = false;
  firstAlreadyLoaded: boolean = false;

  treeViewConfig = {
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: false,
    decoupleChildFromParent: false
  };

  constructor(
    private location: Location,
    private ngRoute: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private adminProfileService: AdminProfileService,
    private treeViewUtilityService: TreeViewUtilityService
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.ADMIN_PROFILE]);
      }

      this.firstAlreadyLoaded = true;
      this.adminProfileId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    });

    this.availableRoles = this.activatedRoute.snapshot.data.profileResponse;
    this.setEditAdminProfile();


    this.getTreeView(this.activatedRoute.snapshot.data.profileResponse);
  }

  getTreeView(roles: ProfileRoleViewModel[]) {
    this.treeViewRoles = [];
    this.treeViewRoles = this.treeViewUtilityService.getTreeViewFromRoles(roles);
    this.getAllAvailableRolesId();
  }

  getAllAvailableRolesId() {
    this.availableRoles.forEach(role => {
      this.availableRolesIdList.push(role.id);
      this.getAllAvailableRolesIdForChildRole(role);
    });
  }

  getAllAvailableRolesIdForChildRole(role) {
    if (role.childRoles) {
      if (role.childRoles.length > 0) {
        role.childRoles.forEach(childRole => {
          this.availableRolesIdList.push(childRole.id);

          if (childRole.childRoles) {
            this.getAllAvailableRolesIdForChildRole(childRole);
          }
        });
      }
    }
  }

  ngOnDestroy() {
    this.adminProfileService.setSelectedAdminProfile(null);
  }

  setEditAdminProfile() {
    const adminProfileDto: AdminProfileDto = this.adminProfileService.getSelectedAdminProfile();
    this.editAdminProfile.name = adminProfileDto.name;
    this.editAdminProfile.description = adminProfileDto.description;
  }

  addRoleOnTreeViewSelectChange(event: any) {

    const selectedRolesValueFromEvent: number[] =
      event.map(e => {
        return e.item.value
      });

    this.treeViewUtilityService.addOrRemoveViewRole(this.treeViewRoles, selectedRolesValueFromEvent,
      this.previouslySelectedRoleExceptViewRole);

    this.selectedRoles = this.treeViewUtilityService.getTreeviewCheckedValues(this.treeViewRoles);

    this.previouslySelectedRoleExceptViewRole = new Map<number, TreeviewItem[]>();
    event.forEach(
      selectedItem => {
        if (!selectedItem.item.text.toLowerCase().startsWith('view')) {
          const selectedItemParent: number = this.treeViewUtilityService.findSecondLastSuperParentValueOfDownlineTreeviewItem(selectedItem);
          let previouslySelectedRoleOfParentRole = this.previouslySelectedRoleExceptViewRole.get(selectedItemParent);
          if (previouslySelectedRoleOfParentRole === undefined) {
            previouslySelectedRoleOfParentRole = [];
          }
          previouslySelectedRoleOfParentRole.push(selectedItem.item);
          this.previouslySelectedRoleExceptViewRole.set(selectedItemParent, previouslySelectedRoleOfParentRole);
        }
      });

    event.forEach(
      selectedItem => {
        if (selectedItem.item.text.toLowerCase().startsWith('view')) {
          const selectedItemParent: number = this.treeViewUtilityService.findSecondLastSuperParentValueOfDownlineTreeviewItem(selectedItem);
          let previouslySelectedRoleOfParentRole = this.previouslySelectedRoleExceptViewRole.get(selectedItemParent);
          if (previouslySelectedRoleOfParentRole === undefined) {
            previouslySelectedRoleOfParentRole = [];
          }

          if (previouslySelectedRoleOfParentRole.length === 0) {
            selectedItem.item.disabled = false;
          }
        }
      });

    this.isAnyRoleChecked = false;

    if (this.selectedRoles.length > 0) {
      this.isAnyRoleChecked = true;
    }
  }

  submitEditAdminProfile() {

    this.buttonPressed = true;
    this.selectedRoles = this.selectedRoles.concat(
      this.treeViewUtilityService.addParentRolesId(
        this.treeViewRoles,
        this.selectedRoles
      )
    );

    this.getProfileFeaturesForCreate();

    this.adminProfileService
      .edit(this.adminProfileId, this.editAdminProfile)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.back();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.editAdminProfileForm.form.contains(error.fieldType)) {
                this.editAdminProfileForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  getProfileFeaturesForCreate() {
    this.editAdminProfile.createProfileFeatures = [];
    this.availableRolesIdList.forEach((roleId: number) => {
      if (this.selectedRoles.includes(roleId)) {
        this.editAdminProfile.createProfileFeatures.push(
          new CreateProfileFeatures(+roleId, true)
        );
      } else {
        this.editAdminProfile.createProfileFeatures.push(
          new CreateProfileFeatures(+roleId, false)
        );
      }
    });
  }

  back() {
    this.location.back();
  }
}
