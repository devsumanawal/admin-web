import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective, PaginationComponent } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { AdminProfileDto } from 'app/module/admin-profile/shared/model/admin-profile-dto-model';
import { AppRoutes } from 'app/constants/app-routes';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from 'app/module/core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from 'app/module/core/models/navigation-role-response-model';

@Component({
  selector: 'app-admin-profile-manage',
  templateUrl: './admin-profile-manage.component.html',
  styleUrls: ['./admin-profile-manage.component.scss']
})
export class AdminProfileManageComponent implements OnInit {
  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
  adminProfiles: Array<AdminProfileDto> = [];

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private location: Location,
    private toastr: ToastrService,
    private router: Router,
    private adminProfileService: AdminProfileService,
    private route: ActivatedRoute,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.resetSearch();
    this.adminProfileService.setSelectedAdminProfile(null);
  }

  resetSearch() {
    this.filterParam = [
      { key: 'name', value: '', label: 'Name', alias: 'Name' },
    ];
    this.searchUserProfileOnUserInput();
  }

  searchUserProfileOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllAdminProfileForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }



  getAllAdminProfileForSearch(event?: PageChangedEvent) {

    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }
    this.searchParam = {
      search: this.filterParam,
      page: this.page,
      size: this.size,
    };

    this.adminProfileService
      .searchAdminProfiles(this.searchParam)
      .subscribe(successResponse => {
        this.adminProfiles = successResponse.object;
        this.totalItems = successResponse.totalCount;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
        });
    this.searchButtonpressed = false;
  }

  clickViewDetailButton(adminProfileDto: AdminProfileDto) {
    this.adminProfileService.setSelectedAdminProfile(adminProfileDto);
    this.router.navigate([AppRoutes.ADMIN_PROFILE, AppRoutes.DETAILS, adminProfileDto.id]);
  }

  clickedEditAdminProfile(adminProfileDto: AdminProfileDto) {
    this.adminProfileService.setSelectedAdminProfile(adminProfileDto);
    this.router.navigate([AppRoutes.ADMIN_PROFILE, AppRoutes.MANAGE, AppRoutes.EDIT, adminProfileDto.id]);
  }

  deleteAdminProfile(event) {
    this.adminProfileService.deleteAdminProfile(event).subscribe(
      (success) => {
        this.getAllAdminProfileForSearch();
        this.toastr.success(success.message)
      },
      (errorResponse) => this.handleErrorResponse(errorResponse)
    )
  }

  blockAdminProfile(event) {
    this.adminProfileService.blockAdminProfile(event).subscribe(
      (success) => {
        this.getAllAdminProfileForSearch();
        this.toastr.success(success.message)
      },
      (errorResponse) => this.handleErrorResponse(errorResponse)
    )
  }

  unBlockAdminProfile(event) {
    this.adminProfileService.unBlockAdminProfile(event).subscribe(
      (success) => {
        this.getAllAdminProfileForSearch();
        this.toastr.success(success.message)
      },
      (errorResponse) => this.handleErrorResponse(errorResponse)
    )
  }

  private handleErrorResponse(errorResponse) {
    if (errorResponse.error.message) {
      this.toastr.error((errorResponse.error.message));
    } else {
      errorResponse.error.forEach(error => {
        this.toastr.error((error.message));
      });
    }
  }

  goBack() {
    this.location.back();
  }

  isActive(statusDescription: string) {
    return statusDescription === "Y";
  }

  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRoleOfChildRole('Profile Setup', 'Setup Admin Profile', 'Manage Admin Profile', childRoleName);
  }

}
