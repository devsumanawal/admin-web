import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { AdminCreateModel } from 'app/module/admin/shared/model/admin-create-model';
import { AdminService } from 'app/module/admin/shared/service/admin.service';
import { AdminDetailService } from 'app/module/admin-details/shared/admin-detail.service';
import { DateUtil } from 'app/module/core/util/date-util.ts';
import { CreateAdminProfile } from 'app/module/admin-profile/shared/model/create-admin-profile';
import { NgForm } from '@angular/forms';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { CreateProfileFeatures } from '../shared/model/create-profile-features';
import { TreeviewItem, DownlineTreeviewItem, TreeviewComponent } from 'ngx-treeview';
import { TreeViewUtilityService } from 'app/module/core/util/tree-view/tree-view-utility.service';
import { ProfileRoleViewModel } from 'app/module/core/models/profile-role-view-model';

@Component({
  selector: 'app-admin-profile-create',
  templateUrl: './admin-profile-create.component.html',
  styleUrls: ['./admin-profile-create.component.scss']
})
export class AdminProfileCreateComponent implements OnInit {

  previouslySelectedRoleExceptViewRole = new Map<number, TreeviewItem[]>();

  treeViewRoles: TreeviewItem[] = [];
  selectedRoles: number[] = [];
  isAnyRoleChecked: boolean;
  availableRoles: any[] = [];
  availableRolesIdList: number[] = [];

  createAdminProfile: CreateAdminProfile = new CreateAdminProfile();
  @ViewChild('createAdminProfileForm') createAdminProfileForm: NgForm;
  serverErrorMessages: string[] = [];
  buttonPressed = false;

  treeViewConfig = {
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: false,
    decoupleChildFromParent: false
  };

  constructor(
    private location: Location,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private adminProfileService: AdminProfileService,
    private treeViewUtilityService: TreeViewUtilityService
  ) {
  }

  ngOnInit() {
    this.availableRoles = this.route.snapshot.data.allAdminRoles;
    this.getTreeView(this.route.snapshot.data.allAdminRoles);
  }

  getTreeView(roles: ProfileRoleViewModel[]) {
    this.treeViewRoles = [];
    this.treeViewRoles = this.treeViewUtilityService.getTreeViewFromRoles(roles);
    this.getAllAvailableRolesId();
  }

  getAllAvailableRolesId() {
    this.availableRoles.forEach(role => {
      this.availableRolesIdList.push(role.id);
      this.getAllAvailableRolesIdForChildRole(role);
    });
  }

  getAllAvailableRolesIdForChildRole(role) {
    if (role.childRoles) {
      if (role.childRoles.length > 0) {
        role.childRoles.forEach(childRole => {
          this.availableRolesIdList.push(childRole.id);

          if (childRole.childRoles) {
            this.getAllAvailableRolesIdForChildRole(childRole);
          }
        });
      }
    }
  }

  addRoleOnTreeViewSelectChange(event: DownlineTreeviewItem[]) {

    const selectedRolesValueFromEvent: number[] =
      event.map(e => {
        return e.item.value
      });

    this.treeViewUtilityService.addOrRemoveViewRole(this.treeViewRoles, selectedRolesValueFromEvent,
      this.previouslySelectedRoleExceptViewRole);

    this.selectedRoles = this.treeViewUtilityService.getTreeviewCheckedValues(this.treeViewRoles);

    this.previouslySelectedRoleExceptViewRole = new Map<number, TreeviewItem[]>();
    event.forEach(
      selectedItem => {
        if (!selectedItem.item.text.toLowerCase().startsWith('view')) {
          const selectedItemParent: number = this.treeViewUtilityService.findSecondLastSuperParentValueOfDownlineTreeviewItem(selectedItem);
          let previouslySelectedRoleOfParentRole = this.previouslySelectedRoleExceptViewRole.get(selectedItemParent);
          if (previouslySelectedRoleOfParentRole === undefined) {
            previouslySelectedRoleOfParentRole = [];
          }
          previouslySelectedRoleOfParentRole.push(selectedItem.item);
          this.previouslySelectedRoleExceptViewRole.set(selectedItemParent, previouslySelectedRoleOfParentRole);
        }
      });

    event.forEach(
      selectedItem => {
        if (selectedItem.item.text.toLowerCase().startsWith('view')) {
          const selectedItemParent: number = this.treeViewUtilityService.findSecondLastSuperParentValueOfDownlineTreeviewItem(selectedItem);
          let previouslySelectedRoleOfParentRole = this.previouslySelectedRoleExceptViewRole.get(selectedItemParent);
          if (previouslySelectedRoleOfParentRole === undefined) {
            previouslySelectedRoleOfParentRole = [];
          }

          if (previouslySelectedRoleOfParentRole.length === 0) {
            selectedItem.item.disabled = false;
          }
        }
      });

    this.isAnyRoleChecked = false;

    if (this.selectedRoles.length > 0) {
      this.isAnyRoleChecked = true;
    }
  }

  submitCreateAdminProfile() {
    this.buttonPressed = true;
    this.selectedRoles = this.selectedRoles.concat(
      this.treeViewUtilityService.addParentRolesId(
        this.treeViewRoles,
        this.selectedRoles
      )
    );

    this.getProfileFeaturesForCreate();

    this.adminProfileService
      .create(this.createAdminProfile)
      .subscribe(response => {
        this.toastr.success(response.message);
        this.resetForm();
        this.buttonPressed = false;
      },
        errorResponse => {
          this.buttonPressed = false;
          if (errorResponse.error.message) {
            this.toastr.error(errorResponse.error.message);
          } else {
            errorResponse.error.forEach(error => {
              if (this.createAdminProfileForm.form.contains(error.fieldType)) {
                this.createAdminProfileForm.controls[error.fieldType].setErrors({ server: true });
                this.serverErrorMessages[error.fieldType] = error.message;
              } else {
                this.toastr.error(error.message);
              }
            });
          }
        });
  }

  getProfileFeaturesForCreate() {
    this.createAdminProfile.createProfileFeatures = [];
    this.availableRolesIdList.forEach((roleId: number) => {
      if (this.selectedRoles.includes(roleId)) {
        this.createAdminProfile.createProfileFeatures.push(
          new CreateProfileFeatures(+roleId, true)
        );
      } else {
        this.createAdminProfile.createProfileFeatures.push(
          new CreateProfileFeatures(+roleId, false)
        );
      }
    });
  }

  resetForm() {
    this.createAdminProfile = new CreateAdminProfile();
    this.createAdminProfileForm.form.reset();
    this.getTreeView(this.route.snapshot.data.allAdminRoles);
  }

  back() {
    this.location.back();
  }

}
