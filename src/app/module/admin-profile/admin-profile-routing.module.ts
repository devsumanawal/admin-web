import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AdminProfileComponent } from 'app/module/admin-profile/admin-profile.component';
import { ViewAdminProfileResolverService } from 'app/module/admin-profile/shared/resolvers/view-admin-profile-resolver.service';
import { AppRoutes } from 'app/constants/app-routes';
import { AdminProfileDetailResolverService } from 'app/module/admin-profile/shared/resolvers/admin-profile-detail-resolver.service';
import { AdminProfileDetailsComponent } from 'app/module/admin-profile/details/admin-profile-details.component';
import { AdminProfileCreateComponent } from './create/admin-profile-create.component';
import { AdminProfileManageComponent } from 'app/module/admin-profile/manage/admin-profile-manage.component';
import { AdminProfileEditComponent } from 'app/module/admin-profile/manage/edit/admin-profile-edit.component';
import { ManageAdminProfileResolverService } from 'app/module/admin-profile/shared/resolvers/manage-admin-profile-resolver.service';
import { AdminProfileDetailsGuardService } from 'app/module/admin-profile/shared/guard/admin-profile-details-guard.service';
import { CreateAdminProfileResolverService } from 'app/module/admin-profile/shared/resolvers/create-admin-profile-resolver.service';

const routeConfig = [
  {
    path: '',
    data: {
      title: 'Admin Profile'
    },
    children: [
      {
        path: '',
        component: AdminProfileManageComponent,
        pathMatch: 'full',
        resolve: {

        }
      },
      {
        path: AppRoutes.DETAILS_ID,
        component: AdminProfileDetailsComponent,
        pathMatch: 'full',
        resolve: {
          profileResponse: AdminProfileDetailResolverService
        },
        canActivate: [
          AdminProfileDetailsGuardService
        ]
      },
      {
        path: AppRoutes.CREATE,
        component: AdminProfileCreateComponent,
        pathMatch: 'full',
        resolve: {
          allAdminRoles: CreateAdminProfileResolverService
        }
      },
      {
        path: AppRoutes.MANAGE,
        children: [
          {
            path: '',
            component: AdminProfileManageComponent,
            pathMatch: 'full',
            resolve: {

            }
          },
          {
            path: AppRoutes.EDIT_ID,
            component: AdminProfileEditComponent,
            pathMatch: 'full',
            resolve: {
              profileResponse: AdminProfileDetailResolverService
            },
            canActivate: [
              AdminProfileDetailsGuardService
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routeConfig)],
  declarations: [],
  exports: [RouterModule]
})
export class AdminProfileRoutingModule { }
