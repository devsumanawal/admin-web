import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { AdminProfileDto } from 'app/module/admin-profile/shared/model/admin-profile-dto-model';
import { AppRoutes } from 'app/constants/app-routes';
import { PaginationComponent } from 'ngx-bootstrap';
import { SearchParam } from 'app/module/core/models/search-param';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/pagination.component';
import { PaginationConstant } from 'app/constants/pagination-constants';
import { SearchRoleViewConstants } from 'app/constants/search-role-view-constants';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { NavigationService } from '../core/service/navigation-service/navigation-service';
import { NavigationRoleResponseModel } from '../core/models/navigation-role-response-model';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {

  @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;

  adminProfiles: Array<AdminProfileDto> = [];
  showDetails = false;

  filterParam: any;
  searchParam = new SearchParam();
  pageEvent: PageChangedEvent;
  page = PaginationConstant.page;
  size = PaginationConstant.size;
  totalItems: number;
  searchButtonpressed = false;
  statusListForSearch = StatusDescriptionConstants.getStatusForFilter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private adminProfileService: AdminProfileService,
    private toastr: ToastrService,
    public navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.resetSearch();
    this.adminProfileService.setSelectedAdminProfile(null);
  }


  resetSearch() {
    this.filterParam = [
      { key: 'status.name', value: '', condition: '=' },
      { key: 'name', value: '' }
    ];
    this.searchAdminProfileOnUserInput();
  }

  searchAdminProfileOnUserInput() {
    if (this.paginationComponent.page === 1) {
      this.getAllAdminProfileForSearch(null);
    } else {
      this.paginationComponent.selectPage(1);
    }
  }

  getAllAdminProfileForSearch(event?: PageChangedEvent) {
    this.searchButtonpressed = true;
    this.page = 1;
    if (event) {
      this.pageEvent = event;
      this.page = event.page;
      this.size = event.itemsPerPage;
    }
    
    this.adminProfileService
      .searchAdminProfiles(this.searchParam)
      .subscribe(successResponse => {
        this.adminProfiles = successResponse.object;
        this.totalItems = successResponse.totalCount;
        this.searchButtonpressed = false;
      },
        errorResponse => {
          this.toastr.error(errorResponse.error.message);
          this.searchButtonpressed = false;
        });
  }

  clickViewDetailButton(adminProfile: AdminProfileDto) {
    this.adminProfileService.setSelectedAdminProfile(adminProfile);
    this.router.navigate([AppRoutes.ADMIN_PROFILE, AppRoutes.DETAILS, adminProfile.id]);
  }

  isActive(statusDescription: string) {
    return StatusDescriptionConstants.isActive(statusDescription);
  }

  getChildRole(childRoleName: string): NavigationRoleResponseModel {
    return this.navigationService.getChildRole('Profile Setup', 'Setup Admin Profile', childRoleName);
  }
}

