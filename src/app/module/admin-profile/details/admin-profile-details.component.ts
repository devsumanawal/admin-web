import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AdminProfileDto } from 'app/module/admin-profile/shared/model/admin-profile-dto-model';
import { ProfileResponse } from 'app/module/admin-profile/shared/model/profile-response';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { StatusDescriptionConstants } from 'app/constants/status-description-constants';
import { AppRoutes } from '../../../constants/app-routes';

@Component({
  selector: 'app-admin-profile-details',
  templateUrl: './admin-profile-details.component.html',
  styleUrls: ['./admin-profile-details.component.scss']
})
export class AdminProfileDetailsComponent implements OnInit, OnDestroy {

  adminProfile: AdminProfileDto;
  profileResponses: ProfileResponse[] = [];
  firstAlreadyLoaded: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ngRoute: Router,
    private adminProfileService: AdminProfileService,
    private location: Location
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {

      if (this.firstAlreadyLoaded) {
        this.ngRoute.navigate([AppRoutes.ADMIN_PROFILE]);
      }

      this.firstAlreadyLoaded = true;

    });


    this.profileResponses = this.activatedRoute.snapshot.data.profileResponse;
    this.adminProfile = this.adminProfileService.getSelectedAdminProfile();
  }

  ngOnDestroy() {
    this.adminProfileService.setSelectedAdminProfile(null);
  }

  goBack() {
    this.location.back();
  }

  isActive(statusDescription: string) {
    return statusDescription === "Y";
  }
}
