import { NgModule } from '@angular/core';
import { AdminProfileRoutingModule } from 'app/module/admin-profile/admin-profile-routing.module';
import { SharedModule } from 'app/module/shared/shared.module';
import { AdminProfileComponent } from 'app/module/admin-profile/admin-profile.component';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';
import { ViewAdminProfileResolverService } from 'app/module/admin-profile/shared/resolvers/view-admin-profile-resolver.service';
import { AdminProfileDetailsComponent } from 'app/module/admin-profile/details/admin-profile-details.component';
import { AdminProfileDetailResolverService } from 'app/module/admin-profile/shared/resolvers/admin-profile-detail-resolver.service';
import { AdminProfileCreateComponent } from 'app/module/admin-profile/create/admin-profile-create.component';
import { AdminProfileManageComponent } from 'app/module/admin-profile/manage/admin-profile-manage.component';
import { AdminProfileEditComponent } from 'app/module/admin-profile/manage/edit/admin-profile-edit.component';
import { ManageAdminProfileResolverService } from 'app/module/admin-profile/shared/resolvers/manage-admin-profile-resolver.service';
import { AdminProfileDetailsGuardService } from 'app/module/admin-profile/shared/guard/admin-profile-details-guard.service';
import { TreeviewModule, TreeviewEventParser, OrderDownlineTreeviewEventParser } from 'ngx-treeview';
import { CreateAdminProfileResolverService } from 'app/module/admin-profile/shared/resolvers/create-admin-profile-resolver.service';

@NgModule({
  imports: [
    SharedModule,
    AdminProfileRoutingModule,
    TreeviewModule.forRoot()
  ],
  declarations: [
    AdminProfileComponent,
    AdminProfileDetailsComponent,
    AdminProfileCreateComponent,
    AdminProfileManageComponent,
    AdminProfileEditComponent
  ],
  providers: [
    AdminProfileService,
    ViewAdminProfileResolverService,
    AdminProfileDetailResolverService,
    AdminProfileDetailsGuardService,
    CreateAdminProfileResolverService,
    ManageAdminProfileResolverService,
    {
      provide: TreeviewEventParser,
      useClass: OrderDownlineTreeviewEventParser
    }
  ]
})
export class AdminProfileModule { }
