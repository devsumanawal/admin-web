import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ApiConstants } from 'app/constants/api-constants';
import { CreateAdminProfile } from 'app/module/admin-profile/shared/model/create-admin-profile';
import { BackendResponse } from 'app/module/core/models/backend-response';
import { AdminProfileDto } from 'app/module/admin-profile/shared/model/admin-profile-dto-model';


@Injectable()
export class AdminProfileService {

  selectedAdminProfile: AdminProfileDto;

  constructor(private http: HttpClient) { }

  setSelectedAdminProfile(adminProfile: AdminProfileDto) {
    this.selectedAdminProfile = adminProfile;
  }

  getSelectedAdminProfile(): AdminProfileDto {
    return this.selectedAdminProfile;
  }

  getAllAdminProfiles() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE);
  }

  searchAdminProfiles(searchParam) {
    return this.http.post<any>(ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + ApiConstants.SEARCH, searchParam);
  }

  searchAdminProfilesForManage() {
    return this.http.get<any>(ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + ApiConstants.MANAGE);
  }

  getAllAdminProfileForAdminCreate() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE);
  }

  getAllAdminProfilesForManage() {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + ApiConstants.MANAGE);
  }

  getAdminProfileById(id) {
    return this.http.get(ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + '/' + id);
  }

  getAllAdminRoles() {
    return this.http.get
      (ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + ApiConstants.ADMIN_ROLES);
  }

  create(createAdminProfile: CreateAdminProfile) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE, createAdminProfile);
  }

  edit(id: number, editAdminProfile: CreateAdminProfile) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + ApiConstants.MODIFY + '/' + id, editAdminProfile);
  }

  deleteAdminProfile(deleteAdminModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + ApiConstants.MANAGE + ApiConstants.DELETE, deleteAdminModel);
  }

  blockAdminProfile(blockAdminModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + ApiConstants.MANAGE + ApiConstants.BLOCK, blockAdminModel);
  }

  unBlockAdminProfile(unBlockAdminModel) {
    return this.http.post<BackendResponse>
      (ApiConstants.ENDPOINT + ApiConstants.ADMIN_PROFILE + ApiConstants.MANAGE + ApiConstants.UNBLOCK, unBlockAdminModel);
  }
}
