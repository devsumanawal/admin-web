export class CreateProfileFeatures {
    serviceGroupRoleMapId: number;
    isActive: boolean;

    constructor(id: number, isActive: boolean) {
        this.isActive = isActive;
        this.serviceGroupRoleMapId = id;
    }
}
