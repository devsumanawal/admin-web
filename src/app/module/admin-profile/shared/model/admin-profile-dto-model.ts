import { ConvergentAdminLoginDto } from 'app/module/admin/shared/model/convergent-admin-login-dto';

export class AdminProfileDto {
  name: string;
  id: number;
  description: string;
  createdDate: Date;
  modifiedDate: Date;
  remarks: string;
  createdBy: ConvergentAdminLoginDto;
  modifiedBy: ConvergentAdminLoginDto;
  approvedBy: ConvergentAdminLoginDto;
  disapprovedBy: ConvergentAdminLoginDto;
}
