export class ProfileResponse {
  id: number;
  name: string;
  description: string;
  isActive: boolean;
  parentName: string;
  childRoles: ProfileResponse[] = [];
}
