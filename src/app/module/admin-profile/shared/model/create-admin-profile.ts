import { CreateProfileFeatures } from 'app/module/admin-profile/shared/model/create-profile-features';

export class CreateAdminProfile {

    name: string;
    description: string;
    createProfileFeatures: Array<CreateProfileFeatures> = []
}
