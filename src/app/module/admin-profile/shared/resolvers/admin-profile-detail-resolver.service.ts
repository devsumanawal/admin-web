import {Injectable} from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import {ToastrService} from 'ngx-toastr';
import {AdminProfileService} from 'app/module/admin-profile/shared/service/admin-profile.service';
import {AppRoutes} from '../../../../constants/app-routes';

@Injectable()
export class AdminProfileDetailResolverService implements Resolve<Observable<any>> {
  constructor(
    private adminProfileService: AdminProfileService,
    private toastr: ToastrService,
    private route: Router,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.adminProfileService.getAdminProfileById(route.paramMap.get('id')).catch((error) => {
      this.toastr.error(error.error.message);
      this.route.navigate([AppRoutes.ADMIN_PROFILE]);
      return Observable.empty();
    });
  }
}
