import { Injectable } from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import { ToastrService } from 'ngx-toastr';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';

@Injectable()
export class ManageAdminProfileResolverService implements Resolve<Observable<any>> {
  constructor(
    private adminProfileService: AdminProfileService,
    private toastr: ToastrService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.adminProfileService.getAllAdminProfilesForManage().catch((error) => {
      this.toastr.error(error.error.message);
      return Observable.empty();
    });
  }
}
