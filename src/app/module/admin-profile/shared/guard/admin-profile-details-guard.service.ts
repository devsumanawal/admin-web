import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AppRoutes } from 'app/constants/app-routes';
import { AdminProfileService } from 'app/module/admin-profile/shared/service/admin-profile.service';

@Injectable()
export class AdminProfileDetailsGuardService implements CanActivate {

  canActivate() {
    if (this.adminProfileService.getSelectedAdminProfile() == null) {
      this.router.navigate([AppRoutes.ADMIN_PROFILE]);
      return false;
    } else {
      return true;
    }
  }

  constructor(
    private adminProfileService: AdminProfileService,
    public router: Router
  ) { }

}
