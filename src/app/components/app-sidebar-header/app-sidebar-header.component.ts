import { Component } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

import { ToastrService } from 'ngx-toastr';
import { AdminDetailService } from 'app/module/admin-details/shared/admin-detail.service';
import { AdminDetails } from 'app/module/admin-details/shared/admin-details';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-sidebar-header',
  templateUrl: './app-sidebar-header.component.html'
})
export class AppSidebarHeaderComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
