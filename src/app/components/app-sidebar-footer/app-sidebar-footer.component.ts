import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AppRoutes } from 'app/constants/app-routes';
import { AuthorizationService } from 'app/module/core/service/authorization.service';

import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

import { ToastrService } from 'ngx-toastr';
import { AdminDetailService } from 'app/module/admin-details/shared/admin-detail.service';
import { AdminDetails } from 'app/module/admin-details/shared/admin-details';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-sidebar-footer',
  templateUrl: './app-sidebar-footer.component.html'
})
export class AppSidebarFooterComponent implements OnInit, OnDestroy {
  adminDetailAppSidebar: AdminDetails = new AdminDetails();
  adminDetailSubscription: Subscription;

  constructor(private adminDetailService: AdminDetailService,
    private authorizationService: AuthorizationService, private router: Router,
    private toastr: ToastrService) {
    this.adminDetailSubscription =
      this.adminDetailService.getAdminDetailsRxSubject()
        .subscribe(adminDetail => {
          this.adminDetailAppSidebar = new AdminDetails();
          this.adminDetailAppSidebar.name = adminDetail.name;
          this.adminDetailAppSidebar.imageURL = adminDetail.imageURL;
        });
  }

  ngOnInit() {
    this.adminDetailService.getDetailAndSyncAdminDetail();
  }

  ngOnDestroy() {
    this.adminDetailSubscription.unsubscribe();
  }

  logout() {
    this.authorizationService.removeToken();
    this.router.navigate([AppRoutes.LOGIN]);
  }
}
