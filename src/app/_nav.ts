import { AppRoutes } from 'app/constants/app-routes';

export const navigation = [
  // {
  //   name: 'Dashboard',
  //   url: getNavUrl(AppRoutes.DASHBOARD),
  //   icon: 'icon-speedometer',
  //   badge: {
  //     variant: 'info'
  //   }
  // },
  {
    name: 'Admin',
    url: getNavUrl(AppRoutes.ADMIN),
    icon: 'cai cai-admin',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    name: 'Merchant',
    url: getNavUrl(AppRoutes.MERCHANT),
    icon: 'cai cai-merchant',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    name: 'Client',
    url: getNavUrl(AppRoutes.CLIENT),
    icon: 'cai cai-user',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    name: 'Admin Profile',
    url: getNavUrl(AppRoutes.ADMIN_PROFILE),
    icon: 'cai cai-admin-profile',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Merchant Profile',
    url: getNavUrl(AppRoutes.MERCHANT_PROFILE),
    icon: 'cai cai-merchant-profile',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Request',
    url: getNavUrl(AppRoutes.REQUEST + '/' + AppRoutes.SEARCH_REQUEST),
    icon: 'cai cai-merchant',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Suspicious Request',
    url: getNavUrl(AppRoutes.REQUEST + '/' + AppRoutes.SEARCH_SUSPICIOUS_REQUEST),
    icon: 'cai cai-merchant',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Merchant Qr Request',
    url: getNavUrl(AppRoutes.REQUEST + '/' + AppRoutes.SEARCH_MERCHANT_QR_REQUEST),
    icon: 'cai cai-merchant',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Esewa Unsettled Request',
    url: getNavUrl(AppRoutes.REQUEST + '/' + AppRoutes.ESEWA_UNSETTLED_REQUEST),
    icon: 'cai cai-merchant',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Esewa Settlement Log',
    url: getNavUrl(AppRoutes.REQUEST + '/' + AppRoutes.SEARCH_ESEWA_SETTLEMENT_LOG),
    icon: 'cai cai-merchant',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Merchant Request Verification',
    url: getNavUrl(AppRoutes.REQUEST + '/' + AppRoutes.SEARCH_MERCHANT_REQUEST_VERIFICATION),
    icon: 'cai cai-merchant',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Client Response',
    url: getNavUrl(AppRoutes.REQUEST + '/' + AppRoutes.SEARCH_CLIENT_RESPONSE),
    icon: 'cai cai-merchant',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Status Info',
    url: getNavUrl(AppRoutes.STATUS),
    icon: 'cai cai-hash',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Meta Table',
    url: getNavUrl(AppRoutes.META_TABLE),
    icon: 'cai cai-table',
    badge: {
      variant: 'info'
    }
  },
  {
    name: 'Password Policy',
    url: getNavUrl(AppRoutes.MANAGE_PASSWORD_POLICY),
    icon: 'ai cai-security',
    badge: {
      variant: 'info'
    }
  }
];

function getNavUrl(appRoute: String): String {
  return '/' + appRoute;
}
