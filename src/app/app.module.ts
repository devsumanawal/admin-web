// tslint:disable:max-line-length
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { AppComponent } from './app.component';

// Import containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

const APP_CONTAINERS = [
  FullLayoutComponent,
  SimpleLayoutComponent
]

// Import components
import {
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
} from './components';

const APP_COMPONENTS = [
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
]

// Import directives
import {
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
} from './directives';

const APP_DIRECTIVES = [
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
]

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { LandingModule } from './module/landing/landing.module';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from './module/shared/shared.module';
import { CoreModule } from './module/core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { AdminDetailsModule } from 'app/module/admin-details/admin-details.module';
import { AdminDetailService } from 'app/module/admin-details/shared/admin-detail.service';
import { ChangePasswordModule } from 'app/module/change-password/change-password.module';
import { ModalModule } from 'ngx-bootstrap/modal/modal.module';
import { PageModule } from 'app/module/pages/page.module';
import { PasswordPolicyModule } from 'app/module/password-policy/password-policy.module';
import { VerticalTimelineModule } from 'angular-vertical-timeline';
import { OfficeTypeModule } from './module/office-type/office-type.module';
import { UserGroupModule } from './module/user-group/user-group.module';
import { OfficeModule } from './module/office/office.module';
import { DepartmentModule } from './module/department/department.module';
import { MinistryModule } from './module/ministry/ministry.module';
import { CurrencyModule } from './module/currency/currency.module';
import { SourceModule } from './module/source/source.module';
import { LineItemModule } from './module/line-item/line-item.module';
import { DonorModule } from './module/donor/donor.module';
import { FiscalYearModule } from './module/fiscal-year/fiscal-year.module';
import { ProjectModule } from './module/project/project.module';

const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    VerticalTimelineModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule,
    ToastrModule.forRoot(
      {
        positionClass: 'toast-bottom-right',
        preventDuplicates: true,
        closeButton: true
      }
    ),
    PerfectScrollbarModule.forRoot(PERFECT_SCROLLBAR_CONFIG),
    BrowserAnimationsModule,

    LandingModule,
    SharedModule,
    PageModule,
    AdminDetailsModule,
    ChangePasswordModule,
    PasswordPolicyModule,
    ModalModule,
    OfficeTypeModule,
    UserGroupModule,
    OfficeModule,
    DepartmentModule,
    MinistryModule,
    CurrencyModule,
    SourceModule,
    LineItemModule,
    DonorModule,
    FiscalYearModule,
    ProjectModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES,
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  },
    AdminDetailService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
